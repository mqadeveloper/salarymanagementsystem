﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MQASalaryManagementSystem
{
    public partial class EditNetSalaryForm : Form
    {
        public EditNetSalaryForm()
        {
            InitializeComponent();
        }

        private void OutputForm_Load(object sender, EventArgs e)
        {
            labelSalaryID.Text = SalaryForm.salaryID;
            labelName.Text = SalaryForm.NName;
            tbBaseSalary.Text = SalaryForm.BaseSalary;
            tbExecutive.Text = SalaryForm.Executive;
            tbTraval.Text = SalaryForm.Traval;
            tbHosing.Text = SalaryForm.Hosing;
            tbLanguage.Text = SalaryForm.Language;
            tbBonus.Text = SalaryForm.Bonus;
            tbOT.Text = SalaryForm.OT;
        }
        public static string salaryID = "";
        public static string NName = "";
        public static string BaseSalary = "";
        public static string Executive = "";
        public static string Traval = "";
        public static string Hosing = "";
        public static string Language = "";
        public static string Bonus = "";
        public static string OT = "";
        public static int sum = 0;
        public static string MN = "";
        public static string YN = "";


        private void butedit_Click(object sender, EventArgs e)
        {
            int x;
            if (string.IsNullOrEmpty(tbBaseSalary.Text))
            {
                MessageBox.Show("Please input employee Base Salary", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbBaseSalary.Text = "";
            }
            else if (!int.TryParse(tbBaseSalary.Text, out x))
            {
                MessageBox.Show("Base Salary is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbBaseSalary.Text = "";
            }
            else if (int.Parse(tbBaseSalary.Text) < 0)
            {
                MessageBox.Show("Base Salary is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbExecutive.Text))
            {
                MessageBox.Show("Please input employee Executive Alloance", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbExecutive.Text = "";
            }
            else if (int.Parse(tbExecutive.Text) < 0)
            {
                MessageBox.Show("Executive Alloance is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (!int.TryParse(tbExecutive.Text, out x))
            {
                MessageBox.Show("Executive Alloance is not number ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbExecutive.Text = "";
            }
            else if (string.IsNullOrEmpty(tbTraval.Text))
            {
                MessageBox.Show("Please input employee Traval Alloance", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbTraval.Text = "";
            }
            else if (!int.TryParse(tbTraval.Text, out x))
            {
                MessageBox.Show("Traval Alloance is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbTraval.Text = "";
            }
            else if (int.Parse(tbTraval.Text) < 0)
            {
                MessageBox.Show("Traval Alloance is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbHosing.Text))
            {
                MessageBox.Show("Please input employee Housing Alloance", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbHosing.Text = "";
            }
            else if (!int.TryParse(tbHosing.Text, out x))
            {
                MessageBox.Show("Housing Alloance is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbHosing.Text = "";
            }
            else if (int.Parse(tbHosing.Text) < 0)
            {
                MessageBox.Show("Housing Alloance is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbLanguage.Text))
            {
                MessageBox.Show("Please input employee Language Alloance", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbLanguage.Text = "";
            }
            else if (!int.TryParse(tbLanguage.Text, out x))
            {
                MessageBox.Show("Language Alloance is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbLanguage.Text = "";
            }
            else if (int.Parse(tbLanguage.Text) < 0)
            {
                MessageBox.Show("Language Alloance is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbBonus.Text))
            {
                MessageBox.Show("Please input employee Bonus", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbBonus.Text = "";
            }
            else if (!int.TryParse(tbBonus.Text, out x))
            {
                MessageBox.Show("Bonus is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbBonus.Text = "";
            }
            else if (int.Parse(tbBonus.Text) < 0)
            {
                MessageBox.Show("Bonus is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbOT.Text))
            {
                MessageBox.Show("Please input employee OT", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbOT.Text = "";
            }
            else if (!int.TryParse(tbOT.Text, out x))
            {
                MessageBox.Show("OT is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbOT.Text = "";
            }
            else if (int.Parse(tbOT.Text) < 0)
            {
                MessageBox.Show("OT is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {


                salaryID = labelSalaryID.Text;
                NName = labelName.Text;
                BaseSalary = tbBaseSalary.Text;
                Executive = tbExecutive.Text;
                Traval = tbTraval.Text;
                Hosing = tbHosing.Text;
                Language = tbLanguage.Text;
                Bonus = tbBonus.Text;
                OT = tbOT.Text;
                MN = SalaryForm.M;
                YN = SalaryForm.Y;
                sum = int.Parse(tbBaseSalary.Text) + int.Parse(tbExecutive.Text) + int.Parse(tbTraval.Text) + int.Parse(tbHosing.Text);
                sum += int.Parse(tbLanguage.Text) + int.Parse(tbBonus.Text) + int.Parse(tbOT.Text);
                SalaryForm salaryForm = new SalaryForm();
                salaryForm.NetSalaryEdit();

                this.Close();
            }
        }

        private void butclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
