﻿using Guna.UI2.WinForms.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MQASalaryManagementSystem
{
    public partial class EditEmpForm : Form
    {

        private string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public EditEmpForm()
        {
            InitializeComponent();
            ShowEditEmp();
            ComboPosition();
        }
        //  ------------------------------------- Datagrifview Cell Click ---------------------------
        private void dataGridViewEditEmp_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex != -1)
            {
                DataGridViewRow dgvRow = dataGridViewEditEmp.Rows[e.RowIndex];
                labelID.Text = dgvRow.Cells[0].Value.ToString();
                tbFname.Text = dgvRow.Cells[1].Value.ToString();
                tbLname.Text = dgvRow.Cells[2].Value.ToString();
                tbNname.Text = dgvRow.Cells[3].Value.ToString();
                tbPhone.Text = dgvRow.Cells[8].Value.ToString();
                cbPosition.Text = dgvRow.Cells[5].Value.ToString();
                dateBirth.Text = dgvRow.Cells[6].Value.ToString();
                dateJoin.Text = dgvRow.Cells[7].Value.ToString();
                tbAddress.Text = dgvRow.Cells[9].Value.ToString();
                string test = dgvRow.Cells[10].Value.ToString();
                
                if(string.IsNullOrEmpty(test))
                {
                    dateResign.CustomFormat = " ";
                }
                else
                {
                    
                    dateResign.Text = dgvRow.Cells[10].Value.ToString();
                }
                

            }
        }

        
        private void ShowEditEmp()
        {
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                string sql_Text = "select * from Employees";
                DataTable tbl = new DataTable();
                DataGridViewScrollHelper hScrollHelper = new DataGridViewScrollHelper(dataGridViewEditEmp, guna2HScrollBar1, true);
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, (OleDbConnection)cnn))
                {

                    adapter.Fill(tbl);
                    dataGridViewEditEmp.DataSource = tbl;
                    dataGridViewEditEmp.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridViewEditEmp.Columns[6].DefaultCellStyle.Format = "yyyy/MM/dd";
                    dataGridViewEditEmp.Columns[7].DefaultCellStyle.Format = "yyyy/MM/dd";
                    dataGridViewEditEmp.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridViewEditEmp.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridViewEditEmp.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
            }
        }
        

        private void tbSearchEdit_TextChanged(object sender, EventArgs e)
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            OleDbConnection cnn = new OleDbConnection(con);
            string sql_Text;
           
            
                sql_Text = "SELECT * FROM Employees";
                sql_Text += " WHERE EMP_ID LIKE '%" + tbSearchEdit.Text + "%'";
                sql_Text += " OR EMP_FristName LIKE '%" + tbSearchEdit.Text + "%'";
                sql_Text += " OR EMP_LastName LIKE '%" + tbSearchEdit.Text + "%'";
                sql_Text += " OR EMP_NickName LIKE '%" + tbSearchEdit.Text + "%'";
                sql_Text += " OR Phone LIKE '%" + tbSearchEdit.Text + "%'";
                DataTable tbl = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, cnn))
                {
                    adapter.Fill(tbl);
                    dataGridViewEditEmp.DataSource = tbl;
                }
            
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            int x;
            if (string.IsNullOrEmpty(tbFname.Text))
            {
                MessageBox.Show("Please input employee First name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbFname.Text = "";
            }
            else if (int.TryParse(tbFname.Text, out x))
            {
                MessageBox.Show("First name is not string", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbFname.Text = "";
            }
            else if (string.IsNullOrEmpty(tbLname.Text))
            {
                MessageBox.Show("Please input employee Last name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbLname.Text = "";
            }
            else if (int.TryParse(tbLname.Text, out x))
            {
                MessageBox.Show("Last name is not string", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbLname.Text = "";
            }
            else if (string.IsNullOrEmpty(tbNname.Text))
            {
                MessageBox.Show("Please input employee Nick name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbNname.Text = "";
            }
            else if (int.TryParse(tbFname.Text, out x))
            {
                MessageBox.Show("Nick name is not string", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbNname.Text = "";
            }
            else if (string.IsNullOrEmpty(cbPosition.Text))
            {
                MessageBox.Show("Please select position", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbPhone.Text))
            {
                MessageBox.Show("Please input employee phone", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbPhone.Text = "";
            }
            else if (!int.TryParse(tbPhone.Text, out x))
            {
                MessageBox.Show("Phone is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbPhone.Text = "";
            }
            else if (int.Parse(tbPhone.Text) < 0)
            {
                MessageBox.Show("Negative Values are not allowed", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbAddress.Text))
            {
                MessageBox.Show("Please input employee Address", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbAddress.Text = "";
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to edit?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialogResult == DialogResult.Yes)
                {

                    EditEMP(labelID.Text);
                    ShowEditEmp();
                }
            }
           
            
        }
        // ----------------------------------- Editt Employees infomation -------------------------------
        private void EditEMP(string EMP_ID)
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            IDbTransaction trans = null;
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                string sql_Text = "UPDATE Employees SET EMP_FristName ='" + tbFname.Text + "',EMP_LastName = '" + tbLname.Text
                    + "',EMP_NickName ='" + tbNname.Text + "',Join_Date ='" + dateJoin.Text 
                    + "',Birth_Date = '" + dateBirth.Text + "',Phone ='" + tbPhone.Text
                    + "' ,Address = '" + tbAddress.Text + "' ,Job_position = '" + cbPosition.Text;
                if (checkBoxResign.Checked == true)
                {
                    sql_Text += "',Resign_date ='" + dateResign.Text + "' WHERE EMP_ID = " + EMP_ID;
                }
                else
                {
                    sql_Text += "',Resign_date = NULL WHERE EMP_ID = " + EMP_ID;
                }
               

                using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                {
                    trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                    cmd_command.Connection = cnn;
                    cmd_command.Transaction = trans;

                    try
                    {
                        cmd_command.ExecuteNonQuery();
                        trans.Commit();
                        MessageBox.Show("Edit Employee Complete", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (OleDbException ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    
                }


            }
        }

        private void checkBoxResign_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxResign.Checked == true)
            {
                dateResign.Enabled = true;
            }
            else
            {
                dateResign.Enabled = false;
            }
        }

        private void dateResign_ValueChanged(object sender, EventArgs e)
        {
            dateResign.CustomFormat = "dd-MM-yyyy";
        }

        private void butclose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ComboPosition()
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (IDbConnection cnn = new OleDbConnection(con))
            {


                string cbposition = "SELECT Position_Name From Job_Position";
                DataTable tbl = new DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(cbposition, con);
                adapter.Fill(tbl);
                foreach (DataRow dr in tbl.Rows)
                {
                    cbPosition.Items.Add(dr["Position_Name"].ToString());
                }
            }
        }

        
    }
}
