﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration;
namespace MQASalaryManagementSystem
{
    public partial class AddNewEmpForm : Form
    {
        private string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public AddNewEmpForm()
        {
            InitializeComponent();
            ComboPosition();
        }

        private void butAdd_Click(object sender, EventArgs e)
        {
            int x;
            if (string.IsNullOrEmpty(tbFname.Text))
            {
                MessageBox.Show("Please input employee First name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbFname.Text = "";
            }
            else if (int.TryParse(tbFname.Text, out x))
            {
                MessageBox.Show("First name is not string", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbFname.Text = "";
            }
            else if (string.IsNullOrEmpty(tbLname.Text))
            {
                MessageBox.Show("Please input employee Last name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbLname.Text = "";
            }
            else if (int.TryParse(tbLname.Text, out x))
            {
                MessageBox.Show("Last name is not string", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbLname.Text = "";
            }
            else if (string.IsNullOrEmpty(tbNname.Text))
            {
                MessageBox.Show("Please input employee Nick name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbNname.Text = "";
            }
            else if (int.TryParse(tbFname.Text, out x))
            {
                MessageBox.Show("Nick name is not string", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbNname.Text = "";
            }
            else if (string.IsNullOrEmpty(cbPosition.Text))
            {
                MessageBox.Show("Please select position", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbPhone.Text))
            {
                MessageBox.Show("Please input employee phone", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbPhone.Text = "";
            }
            else if (!int.TryParse(tbPhone.Text, out x))
            {
                MessageBox.Show("Phone is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbPhone.Text = "";
            }
            else if (string.IsNullOrEmpty(tbAddress.Text))
            {
                MessageBox.Show("Please input employee Address", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbAddress.Text = "";
            }
            else
            {

                DialogResult dialogResult = MessageBox.Show("Do you want to Add New Employee?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialogResult == DialogResult.Yes)
                {
                    AddNewEmp(0);
                    tbAddress.Text = " ";
                    tbFname.Text = " ";
                    tbLname.Text = " ";
                    tbNname.Text = " ";
                    tbPhone.Text = " ";
                    AddNewEmp(1);
                    AddNewEmp(2);
                    AddNewEmp(3);

                }
            }

        }
        //--------------Create New Employee infomation -----------------------------
        private void AddNewEmp(int num)
        {
            
            IDbTransaction trans = null;
            string year = DateTime.Today.ToString("yyyy");
            using (IDbConnection cnn = new OleDbConnection(con))
            {


                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                string pst = cbPosition.Text;
                string sql_Text = " ";
                if (num == 0) 
                {
                    sql_Text = "INSERT INTO Employees (EMP_FristName,EMP_LastName,EMP_NickName,Job_position,Join_Date,Birth_Date,Phone,Address) ";
                    sql_Text += "VALUES ('" + tbFname.Text + "','" + tbLname.Text + "','" + tbNname.Text + "','" + pst + "','";
                    sql_Text += dateJoin.Text + "','" + dateBirth.Text + "','" + tbPhone.Text + "','" + tbAddress.Text + "')";
                }
                else if (num == 1)
                {
                    sql_Text = "INSERT INTO Salary (Base_salary,Executive_Allowance,Travel_Allowance,housing_Allowance,Language_Allowance,Bonus,OT)";
                    sql_Text += "VALUES (0,0,0,0,0,0,0)";
                }   
                else if (num == 2)
                {
                    sql_Text = "INSERT INTO Tax_Deduction (Personal_Deduction,Spouse_Deduction,Child_Deduction,Parental_Care_Deduction,Expense_Deduction,LMF_RTF_Deduction,Home_Loan_Deduction)";
                    sql_Text += "VALUES (0,0,0,0,0,0,0)";
                }
                else if (num == 3)
                {
                    sql_Text = "INSERT INTO Social_Security (Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dece,Year_Premiums)";
                    sql_Text += "VALUES (750,750,750,750,750,750,750,750,750,750,750,750,"+year+")";
                }
                using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                {
                    trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                    cmd_command.Connection = cnn;
                    cmd_command.Transaction = trans;

                    try
                    {
                        cmd_command.ExecuteNonQuery();
                        trans.Commit();
                        MessageBox.Show("Add New Employee Complete", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (OleDbException ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }

            }
        }
        private void ComboPosition()
        {
           
            using (IDbConnection cnn = new OleDbConnection(con))
            {


                string cbposition = "SELECT Position_Name From Job_Position";
                DataTable tbl = new DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(cbposition, con);
                adapter.Fill(tbl);
                foreach (DataRow dr in tbl.Rows)
                {
                    cbPosition.Items.Add(dr["Position_Name"].ToString());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
