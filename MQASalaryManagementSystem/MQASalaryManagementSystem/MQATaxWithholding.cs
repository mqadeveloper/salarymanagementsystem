﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace MQASalaryManagementSystem
{
    class MQATaxWithholding
    {
        public static void MQATaxWithholdingEx()
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            DateTime time = DateTime.Now;
            string timef = time.ToString("yyyyMM");
            //string timef = DateTime.Today.ToString("7/5/2021 12:00:00 A'M'");
            Application excApp;
            Workbook excBook;
            Worksheet excSheet;
            Range rg;
            excApp = new Application();
            excApp.Visible = false;
            excApp.UserControl = false;
            excBook = (Workbook)(excApp.Workbooks.Add(Missing.Value));
            using (IDbConnection cnn = new OleDbConnection(con))

            {


                string tr = DateTime.Today.ToString("MM");
                string SQL_Text = "SELECT  Employees.EMP_ID,Employees.EMP_FristName,Employees.EMP_LastName,Employees.EMP_NickName,Net_Salary.*,Tax_Deduction.*";
                SQL_Text += "FROM ((Employees INNER JOIN Net_Salary ON Employees.EMP_ID = Net_Salary.Net_Salary_ID) ";
                SQL_Text += "INNER JOIN Tax_Deduction ON Employees.EMP_ID = Tax_Deduction.Tax_ID) ";
                SQL_Text += "WHERE MONTH(Pay_Date) = "+tr;

                //SQL_Text += "INNER JOIN Tax_Deduction ON Employees.Tax_ID = Tax_Deduction.Tax_ID ";
                //SQL_Text += "WHERE year(Salary_Date) = 2021 AND MONTH(Salary_Date) = 07";

                //"SELECT * FROM Salary WHERE year(Salary_Date) = 2021 AND MONTH(Salary_Date) = 07";
                System.Data.DataTable tbl = new System.Data.DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
                adapter.Fill(tbl);
                string ty = "07/05/2021 12:00:00 AM";
                
                int i = 0;
                
                foreach (DataRow dr in tbl.Rows)
                {
                    int empid = int.Parse(dr[0].ToString());
                    string Fname = dr[1].ToString();
                    string Lname = dr[2].ToString();
                    string Nname = dr[3].ToString();
                    int Base = int.Parse(dr[7].ToString());
                    int Executive = int.Parse(dr[8].ToString());
                    int Travel = int.Parse(dr[9].ToString());
                    int housing = int.Parse(dr[10].ToString());
                    int Language = int.Parse(dr[11].ToString());
                    int Bonus = int.Parse(dr[12].ToString());
                    int OT = int.Parse(dr[13].ToString());
                    string sdate = dr[18].ToString();
                    int Personal = int.Parse(dr[20].ToString());
                    int Spouse = int.Parse(dr[21].ToString());
                    int Child = int.Parse(dr[22].ToString());
                    int Parental = int.Parse(dr[23].ToString());
                    int Expense = int.Parse(dr[24].ToString());
                    int LMF_RTF = int.Parse(dr[25].ToString());
                    int Home_Loan = int.Parse(dr[26].ToString());


                    Console.WriteLine(sdate);




                        excSheet = (Worksheet)excBook.Worksheets.Add();
                        excSheet.Name = Fname;
                        excSheet.Cells[1, 1] = "所得税計算";
                        excSheet.Cells[1, 2] = Fname+" "+Lname;
                        excSheet.Cells[3, 1] = "項目";
                        excSheet.Cells[3, 2] = "給与額";
                        excSheet.Cells[3, 3] = "適用月数";
                        excSheet.Cells[3, 4] = "金額 (THB)";
                        excSheet.Cells[4, 1] = "基本給(1月～5月)";
                        excSheet.Cells[4, 2] = Base;
                        excSheet.Cells[4, 3] = CountEmpidSalary(empid,1);
                        excSheet.Cells[4, 4].Formula = "=B4*C4";
                        excSheet.Cells[5, 1] = "基本給(6月～12月)";
                        excSheet.Cells[5, 2] = Base;
                        excSheet.Cells[5, 3] = CountEmpidSalary(empid, 2);
                        excSheet.Cells[5, 4].Formula = "=B5*C5";
                        excSheet.Cells[6, 1] = "賞与";
                        excSheet.Cells[6, 2] = Bonus;
                        excSheet.Cells[6, 3] = /*CountEmpidSalary(empid,0)*/ 12;
                        excSheet.Cells[6, 4].Formula = "=B6*C6";
                        excSheet.Cells[7, 1] = "役職手当(1月～12月)";
                        excSheet.Cells[7, 2] = Executive;
                        excSheet.Cells[7, 3] = /*CountEmpidSalary(empid, 0)*/ 12; ;
                        excSheet.Cells[7, 4].Formula = "=B7*C7";
                        excSheet.Cells[8, 1] = "役職手当(x月～x月)";
                        excSheet.Cells[9, 1] = "通勤手当(1月～12月)";
                        excSheet.Cells[9, 2] = Travel;
                        excSheet.Cells[9, 3] = TravalAllow(empid,1);
                        excSheet.Cells[9, 4] = TravalAllow(empid,0);
                        excSheet.Cells[10, 1] = "通勤手当(4月)";
                        excSheet.Cells[11, 1] = "通勤手当(5月)";
                        excSheet.Cells[12, 1] = "住宅手当(1月～12月)";
                        excSheet.Cells[12, 2] = housing;
                        excSheet.Cells[12, 3] = /*CountEmpidSalary(empid, 0)*/ 12;
                        excSheet.Cells[12, 4].Formula = "=B12*C12";
                        excSheet.Cells[13, 1] = "住宅手当(x月～x月)";
                        excSheet.Cells[14, 1] = "語学手当(1月～12月)";
                        excSheet.Cells[14, 2] = Language;
                        excSheet.Cells[14, 3] = /*CountEmpidSalary(empid, 0)*/ 12;
                        excSheet.Cells[14, 4].Formula = "=B14*C14";
                        excSheet.Cells[15, 1] = "語学手当(x月～x月)";
                        excSheet.Cells[16, 1] = "その他手当(1月)";
                        excSheet.Cells[16, 2] = OT;
                        excSheet.Cells[16, 3] = /*CountEmpidSalary(empid,0)*/ 12;
                        excSheet.Cells[16, 4].Formula = "=B16*C16";
                        excSheet.Cells[17, 1] = "その他手当(x月)";
                        excSheet.Cells[18, 1] = "総所得額";
                        excSheet.Cells[18, 4].Formula = "=SUM(D4:D17)";
                        excSheet.Cells[19, 1] = "基礎控除";
                        excSheet.Cells[19, 4].Formula = "=IF(D18*0.5>100000, -100000, -D18*0.5)";
                        excSheet.Cells[19, 5] = "所得額の50%。最高100,000THB迄";
                        excSheet.Cells[20, 1] = "基礎控除を控除後の総所得額";
                        excSheet.Cells[20, 4].Formula = "=SUM(D18:D19)";
                        excSheet.Cells[22, 1] = "控除";
                        excSheet.Cells[23, 1] = "項目";
                        excSheet.Cells[24, 1] = "個人控除";
                        excSheet.Cells[24, 4] = -Personal;
                        excSheet.Cells[24, 5] = "60,000THB";
                        excSheet.Cells[25, 1] = "配偶者控除";
                        excSheet.Cells[25, 4] = -Spouse;
                        excSheet.Cells[25, 5] = "30,000THB (収入ない場合に限る)";
                        excSheet.Cells[26, 1] = "子供控除";
                        excSheet.Cells[26, 4] = -Child;
                        excSheet.Cells[26, 5] = "1人当たり15,000THB (年齢25歳未満、最大3人)";
                        excSheet.Cells[27, 1] = "教育費控除";

                        excSheet.Cells[27, 5] = "1人当たり2,000THB";
                        excSheet.Cells[28, 1] = "両親控除";
                        excSheet.Cells[28, 4] = -Parental;
                        excSheet.Cells[28, 5] = "1人当たり30,000THB (年齢60歳以、収入ない場合)";
                        excSheet.Cells[29, 1] = "生命保険控除";
                        excSheet.Cells[29, 4] = -Expense;
                        excSheet.Cells[29, 5] = "支払い額 (上限100,000THB)";
                        excSheet.Cells[30, 1] = "家ローン";
                        excSheet.Cells[30, 4] = -Home_Loan;
                        excSheet.Cells[30, 5] = "支払い額 (上限100,000THB)";
                        excSheet.Cells[31, 1] = "LMF, RTF 投資";
                        excSheet.Cells[31, 4] = -LMF_RTF;
                        excSheet.Cells[31, 5] = "総所得の15% (上限500,000THB)";
                        excSheet.Cells[32, 1] = "社会保険";
                        excSheet.Cells[32, 3] = Secur_premium(empid,0,"none");
                        excSheet.Cells[32, 4] = -Secur_premium(empid,1,"none");
                        excSheet.Cells[32, 5] = "支払い額の5% (上限9,000THB)";
                        excSheet.Cells[34, 1] = "控除後課税対象額";
                        excSheet.Cells[34, 4].Formula = "=IF(D20+D32>60000,SUM(D20:D32),0)";
                        excSheet.Cells[36, 1] = "課税対象額計算";
                        excSheet.Cells[37, 1] = "控除後課税対象額";
                        excSheet.Cells[37, 2] = "税率";
                        excSheet.Cells[37, 3] = "課税額";
                        excSheet.Cells[37, 4] = "税額";
                        excSheet.Cells[38, 1] = "0 ～ 150,000";
                        excSheet.Cells[38, 2] = "0%";
                        excSheet.Cells[38, 3].Formula = "=IF(D34-150000>0, 150000, D34)";
                        excSheet.Cells[38, 4].Formula = "=C38*B38";
                        excSheet.Cells[39, 1] = "150,001 ～ 300,000";
                        excSheet.Cells[39, 2] = "5%";
                        excSheet.Cells[39, 3].Formula = "=IF(D34-C38-150000>0, 150000, D34-C38)";
                        excSheet.Cells[39, 4].Formula = "=C39*B39";
                        excSheet.Cells[40, 1] = "300,001 ～ 500,000";
                        excSheet.Cells[40, 2] = "10%";
                        excSheet.Cells[40, 3].Formula = "=IF(D34-C38-C39-200000>0, 200000, D34-C38-C39)";
                        excSheet.Cells[40, 4].Formula = "=C40*B40";
                        excSheet.Cells[41, 1] = "500,001 ～ 750,000";
                        excSheet.Cells[41, 2] = "15%";
                        excSheet.Cells[41, 3].Formula = "=IF(D34-C38-C39-C40-250000>0, 250000, D34-C38-C39-C40)";
                        excSheet.Cells[41, 4].Formula = "=C41*B41";
                        excSheet.Cells[42, 1] = "750,001 ～ 1,000,000";
                        excSheet.Cells[42, 2] = "20%";
                        excSheet.Cells[42, 3].Formula = "=IF(D34-C38-C39-C40-C41-250000>0, 250000, D34-C38-C39-C40-C41)";
                        excSheet.Cells[42, 4].Formula = "=C42*B42";
                        excSheet.Cells[43, 1] = "1,000,001 ～2,000,000";
                        excSheet.Cells[43, 2] = "25%";
                        excSheet.Cells[43, 3].Formula = "=IF(D34-C38-C39-C40-C41-C42-1000000>0, 250000, D34-C38-C39-C40-C41-C42)";
                        excSheet.Cells[43, 4].Formula = "=C43*B43";
                        excSheet.Cells[44, 1] = "2,000,001 ～4,000,000";
                        excSheet.Cells[44, 2] = "30%";
                        excSheet.Cells[44, 3].Formula = "=IF(D34-C38-C39-C40-C41-C42-C43-2000000>0, 250000, D34-C38-C39-C40-C41-C42-C43)";
                        excSheet.Cells[44, 4].Formula = "=C44*B44";
                        excSheet.Cells[45, 1] = "4,000,001 　以上";
                        excSheet.Cells[45, 2] = "35%";
                        excSheet.Cells[46, 1] = "合計";
                        excSheet.Cells[46, 4].Formula = "=SUM(D38:D45)";
                        excSheet.Cells[48, 1] = "源泉徴収税";
                        excSheet.Cells[49, 1] = "-";
                        excSheet.Cells[49, 2] = "月";
                        excSheet.Cells[49, 3] = "-";
                        excSheet.Cells[49, 4] = "税額";
                        excSheet.Cells[50, 2] = "1月";
                        excSheet.Cells[50, 4].Formula = "=FLOOR(D46/12, 1)";
                        excSheet.Cells[51, 2] = "2月";
                        excSheet.Cells[51, 4].Formula = "=FLOOR((D$46-SUM(D$50:D50))/11, 1)";
                        excSheet.Cells[52, 2] = "3月";
                        excSheet.Cells[52, 4].Formula = "=FLOOR((D$46-SUM(D$50:D51))/10, 1)";
                        excSheet.Cells[53, 2] = "4月";
                        excSheet.Cells[53, 4].Formula = "=FLOOR((D$46-SUM(D$50:D52))/9, 1)";
                        excSheet.Cells[54, 2] = "5月";
                        excSheet.Cells[54, 4].Formula = "=FLOOR((D$46-SUM(D$50:D53))/8, 1)";
                        excSheet.Cells[55, 2] = "6月";
                        excSheet.Cells[55, 4].Formula = "=FLOOR((D$46-SUM(D$50:D54))/7, 1)";
                        excSheet.Cells[56, 2] = "7月";
                        excSheet.Cells[56, 4].Formula = "=FLOOR((D$46-SUM(D$50:D55))/6, 1)";
                        excSheet.Cells[57, 2] = "8月";
                        excSheet.Cells[57, 4].Formula = "=FLOOR((D$46-SUM(D$50:D56))/5, 1)";
                        excSheet.Cells[58, 2] = "9月";
                        excSheet.Cells[58, 4].Formula = "=FLOOR((D$46-SUM(D$50:D57))/4, 1)";
                        excSheet.Cells[59, 2] = "10月";
                        excSheet.Cells[59, 4].Formula = "=FLOOR((D$46-SUM(D$50:D58))/3, 1)";
                        excSheet.Cells[60, 2] = "11月";
                        excSheet.Cells[60, 4].Formula = "=FLOOR((D$46-SUM(D$50:D59))/2, 1)";
                        excSheet.Cells[61, 2] = "12月";
                        excSheet.Cells[61, 4].Formula = "=D46-SUM(D50:D60)";
                        excSheet.Cells[62, 1] = "合計";
                        excSheet.Cells[62, 4].Formula = "=SUM(D50:D61)";
                        excSheet.Cells[64, 1] = "年間所得額";
                        excSheet.Cells[65, 1] = "年月";
                        excSheet.Cells[65, 2] = "基本給";
                        excSheet.Cells[65, 3] = "役職手当";
                        excSheet.Cells[65, 4] = "通勤手当";
                        excSheet.Cells[65, 5] = "住宅手当";
                        excSheet.Cells[65, 6] = "資格手当";
                        excSheet.Cells[65, 7] = "その他手当";
                        excSheet.Cells[65, 8] = "社会保険料";
                        excSheet.Cells[65, 9] = "源泉徴収税";
                        excSheet.Cells[65, 10] = "差引き支給額";
                        
                        excSheet.Cells[66, 1] = "1/1/2021";
                        excSheet.Cells[66, 2] = Base;
                        excSheet.Cells[66, 3] = Executive;
                        excSheet.Cells[66, 4] = Travel;
                        excSheet.Cells[66, 5] = housing;
                        excSheet.Cells[66, 6] = Language;
                        excSheet.Cells[66, 7] = OT;
                        excSheet.Cells[66, 8] = -Secur_premium(empid, 2, "Jan");
                        excSheet.Cells[66, 9].Formula = "=-D50";
                        excSheet.Cells[66, 10].Formula = "=SUM(B66:I66)";
                        excSheet.Cells[67, 1] = "2/1/2021";
                        excSheet.Cells[67, 2] = Base;
                        excSheet.Cells[67, 3] = Executive;
                        excSheet.Cells[67, 4] = Travel;
                        excSheet.Cells[67, 5] = housing;
                        excSheet.Cells[67, 6] = Language;
                        excSheet.Cells[67, 7] = OT;
                        excSheet.Cells[67, 8] = -Secur_premium(empid, 2, "Feb"); 
                        excSheet.Cells[67, 9].Formula = "=-D51";
                        excSheet.Cells[67, 10].Formula = "=SUM(B67:I67)";
                        excSheet.Cells[68, 1] = "3/1/2021";
                        excSheet.Cells[68, 2] = Base;
                        excSheet.Cells[68, 3] = Executive;
                        excSheet.Cells[68, 4] = Travel;
                        excSheet.Cells[68, 5] = housing;
                        excSheet.Cells[68, 6] = Language;
                        excSheet.Cells[68, 7] = OT;
                        excSheet.Cells[68, 8] = -Secur_premium(empid, 2, "Mar");
                        excSheet.Cells[68, 9].Formula = "=-D52";
                        excSheet.Cells[68, 10].Formula = "=SUM(B68:I68)";
                        excSheet.Cells[69, 1] = "4/1/2021";
                        excSheet.Cells[69, 2] = Base;
                        excSheet.Cells[69, 3] = Executive;
                        excSheet.Cells[69, 4] = Travel;
                        excSheet.Cells[69, 5] = housing;
                        excSheet.Cells[69, 6] = Language;
                        excSheet.Cells[69, 7] = OT;
                        excSheet.Cells[69, 8] = -Secur_premium(empid, 2, "Apr");
                        excSheet.Cells[69, 9].Formula = "=-D53";
                        excSheet.Cells[69, 10].Formula = "=SUM(B69:I69)";
                        excSheet.Cells[70, 1] = "5/1/2021";
                        excSheet.Cells[70, 2] = Base;
                        excSheet.Cells[70, 3] = Executive;
                        excSheet.Cells[70, 4] = Travel;
                        excSheet.Cells[70, 5] = housing;
                        excSheet.Cells[70, 6] = Language;
                        excSheet.Cells[70, 7] = OT;
                        excSheet.Cells[70, 8] = -Secur_premium(empid, 2, "May");
                        excSheet.Cells[70, 9].Formula = "=-D54";
                        excSheet.Cells[70, 10].Formula = "=SUM(B70:I70)";
                        excSheet.Cells[71, 1] = "6/1/2021";
                        excSheet.Cells[71, 2] = Base;
                        excSheet.Cells[71, 3] = Executive;
                        excSheet.Cells[71, 4] = Travel;
                        excSheet.Cells[71, 5] = housing;
                        excSheet.Cells[71, 6] = Language;
                        excSheet.Cells[71, 7] = OT;
                        excSheet.Cells[71, 8] =  -Secur_premium(empid, 2, "Jun"); 
                        excSheet.Cells[71, 9].Formula = "=-D55";
                        excSheet.Cells[71, 10].Formula = "=SUM(B71:I71)";
                        excSheet.Cells[72, 1] = "7/1/2021";
                        excSheet.Cells[72, 2] = Base;
                        excSheet.Cells[72, 3] = Executive;
                        excSheet.Cells[72, 4] = Travel;
                        excSheet.Cells[72, 5] = housing;
                        excSheet.Cells[72, 6] = Language;
                        excSheet.Cells[72, 7] = OT;
                        excSheet.Cells[72, 8] = -Secur_premium(empid, 2, "Jul"); 
                        excSheet.Cells[72, 9].Formula = "=-D56";
                        excSheet.Cells[72, 10].Formula = "=SUM(B72:I72)";
                        excSheet.Cells[73, 1] = "8/1/2021";
                        excSheet.Cells[73, 2] = Base;
                        excSheet.Cells[73, 3] = Executive;
                        excSheet.Cells[73, 4] = Travel;
                        excSheet.Cells[73, 5] = housing;
                        excSheet.Cells[73, 6] = Language;
                        excSheet.Cells[73, 7] = OT;
                        excSheet.Cells[73, 8] = -Secur_premium(empid, 2, "Aug"); 
                        excSheet.Cells[73, 9].Formula = "=-D57";
                        excSheet.Cells[73, 10].Formula = "=SUM(B73:I73)";
                        excSheet.Cells[74, 1] = "9/1/2021";
                        excSheet.Cells[74, 2] = Base;
                        excSheet.Cells[74, 3] = Executive;
                        excSheet.Cells[74, 4] = Travel;
                        excSheet.Cells[74, 5] = housing;
                        excSheet.Cells[74, 6] = Language;
                        excSheet.Cells[74, 7] = OT;
                        excSheet.Cells[74, 8] = -Secur_premium(empid, 2, "Sep"); 
                        excSheet.Cells[74, 9].Formula = "=-D58";
                        excSheet.Cells[74, 10].Formula = "=SUM(B74:I74)";
                        excSheet.Cells[75, 1] = "10/1/2021";
                        excSheet.Cells[75, 2] = Base;
                        excSheet.Cells[75, 3] = Executive;
                        excSheet.Cells[75, 4] = Travel;
                        excSheet.Cells[75, 5] = housing;
                        excSheet.Cells[75, 6] = Language;
                        excSheet.Cells[75, 7] = OT;
                        excSheet.Cells[75, 8] = -Secur_premium(empid, 2, "Oct"); 
                        excSheet.Cells[75, 9].Formula = "=-D59";
                        excSheet.Cells[75, 10].Formula = "=SUM(B75:I75)";
                        excSheet.Cells[76, 1] = "11/1/2021";
                        excSheet.Cells[76, 2] = Base;
                        excSheet.Cells[76, 3] = Executive;
                        excSheet.Cells[76, 4] = Travel;
                        excSheet.Cells[76, 5] = housing;
                        excSheet.Cells[76, 6] = Language;
                        excSheet.Cells[76, 7] = OT;
                        excSheet.Cells[76, 8] = -Secur_premium(empid, 2, "Nov"); ;
                        excSheet.Cells[76, 9].Formula = "=-D60";
                        excSheet.Cells[76, 10].Formula = "=SUM(B76:I76)";
                        excSheet.Cells[77, 1] = "12/1/2021";
                        excSheet.Cells[77, 2] = Base;
                        excSheet.Cells[77, 3] = Executive;
                        excSheet.Cells[77, 4] = Travel;
                        excSheet.Cells[77, 5] = housing;
                        excSheet.Cells[77, 6] = Language;
                        excSheet.Cells[77, 7] = OT;
                        excSheet.Cells[77, 8] = -Secur_premium(empid, 2, "Dec"); 
                        excSheet.Cells[77, 9].Formula = "=-D61";
                        excSheet.Cells[77, 10].Formula = "=SUM(B77:I77)";
                        
                        excSheet.Cells[79, 1] = "合計";
                        excSheet.Cells[79, 2].Formula = "=SUM(B66:B78)";
                        excSheet.Cells[79, 3].Formula = "=SUM(C66:C78)";
                        excSheet.Cells[79, 4].Formula = "=SUM(D66:D78)";
                        excSheet.Cells[79, 5].Formula = "=SUM(E66:E78)";
                        excSheet.Cells[79, 6].Formula = "=SUM(F66:F78)";
                        excSheet.Cells[79, 7].Formula = "=SUM(G66:G78)";
                        excSheet.Cells[79, 8].Formula = "=SUM(H66:H78)";
                        excSheet.Cells[79, 9].Formula = "=SUM(I66:I78)";
                        excSheet.Cells[79, 10].Formula = "=SUM(J66:J78)";
                        

                        //format excel
                        excSheet.get_Range("A3", "D20").Cells.Borders.Weight = XlBorderWeight.xlThin;
                        excSheet.get_Range("A23", "D32").Cells.Borders.Weight = XlBorderWeight.xlThin;
                        excSheet.get_Range("A34", "D34").Cells.Borders.Weight = XlBorderWeight.xlThin;
                        excSheet.get_Range("A37", "D46").Cells.Borders.Weight = XlBorderWeight.xlThin;
                        excSheet.get_Range("A49", "D62").Cells.Borders.Weight = XlBorderWeight.xlThin;
                        excSheet.get_Range("A65", "J79").Cells.Borders.Weight = XlBorderWeight.xlThin;
                        excSheet.Range["A:A"].ColumnWidth = 30;
                        excSheet.Range["B:J"].ColumnWidth = 15;
                        excSheet.Range["A3:D3"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        excSheet.Range["A23:D23"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        excSheet.Range["A37:D37"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        excSheet.Range["A49:D49"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        excSheet.Range["A65:J65"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        excSheet.Range["B38:B61"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        excSheet.get_Range("A3", "D3").Interior.Color = Color.FromArgb(217, 217, 217);
                        excSheet.get_Range("A23", "D23").Interior.Color = Color.FromArgb(217, 217, 217);
                        excSheet.get_Range("A37", "D37").Interior.Color = Color.FromArgb(217, 217, 217);
                        excSheet.get_Range("A49", "D49").Interior.Color = Color.FromArgb(217, 217, 217);
                        excSheet.get_Range("A65", "J65").Interior.Color = Color.FromArgb(217, 217, 217);
                        excSheet.get_Range("A4", "D5").Interior.Color = Color.FromArgb(220, 230, 241);
                        excSheet.get_Range("A6", "D6").Interior.Color = Color.FromArgb(253, 233, 217);
                        excSheet.get_Range("A7", "D8").Interior.Color = Color.FromArgb(220, 230, 241);
                        excSheet.get_Range("A9", "D11").Interior.Color = Color.FromArgb(253, 233, 217);
                        excSheet.get_Range("A12", "D13").Interior.Color = Color.FromArgb(220, 230, 241);
                        excSheet.get_Range("A14", "D15").Interior.Color = Color.FromArgb(253, 233, 217);
                        excSheet.get_Range("A16", "D17").Interior.Color = Color.FromArgb(220, 230, 241);
                        excSheet.get_Range("A18", "D19").Interior.Color = Color.FromArgb(216, 228, 188);
                        excSheet.get_Range("A20", "D20").Interior.Color = Color.FromArgb(242, 220, 219);
                        excSheet.get_Range("A34", "D34").Interior.Color = Color.FromArgb(242, 220, 219);
                        excSheet.get_Range("A46", "D46").Interior.Color = Color.FromArgb(242, 220, 219);
                        excSheet.get_Range("A62", "D62").Interior.Color = Color.FromArgb(242, 220, 219);
                        excSheet.get_Range("A79", "J79").Interior.Color = Color.FromArgb(242, 220, 219);
                        //excSheet.Range["B4:J79"].NumberFormat = "#,##0.00";
                        excSheet.Range["B4:D34"].NumberFormat = "_(* #,##0.00_);_(* (#,##0.00);_(* \"-\"_);_(@_)";
                        excSheet.Range["C38:D62"].NumberFormat = "_(* #,##0.00_);_(* (#,##0.00);_(* \"-\"_);_(@_)";
                        excSheet.Range["B66:J79"].NumberFormat = "_(* #,##0.00_);_(* (#,##0.00);_(* \"-\"_);_(@_)";




                }
                excBook.SaveAs(@"C:\MQATaxWithholding"+timef+".xlsx");
                excBook.Close();
                
            }
           
        }
        static int CountEmpidSalary(int empid,int num)
        {
            int i = 0;
            int n = 0;
            string month ;
            int cmonth = 0;
            if(num == 1)
            {
               month = "AND ( MONTH(Pay_Date) BETWEEN 1 AND 5 )";
                cmonth = 5;

            }
            else if (num == 2)
            {
                month = "AND ( MONTH(Pay_Date) BETWEEN 6 AND 12 )";
                cmonth = 7;
            }
            else
            {
                month = " ";
            }

            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string SQL_Text = "SELECT  * FROM Net_Salary WHERE EMP_ID = " + empid + month;

            System.Data.DataTable tbl = new System.Data.DataTable();
            OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
            adapter.Fill(tbl);
            string tr = DateTime.Today.ToString("MM");
            int rt = int.Parse(tr);
            foreach (DataRow dr in tbl.Rows)
            {
                i++;
            }
            
            if (i<cmonth && rt>i)
            {
                Console.WriteLine("the "+i);
                i = cmonth;
            }
            return i;
        }
        static int Secur_premium(int empid, int num,string month)
        {
            int i = 0;
            int n = 0;
            int ms = 0;
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string SQL_Text = "SELECT  * FROM Social_Security  WHERE Social_Security_ID = " + empid;

            System.Data.DataTable tbl = new System.Data.DataTable();
            OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
            adapter.Fill(tbl);
            foreach (DataRow dr in tbl.Rows)
            {


                if (int.Parse(dr[1].ToString()) != 0)
                {
                    n += int.Parse(dr[1].ToString());
                    i++;
                    if (month.Contains("Jan"))
                    {
                        ms = int.Parse(dr[1].ToString());
                    }
                }
                if(int.Parse(dr[2].ToString()) != 0)
                {
                    n += int.Parse(dr[2].ToString());
                    i++;
                    if (month.Contains("Feb"))
                    {
                        ms = int.Parse(dr[2].ToString());
                    }
                }
                if(int.Parse(dr[3].ToString()) != 0)
                {
                    n += int.Parse(dr[3].ToString());
                    i++;
                    if (month.Contains("Mar"))
                    {
                        ms = int.Parse(dr[3].ToString());
                    }
                }
                if(int.Parse(dr[4].ToString()) != 0)
                {
                    n += int.Parse(dr[4].ToString());
                    i++;
                    if (month.Contains("Apr"))
                    {
                        ms = int.Parse(dr[4].ToString());
                    }
                }
                if(int.Parse(dr[5].ToString()) != 0)
                {
                    n += int.Parse(dr[5].ToString());
                    i++;
                    if (month.Contains("May"))
                    {
                        ms = int.Parse(dr[5].ToString());
                    }
                   
                }
                if(int.Parse(dr[6].ToString()) != 0)
                {
                    n += int.Parse(dr[6].ToString());
                    i++;
                    if (month.Contains("Jun"))
                    {
                        ms = int.Parse(dr[6].ToString());
                    }
                }
                if(int.Parse(dr[7].ToString()) != 0)
                {
                    n += int.Parse(dr[7].ToString());
                    i++;
                    if (month.Contains("Jul"))
                    {
                        ms = int.Parse(dr[7].ToString());
                    }
                }
                if(int.Parse(dr[8].ToString()) != 0)
                {
                    n += int.Parse(dr[8].ToString());
                    i++;
                    if (month.Contains("Aug"))
                    {
                        ms = int.Parse(dr[8].ToString());
                    }
                }
                if(int.Parse(dr[9].ToString()) != 0)
                {
                    n += int.Parse(dr[9].ToString());
                    i++;
                    if (month.Contains("Sep"))
                    {
                        ms = int.Parse(dr[9].ToString());
                    }
                }
                if(int.Parse(dr[10].ToString()) != 0)
                {
                    n += int.Parse(dr[10].ToString());
                    i++;
                    if (month.Contains("Oct"))
                    {
                        ms = int.Parse(dr[10].ToString());
                    }
                }
                if(int.Parse(dr[11].ToString()) != 0)
                {
                    n += int.Parse(dr[11].ToString());
                    i++;
                    if (month.Contains("Nov"))
                    {
                        ms = int.Parse(dr[11].ToString());
                    }
                }
                if (int.Parse(dr[12].ToString()) != 0)
                {
                    n += int.Parse(dr[12].ToString());
                    i++;
                    if (month.Contains("Dec"))
                    {
                        ms = int.Parse(dr[12].ToString());
                    }
                }

            }
            if (num == 1)
            {
                return n;
            }
            else if(num == 2)
            {
                return ms;
            }
            else
            {
                return i;
            }
            
        }
        static int TravalAllow(int empid,int count)
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string SQL_Text = "SELECT * FROM Net_Salary WHERE EMP_ID = "+empid;
            int traval = 0;
            int maintraval = 0;
            int i = 0;
            int l = 0;
            System.Data.DataTable tbl = new System.Data.DataTable();
            OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
            adapter.Fill(tbl);
            foreach (DataRow dr in tbl.Rows)
            {
                
                if(maintraval < int.Parse(dr[5].ToString()))
                {
                    maintraval = int.Parse(dr[5].ToString());
                }
                if(maintraval== int.Parse(dr[5].ToString()))
                {
                    traval += int.Parse(dr[5].ToString());
                    i++;
                }
                else if (maintraval != int.Parse(dr[5].ToString()))
                {
                    traval += int.Parse(dr[5].ToString());
                    l++;
                }
                
                
                Console.WriteLine(i);
                
            }
            int u = 0;
            if(i != 12)
            {
                for(int t = (l + i +1); t <= 12; t++)
                {
                    traval += maintraval;
                    u++;
                    Console.WriteLine("tttt"+t);
                }
            }
          if(count == 1)
            {
                return i+u;
            }
            else
            {
                return traval;
            }
            
        }
    }
}
