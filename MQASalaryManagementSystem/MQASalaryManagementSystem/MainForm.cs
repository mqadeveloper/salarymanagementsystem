﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MQASalaryManagementSystem
{
    public partial class MainForm : Form
    {
        private string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public MainForm()
        {
            InitializeComponent();
            IDbConnection cnn = new OleDbConnection(con);
            try
            {
                cnn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void butEmp_Click(object sender, EventArgs e)
        {
            EmployeeForm emp = new EmployeeForm();
            
            emp.TopLevel = false;
            emp.WindowState = FormWindowState.Maximized;
            panelcontrol.Controls.Add(emp);
            emp.BringToFront();
            emp.Show();
            butSocial.BackColor = Color.Transparent;
            butEmp.BackColor = Color.LightCoral;
            butSalary.BackColor = Color.Transparent;
            butworkday.BackColor = Color.Transparent;
            butMonthly.BackColor = Color.Transparent;
        }

        private void butSocial_Click(object sender, EventArgs e)
        {
            SocialForm social = new SocialForm();
            social.TopLevel = false;
            social.WindowState = FormWindowState.Maximized;
            panelcontrol.Controls.Add(social);
            social.BringToFront();
            social.Show();

            butSocial.BackColor = Color.LightCoral;
            butEmp.BackColor = Color.Transparent;
            butSalary.BackColor = Color.Transparent;
            butworkday.BackColor = Color.Transparent;
            butMonthly.BackColor = Color.Transparent;
        }

        private void butSalary_Click(object sender, EventArgs e)
        {
            SalaryForm salaryf = new SalaryForm();
            salaryf.TopLevel = false;
            salaryf.WindowState = FormWindowState.Maximized;
            panelcontrol.Controls.Add(salaryf);
            salaryf.BringToFront();
            salaryf.Show();
            butSocial.BackColor = Color.Transparent;
            butEmp.BackColor = Color.Transparent;
            butSalary.BackColor = Color.LightCoral;
            butworkday.BackColor = Color.Transparent;
            butMonthly.BackColor = Color.Transparent;


        }

        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            if (butEmp.BackColor == Color.LightCoral)
            {
                if (this.WindowState == FormWindowState.Maximized)
                {
                    EmployeeForm emp = new EmployeeForm();
                    emp.TopLevel = false;
                    emp.WindowState = FormWindowState.Maximized;
                    panelcontrol.Controls.Add(emp);
                    emp.BringToFront();
                    emp.Show();
                }
                else if (this.WindowState == FormWindowState.Normal)
                {
                    EmployeeForm emp = new EmployeeForm();
                    emp.TopLevel = false;
                    emp.WindowState = FormWindowState.Normal;
                    panelcontrol.Controls.Add(emp);
                    emp.BringToFront();
                    emp.Show();
                }

            }
            else if (butSocial.BackColor == Color.LightCoral)
            {
                if (this.WindowState == FormWindowState.Maximized)
                {
                    SocialForm social = new SocialForm();
                    social.TopLevel = false;
                    social.WindowState = FormWindowState.Maximized;
                    panelcontrol.Controls.Add(social);
                    social.BringToFront();
                    social.Show();
                }
                else if (this.WindowState == FormWindowState.Normal)
                {
                    SocialForm social = new SocialForm();
                    social.TopLevel = false;
                    social.WindowState = FormWindowState.Normal;
                    panelcontrol.Controls.Add(social);
                    social.BringToFront();
                    social.Show();
                }
            }
            else if (butSalary.BackColor == Color.LightCoral)
            {
                if (this.WindowState == FormWindowState.Maximized)
                {
                    SalaryForm salaryf = new SalaryForm();
                    salaryf.TopLevel = false;
                    salaryf.WindowState = FormWindowState.Maximized;
                    panelcontrol.Controls.Add(salaryf);
                    salaryf.BringToFront();
                    salaryf.Show();
                }
                else if (this.WindowState == FormWindowState.Normal)
                {
                    SalaryForm salaryf = new SalaryForm();
                    salaryf.TopLevel = false;
                    salaryf.WindowState = FormWindowState.Normal;
                    panelcontrol.Controls.Add(salaryf);
                    salaryf.BringToFront();
                    salaryf.Show();
                }
            }

        }

        private void butworkday_Click(object sender, EventArgs e)
        {
            WorkdayForm workday = new WorkdayForm();
            workday.TopLevel = false;
            workday.WindowState = FormWindowState.Maximized;
            panelcontrol.Controls.Add(workday);
            workday.BringToFront();
            workday.Show();
            butSocial.BackColor = Color.Transparent;
            butEmp.BackColor = Color.Transparent;
            butSalary.BackColor = Color.Transparent;
            butworkday.BackColor = Color.LightCoral;
            butMonthly.BackColor = Color.Transparent;
        }

        private void butMonthly_Click(object sender, EventArgs e)
        {
            MonthlyHolidaysForm monthlyH = new MonthlyHolidaysForm();
            monthlyH.TopLevel = false;
            monthlyH.WindowState = FormWindowState.Maximized;
            panelcontrol.Controls.Add(monthlyH);
            monthlyH.BringToFront();
            monthlyH.Show();
            butSocial.BackColor = Color.Transparent;
            butEmp.BackColor = Color.Transparent;
            butSalary.BackColor = Color.Transparent;
            butworkday.BackColor = Color.Transparent;
            butMonthly.BackColor = Color.LightCoral;
            
        }
    }
}
