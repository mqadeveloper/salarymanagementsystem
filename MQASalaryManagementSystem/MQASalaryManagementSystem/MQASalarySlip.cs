﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;



namespace MQASalaryManagementSystem
{
    class MQASalarySlip
    {
        public static void MQASalarySlipEx()
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            DateTime time = DateTime.Now;
            string filenametime = time.ToString("yyyyMM");
            Application excApp;
            Workbook excBook;
            Worksheet excSheet;
            Worksheet excSheet2;
            Worksheet newSheet;
            Range rg;
            

            excApp = new Application();
            excApp.Visible = false;
            excApp.UserControl = false;
            excBook = (Workbook)(excApp.Workbooks.Add(Missing.Value));
            excSheet = (Worksheet)excBook.ActiveSheet;
            excSheet.Name = "Master";
            excSheet.Cells[1, 1] = "ここの日付を変更すると全ての給与明細シートに反映されます";
            excSheet.Cells[2, 1] = "給与年月";
            excSheet.Cells[2, 2] = "4/2021";
            excSheet.Cells[3, 1] = "給与年月";
            excSheet.Cells[3, 2] = "支払い日";
            excSheet.Cells[5, 1] = "必ずTrueになっていることを確認すること";
            // ---------------------------------------------- excSheet -------------------------------------------

            excSheet2 = (Worksheet)excBook.Worksheets.Add();
            excSheet2.Name = "給与明細";
            excSheet2.Cells[1, 1] = "MQA給与明細";
            excSheet2.Cells[3, 1] = " 支給年月日 ";
            excSheet2.Cells[3, 2] = " 社員番号 ";
            excSheet2.Cells[3, 3] = " 氏名 ";
            excSheet2.Cells[3, 5] = " 基本給 ";
            excSheet2.Cells[3, 6] = " 役職手当 ";
            excSheet2.Cells[3, 7] = " その他手当 ";
            excSheet2.Cells[3, 8] = " 通勤手当 ";
            excSheet2.Cells[3, 9] = " 住宅手当 ";
            excSheet2.Cells[3, 10] = " 語学手当 ";
            excSheet2.Cells[3, 11] = " 手当合計 ";
            excSheet2.Cells[3, 12] = " 支給総額 ";
            excSheet2.Cells[3, 13] = " 社会保険料 ";
            excSheet2.Cells[3, 14] = " 給与所得税 ";
            excSheet2.Cells[3, 15] = " 控除総額 ";
            excSheet2.Cells[3, 16] = " 差引支給額 ";

            //format excSheet2
            excSheet2.get_Range("A3", "P3").Interior.Color = Color.FromArgb(191, 191, 191);

            using (IDbConnection cnn = new OleDbConnection(con))

            {
                string month = DateTime.Today.ToString("MM");
                string year = DateTime.Today.ToString("yyyy");
                string SQL_Text = "SELECT Employees.EMP_ID,Employees.EMP_FristName,Employees.EMP_LastName, Employees.EMP_NickName,Employees.Job_position, Net_Salary.* ";
                SQL_Text += "FROM Net_Salary INNER JOIN Employees ON Employees.EMP_ID = Net_Salary.Net_Salary_ID ";
                SQL_Text += "WHERE year(Pay_Date) = "+year+" AND MONTH(Pay_Date) = "+month;
                System.Data.DataTable tbl = new System.Data.DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
                adapter.Fill(tbl);
                int i = 3;

                foreach (DataRow dr in tbl.Rows)
                {
                    int empid = int.Parse(dr[0].ToString());
                    string Fname = dr[1].ToString();
                    string Lname = dr[2].ToString();
                    string Nname = dr[3].ToString();
                    string Position = dr[4].ToString();
                    int Base = int.Parse(dr[8].ToString());
                    int Executive = int.Parse(dr[9].ToString());
                    int Travel = int.Parse(dr[10].ToString());
                    int housing = int.Parse(dr[11].ToString());
                    int Language = int.Parse(dr[12].ToString());
                    int Bonus = int.Parse(dr[13].ToString());
                    int OT = int.Parse(dr[14].ToString());
                    int totalsalary = int.Parse(dr[15].ToString());
                    int salarytax = int.Parse(dr[16].ToString());
                    int insurance = int.Parse(dr[17].ToString());
                    int netsalary = int.Parse(dr[18].ToString());
                    string sdate = dr[19].ToString();

                    i++;


                    rg = excSheet2.get_Range("A" + i.ToString());
                    rg.NumberFormat = "yyyy/MM/dd";
                    excSheet2.Cells[i, 1] = sdate;
                    excSheet2.Cells[i, 2] = empid;
                    excSheet2.Cells[i, 3] = Fname + " " + Lname;
                    excSheet2.Cells[i, 4] = Nname;
                    excSheet2.Cells[i, 5] = Base;
                    excSheet2.Cells[i, 6] = Executive;
                    excSheet2.Cells[i, 7] = OT;
                    excSheet2.Cells[i, 8] = Travel;
                    excSheet2.Cells[i, 9] = housing;
                    excSheet2.Cells[i, 10] = Language;
                    excSheet2.Cells[i, 11].Formula = "=SUM(F"+i+":J" + i + ")";
                    excSheet2.Cells[i, 12] = totalsalary;
                    excSheet2.Cells[i, 13] = insurance;
                    excSheet2.Cells[i, 14] = salarytax;
                    excSheet2.Cells[i, 15].Formula = "=SUM(M" + i + ":N" + i + ")";
                    excSheet2.Cells[i, 16] = netsalary;
                    excSheet2.Cells[i + 1, 3] = "合計";
                    excSheet2.Cells[i + 1, 5].Formula = "=SUM(E4:E" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 6].Formula = "=SUM(F4:F" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 7].Formula = "=SUM(G4:G" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 8].Formula = "=SUM(H4:H" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 9].Formula = "=SUM(I4:I" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 10].Formula = "=SUM(J4:J" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 11].Formula = "=SUM(K4:K" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 12].Formula = "=SUM(L4:L" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 13].Formula = "=SUM(M4:M" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 14].Formula = "=SUM(N4:N" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 15].Formula = "=SUM(O4:O" + i.ToString() + ")";
                    excSheet2.Cells[i + 1, 16].Formula = "=SUM(P4:P" + i.ToString() + ")";

                    //excel format
                    excSheet2.Range["A3"].Columns.AutoFit();
                    excSheet2.Range["B3"].Columns.AutoFit();
                    excSheet2.Range["E3"].Columns.AutoFit();
                    excSheet2.Range["F3"].Columns.AutoFit();
                    excSheet2.Range["G3"].Columns.AutoFit();
                    excSheet2.Range["H3"].Columns.AutoFit();
                    excSheet2.Range["I3"].Columns.AutoFit();
                    excSheet2.Range["J3"].Columns.AutoFit();
                    excSheet2.Range["K3"].Columns.AutoFit();
                    excSheet2.Range["L3"].Columns.AutoFit();
                    excSheet2.Range["M3"].Columns.AutoFit();
                    excSheet2.Range["N3"].Columns.AutoFit();
                    excSheet2.Range["O3"].Columns.AutoFit();
                    excSheet2.Range["P3"].Columns.AutoFit();
                    excSheet2.Range["C:C"].ColumnWidth = 30;
                    excSheet2.Range["A3:A" + i.ToString()].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    excSheet2.Range["B3:B" + i.ToString()].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    excSheet2.Range["C3"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    excSheet2.Range["E:P"].NumberFormat = "_(* #,##0.00_);_(* (#,##0.00);_(* \"-\"_);_(@_)";
                    excSheet2.Range["C3:D3"].Merge();
                    excSheet2.get_Range("A3", "P" + i.ToString()).Cells.Borders.Weight = XlBorderWeight.xlThin;
                    excSheet2.Range["A" + (i + 1), "P" + i].Borders.Weight = XlBorderWeight.xlThin;







                }


               
                foreach (DataRow dr in tbl.Rows)
                {
                    int empid = int.Parse(dr[0].ToString());
                    string Fname = dr[1].ToString();
                    string Lname = dr[2].ToString();
                    string Nname = dr[3].ToString();
                    string Position = dr[4].ToString();
                    int Base = int.Parse(dr[8].ToString());
                    int Executive = int.Parse(dr[9].ToString());
                    int Travel = int.Parse(dr[10].ToString());
                    int housing = int.Parse(dr[11].ToString());
                    int Language = int.Parse(dr[12].ToString());
                    int Bonus = int.Parse(dr[13].ToString());
                    int OT = int.Parse(dr[14].ToString());
                    int totalsalary = int.Parse(dr[15].ToString());
                    int salarytax = int.Parse(dr[16].ToString());
                    int insurance = int.Parse(dr[17].ToString());
                    int netsalary = int.Parse(dr[18].ToString());
                    string sdate = ((DateTime)dr[19]).ToString("yyyy/MM/dd");
                    newSheet = (Worksheet)excBook.Worksheets.Add();
                    newSheet.Name = Nname ;
                    newSheet.Cells[2,1] = "給与明細書/PAYSLIP";
                    newSheet.Cells[3, 4] = " for employee ";
                    newSheet.Cells[4, 1] = "会社名/Company name";
                    newSheet.Cells[4, 2] = "MapQuestAsia Co.,Ltd";
                    newSheet.Cells[5, 1] = "従業員名/Name of employee";
                    newSheet.Cells[5, 2] = empid;
                    newSheet.Cells[5, 3] = Fname + " " + Lname;
                    newSheet.Cells[6, 1] = "役職/Position";
                    newSheet.Cells[6, 2] = Position;
                    newSheet.Cells[7, 1] = "支払い日/Payment day";
                    newSheet.Cells[7, 2] = sdate;
                    newSheet.Cells[8, 1] = "支給内訳(THB)";
                    newSheet.Cells[8, 3] = "控除内訳(THB)";
                    newSheet.Cells[9, 1] = "基本給/Basic salary";
                    newSheet.Cells[9, 2] = Base;
                    newSheet.Cells[10, 1] = "資格手当/skills allowance";
                    newSheet.Cells[10, 2] = Executive;
                    newSheet.Cells[11, 1] = "通勤手当/Travel allowance";
                    newSheet.Cells[11, 2] = Travel;
                    newSheet.Cells[12, 1] = "住宅手当/Housing allowance";
                    newSheet.Cells[12, 2] = housing;
                    newSheet.Cells[13, 1] = "語学手当/Language allowance";
                    newSheet.Cells[13, 2] = Language; 
                    newSheet.Cells[14, 1] = "その他手当/Other ";
                    newSheet.Cells[14, 2] = OT + Bonus;
                    newSheet.Cells[9, 3] = "給与所得税/Salary Tax";
                    newSheet.Cells[9, 4] = salarytax;
                    newSheet.Cells[10, 3] = "社会保険料/Insurance";
                    newSheet.Cells[10, 4] = insurance;
                    newSheet.Cells[11, 3] = "その他控除/Other ";
                    newSheet.Cells[15, 1] = "支給総額";
                    newSheet.Cells[15, 2].Formula = "=SUM(B9:B14)";
                    newSheet.Cells[15, 3] = "控除総額";
                    newSheet.Cells[15, 4].Formula = "=SUM((D9:D14))";
                    newSheet.Cells[16, 1] = "差し引き支給額";
                    newSheet.Cells[16, 2] = netsalary;


                    newSheet.Cells[24,1] = "สำหรับบริษัท/PAYSLIP";
                    newSheet.Cells[25, 4] = "for employer";
                    newSheet.Cells[26, 1] = "ชื่อบริษัท/Company name";
                    newSheet.Cells[26, 2] = "MapQuestAsia Co.,Ltd";
                    newSheet.Cells[27, 1] = "ชื่อพนักงาน/Name of employee";
                    newSheet.Cells[27, 2] = empid;
                    newSheet.Cells[27, 3] = Fname + " " + Lname;
                    newSheet.Cells[28, 1] = "ตำแหน่ง/Position";
                    newSheet.Cells[28, 2] = Position;
                    newSheet.Cells[29, 1] = "วันที่จ่าย/Payment day";
                    newSheet.Cells[29, 2] = sdate;
                    newSheet.Cells[30, 1] = "รายละเอียดรายได้";
                    newSheet.Cells[30, 3] = "รายละเอียดการหัก";
                    newSheet.Cells[31, 1] = "เงินเดือน/Basic salary";
                    newSheet.Cells[31, 2] = Base;
                    newSheet.Cells[32, 1] = "資格手当/skills allowance";
                    newSheet.Cells[32, 2] = Executive;
                    newSheet.Cells[33, 1] = "ค่าเดินทาง/Travel allowance";
                    newSheet.Cells[33, 2] = Travel;
                    newSheet.Cells[34, 1] = "ค่าที่อยู่อาศัย/Housing allowance";
                    newSheet.Cells[34, 2] = housing;
                    newSheet.Cells[35, 1] = "ค่าภาษา/Language allowance";
                    newSheet.Cells[35, 2] = Language;
                    newSheet.Cells[36, 1] = "อื่นๆ/Other ";
                    newSheet.Cells[36, 2] = OT + Bonus;
                    newSheet.Cells[31, 3] = "ภาษีเงินได้/Salary Tax";
                    newSheet.Cells[31, 4] = salarytax;
                    newSheet.Cells[32, 3] = "ประกันสังคม/Insurance";
                    newSheet.Cells[32, 4] = insurance;
                    newSheet.Cells[33, 3] = "อื่นๆ/Other  ";
                    newSheet.Cells[37, 1] = "รวมรายได้";
                    newSheet.Cells[37, 2].Formula = "=SUM(B31:B36)";
                    newSheet.Cells[37, 3] = "รวมยอดหัก";
                    newSheet.Cells[37, 4].Formula = "=SUM(D31:D36)";
                    newSheet.Cells[38, 1] = "รวมรายได้สุทธิ";
                    newSheet.Cells[38, 2] = netsalary;
                    newSheet.Cells[40, 1] = "ลายเซ็นลูกจ้าง/Signature of employee";
                    newSheet.Cells[41, 1] = "วันที่/Date";
                    


                    // format cell employees
                    newSheet.Range["B4:D4"].Merge();
                    newSheet.Range["B6:D6"].Merge();
                    newSheet.Range["A8:B8"].Merge();
                    newSheet.Range["C8:D8"].Merge();
                    newSheet.Range["B7:D7"].Merge();
                    newSheet.Range["A2:D2"].Merge();
                    newSheet.Range["A2"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    newSheet.Range["A8"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    newSheet.Range["C8"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    newSheet.Range["B4:D7"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    newSheet.get_Range("A8", "A15").Interior.Color = Color.FromArgb(197, 217, 241);
                    newSheet.get_Range("C8", "C15").Interior.Color = Color.FromArgb(221, 217, 196);
                    newSheet.get_Range("A1", "D1").Interior.Color = Color.FromArgb(191, 191, 191);
                    newSheet.get_Range("A16", "D16").Interior.Color = Color.FromArgb(191, 191, 191);
                    newSheet.HPageBreaks.Add(newSheet.Range["A42"]);
                    newSheet.Range["B9:D16"].NumberFormat = "_(* #,##0.00_);_(* (#,##0.00);_(* \"-\"_);_(@_)";
                    newSheet.get_Range("A2").Font.Bold = true;
                    newSheet.get_Range("D3").Font.Bold = true;
                    newSheet.get_Range("A8","C8").Font.Bold = true;
                    newSheet.get_Range("A16","B16").Font.Bold = true;

                    // format cell employeer
                    newSheet.Range["B26:D26"].Merge();
                    newSheet.Range["A30:B30"].Merge();
                    newSheet.Range["C30:D30"].Merge();
                    newSheet.Range["B28:D28"].Merge();
                    newSheet.Range["B29:D29"].Merge();
                    newSheet.Range["A24:D24"].Merge();
                    newSheet.Range["A24"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    newSheet.Range["A30"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    newSheet.Range["C30"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    newSheet.Range["B26:D29"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    newSheet.get_Range("A30", "A37").Interior.Color = Color.FromArgb(197, 217, 241);
                    newSheet.get_Range("C30", "C37").Interior.Color = Color.FromArgb(221, 217, 196);
                    newSheet.get_Range("A23", "D23").Interior.Color = Color.FromArgb(191, 191, 191);
                    newSheet.get_Range("A38", "D38").Interior.Color = Color.FromArgb(191, 191, 191);
                    newSheet.Range["B31:D39"].NumberFormat = "_(* #,##0.00_);_(* (#,##0.00);_(* \"-\"_);_(@_)";
                    //format All 
                    newSheet.Range["A34"].Columns.AutoFit();
                    newSheet.Range["C10"].Columns.AutoFit();
                    newSheet.Range["D3"].Columns.AutoFit();
                    newSheet.get_Range("A4", "D16").Cells.Borders.Weight = XlBorderWeight.xlThin;
                    newSheet.get_Range("A26", "D38").Cells.Borders.Weight = XlBorderWeight.xlThin;
                    newSheet.Range["A21:D21"].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlDashDotDot;
                    newSheet.Range["C40"].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlDot;
                    newSheet.Range["C41"].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlDot;
                    newSheet.Range["D1:D42"].Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlThick;
                    newSheet.Range["A42:D42"].Borders[XlBordersIndex.xlEdgeBottom].Weight = XlBorderWeight.xlThick;
                    newSheet.get_Range("A24").Font.Bold = true;
                    newSheet.get_Range("D25").Font.Bold = true;
                    newSheet.get_Range("A30", "C30").Font.Bold = true;
                    newSheet.get_Range("A38", "B38").Font.Bold = true;
                    newSheet.Move(excBook.Sheets[1]);

                }
                excSheet2.Move(excBook.Sheets[1]);
                excSheet.Move(excBook.Sheets[1]);
            }


            // Save file
            excBook.SaveAs(@"C:\MQASalarySlip"+ filenametime + ".xlsx");
            excBook.Close();



        }
    }
}
