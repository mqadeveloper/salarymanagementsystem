﻿using System;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace MQASalaryManagementSystem
{
    class MQASalarySlipWithTemplate
    {
        public static void MQASalarySlipWithTemplateEX()
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            DateTime time = DateTime.Now;
            string filenametime = time.ToString("yyyyMM");
            Application excApp;
            Workbook excBook;
            Worksheet excSheet, excSheet2, excSheet3 = null, excSheetnew;
            Range rg;
            excApp = new Application();
            excApp.Visible = false;
            excApp.UserControl = false;
            string templateFile = "MQASalarySlip202105.xlsx";
            string template = Path.Combine(Environment.CurrentDirectory, @"template\", templateFile);
            excBook = (Workbook)excApp.Workbooks.Add(template);
            int y = 0;

            using (IDbConnection cnn = new OleDbConnection(con))

            {
                string month = DateTime.Today.ToString("MM");
                string year = DateTime.Today.ToString("yyyy");
                string SQL_Text = "SELECT Employees.EMP_ID,Employees.EMP_FristName,Employees.EMP_LastName, Employees.EMP_NickName,Employees.Job_position, Net_Salary.* ";
                SQL_Text += "FROM Net_Salary INNER JOIN Employees ON Employees.EMP_ID = Net_Salary.Net_Salary_ID ";
                SQL_Text += "WHERE year(Pay_Date) = " + SalaryForm.comboYear + " AND MONTH(Pay_Date) = " + SalaryForm.comboMonth;
                SQL_Text += " ORDER BY Employees.EMP_ID ASC";
                System.Data.DataTable tbl = new System.Data.DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
                adapter.Fill(tbl);
                int i = 3;
                excSheet2 = excBook.Sheets[2];
                foreach (DataRow dr in tbl.Rows)
                {
                    int empid = int.Parse(dr[0].ToString());
                    string Fname = dr[1].ToString();
                    string Lname = dr[2].ToString();
                    string Nname = dr[3].ToString();
                    string Position = dr[4].ToString();
                    int Base = int.Parse(dr[8].ToString());
                    int Executive = int.Parse(dr[9].ToString());
                    int Travel = int.Parse(dr[10].ToString());
                    int housing = int.Parse(dr[11].ToString());
                    int Language = int.Parse(dr[12].ToString());
                    int Bonus = int.Parse(dr[13].ToString());
                    int OT = int.Parse(dr[14].ToString());
                    int totalsalary = int.Parse(dr[15].ToString());
                    int salarytax = int.Parse(dr[16].ToString());
                    int insurance = int.Parse(dr[17].ToString());
                    int netsalary = int.Parse(dr[18].ToString());
                    string sdate = dr[19].ToString();

                    i++;


                    rg = excSheet2.get_Range("A" + i.ToString());
                    rg.NumberFormat = "yyyy/MM/dd";
                    excSheet2.Cells[i, 1] = sdate;
                    excSheet2.Cells[i, 2] = empid;
                    excSheet2.Cells[i, 3] = Fname + " " + Lname;
                    excSheet2.Cells[i, 4] = Nname;
                    excSheet2.Cells[i, 5] = Base;
                    excSheet2.Cells[i, 6] = Executive;
                    excSheet2.Cells[i, 7] = OT;
                    excSheet2.Cells[i, 8] = Travel;
                    excSheet2.Cells[i, 9] = housing;
                    excSheet2.Cells[i, 10] = Language;

                    excSheet2.Cells[i, 12] = totalsalary;
                    excSheet2.Cells[i, 13] = insurance;
                    excSheet2.Cells[i, 14] = salarytax;

                    excSheet2.Cells[i, 16] = netsalary;
                }

                int ye = 0;
                foreach (DataRow dr in tbl.Rows)
                {
                    ye++;
                    int empid = int.Parse(dr[0].ToString());
                    string Fname = dr[1].ToString();
                    string Lname = dr[2].ToString();
                    string Nname = dr[3].ToString();
                    string Position = dr[4].ToString();
                    int Base = int.Parse(dr[8].ToString());
                    int Executive = int.Parse(dr[9].ToString());
                    int Travel = int.Parse(dr[10].ToString());
                    int housing = int.Parse(dr[11].ToString());
                    int Language = int.Parse(dr[12].ToString());
                    int Bonus = int.Parse(dr[13].ToString());
                    int OT = int.Parse(dr[14].ToString());
                    int totalsalary = int.Parse(dr[15].ToString());
                    int salarytax = int.Parse(dr[16].ToString());
                    int insurance = int.Parse(dr[17].ToString());
                    int netsalary = int.Parse(dr[18].ToString());
                    string sdate = ((DateTime)dr[19]).ToString("yyyy/MM/dd");

                    if (ye == 1)
                    {
                        excSheet3 = excBook.Sheets[3];
                        excSheet3.Name = Nname;
                        excSheet3.Cells[5, 2] = empid;
                        excSheet3.Cells[5, 3] = Fname + " " + Lname;
                        excSheet3.Cells[6, 2] = Position;
                        excSheet3.Cells[7, 2] = sdate;
                        excSheet3.Cells[9, 2] = Base;
                        excSheet3.Cells[10, 2] = Executive;
                        excSheet3.Cells[11, 2] = Travel;
                        excSheet3.Cells[12, 2] = housing;
                        excSheet3.Cells[13, 2] = Language;
                        excSheet3.Cells[14, 2] = OT + Bonus;
                        excSheet3.Cells[9, 4] = salarytax;
                        excSheet3.Cells[10, 4] = insurance;
                        excSheet3.Cells[16, 2] = netsalary;
                        excSheet3.Cells[27, 2] = empid;
                        excSheet3.Cells[27, 3] = Fname + " " + Lname;
                        excSheet3.Cells[28, 2] = Position;
                        excSheet3.Cells[29, 2] = sdate;
                        excSheet3.Cells[31, 2] = Base;
                        excSheet3.Cells[32, 2] = Executive;
                        excSheet3.Cells[33, 2] = Travel;
                        excSheet3.Cells[34, 2] = housing;
                        excSheet3.Cells[35, 2] = Language;
                        excSheet3.Cells[36, 2] = OT + Bonus;
                        excSheet3.Cells[31, 4] = salarytax;
                        excSheet3.Cells[32, 4] = insurance;
                        excSheet3.Cells[38, 2] = netsalary;



                    }
                    else
                    {
                        excSheet3.Copy(Type.Missing, excBook.Sheets[excBook.Sheets.Count]);
                        excSheetnew = excBook.Sheets[excBook.Sheets.Count];
                        excSheetnew.Name = Nname;
                        excSheetnew.Cells[5, 2] = empid;
                        excSheetnew.Cells[5, 3] = Fname + " " + Lname;
                        excSheetnew.Cells[6, 2] = Position;
                        excSheetnew.Cells[7, 2] = sdate;
                        excSheetnew.Cells[9, 2] = Base;
                        excSheetnew.Cells[10, 2] = Executive;
                        excSheetnew.Cells[11, 2] = Travel;
                        excSheetnew.Cells[12, 2] = housing;
                        excSheetnew.Cells[13, 2] = Language;
                        excSheetnew.Cells[14, 2] = OT + Bonus;
                        excSheetnew.Cells[9, 4] = salarytax;
                        excSheetnew.Cells[10, 4] = insurance;
                        excSheetnew.Cells[16, 2] = netsalary;
                        excSheetnew.Cells[27, 2] = empid;
                        excSheetnew.Cells[27, 3] = Fname + " " + Lname;
                        excSheetnew.Cells[28, 2] = Position;
                        excSheetnew.Cells[29, 2] = sdate;
                        excSheetnew.Cells[31, 2] = Base;
                        excSheetnew.Cells[32, 2] = Executive;
                        excSheetnew.Cells[33, 2] = Travel;
                        excSheetnew.Cells[34, 2] = housing;
                        excSheetnew.Cells[35, 2] = Language;
                        excSheetnew.Cells[36, 2] = OT + Bonus;
                        excSheetnew.Cells[31, 4] = salarytax;
                        excSheetnew.Cells[32, 4] = insurance;
                        excSheetnew.Cells[38, 2] = netsalary;
                    }




                }


                excBook.SaveAs(@"C:\MQASalarySlip" + SalaryForm.comboYear + "" + SalaryForm.comboMonth + ".xlsx");
                excBook.Close();
            }
        }
    }
}
