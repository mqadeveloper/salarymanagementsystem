﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQASalaryManagementSystem
{
    class MQASalaryListWithTemplate
    {
        public static void SalaryListWithTemplateEx()
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            DateTime time = DateTime.Now;
            string filenametime = time.ToString("yyyyMM");
            Application excApp;
            Workbook excBook;
            Worksheet excSheet, excSheet2, excSheet3 = null, excSheetnew;
            Range rg;
            excApp = new Application();
            excApp.Visible = false;
            excApp.UserControl = false;
            string templateFile = "MQASalaryList202105.xlsx";
            string template = Path.Combine(Environment.CurrentDirectory, @"template\", templateFile);
            
            excBook = (Workbook)excApp.Workbooks.Add(template);
            string tr = DateTime.Today.ToString("MM");
            int rt = int.Parse(SalaryForm.comboMonth);
            for (int o = 1; o <= rt; o++)
            {


                using (IDbConnection cnn = new OleDbConnection(con))

                {
                    string SQL_Text = "SELECT Employees.EMP_ID,Employees.EMP_FristName,Employees.EMP_LastName, Employees.EMP_NickName, Net_Salary.* ";
                    SQL_Text += "FROM Net_Salary INNER JOIN Employees ON Employees.EMP_ID = Net_Salary.Net_Salary_ID ";
                    SQL_Text += "WHERE MONTH(Pay_Date) = " + o+" AND YEAR(Pay_Date) = " + SalaryForm.comboYear;
                    SQL_Text += " ORDER BY Employees.EMP_ID ASC";
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
                    adapter.Fill(tbl);


                    int i = 3;
                    int y = 0;
                    excSheet = excBook.Sheets[1];
                    excSheet.Copy(Type.Missing, excBook.Sheets[excBook.Sheets.Count]);
                    excSheetnew = excBook.Sheets[excBook.Sheets.Count];



                    foreach (DataRow dr in tbl.Rows)
                    {
                        int empid = int.Parse(dr[0].ToString());
                        string Fname = dr[1].ToString();
                        string Lname = dr[2].ToString();
                        string Nname = dr[3].ToString();
                        int Base = int.Parse(dr[7].ToString());
                        int Executive = int.Parse(dr[8].ToString());
                        int Travel = int.Parse(dr[9].ToString());
                        int housing = int.Parse(dr[10].ToString());
                        int Language = int.Parse(dr[11].ToString());
                        int Bonus = int.Parse(dr[12].ToString());
                        int OT = int.Parse(dr[13].ToString());
                        int totalsalary = int.Parse(dr[14].ToString());
                        int salarytax = int.Parse(dr[15].ToString());
                        int insurance = int.Parse(dr[16].ToString());
                        int netsalary = int.Parse(dr[17].ToString());
                        string paydate = dr[18].ToString();
                        string salarydate = ((DateTime)dr[18]).ToString("yyyy.MM");

                        i++;
                        excSheetnew.Name = salarydate;
                        rg = excSheetnew.get_Range("A" + i.ToString());
                        rg.NumberFormat = "yyyy/MM/dd";
                        excSheetnew.Cells[i, 1] = paydate;
                        excSheetnew.Cells[i, 2] = empid;
                        excSheetnew.Cells[i, 3] = Fname + " " + Lname;
                        excSheetnew.Cells[i, 4] = Nname;
                        excSheetnew.Cells[i, 5] = Base;
                        excSheetnew.Cells[i, 6] = Executive;
                        excSheetnew.Cells[i, 7] = OT;
                        excSheetnew.Cells[i, 8] = Travel;
                        excSheetnew.Cells[i, 9] = housing;
                        excSheetnew.Cells[i, 10] = Language;

                        excSheetnew.Cells[i, 12] = Bonus;
                        excSheetnew.Cells[i, 13] = totalsalary;
                        excSheetnew.Cells[i, 14] = insurance;
                        excSheetnew.Cells[i, 15] = salarytax;

                        excSheetnew.Cells[i, 17] = netsalary;

                    }

                }

            }

            excBook.SaveAs(@"C:\MQASalarylist" + SalaryForm.comboYear + "" + SalaryForm.comboMonth + ".xlsx");
            excBook.Close();
        }
    }
}
