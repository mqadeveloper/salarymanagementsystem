﻿using Guna.UI2.WinForms.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MQASalaryManagementSystem
{
    public partial class EditSalaryForm : Form
    {
        private string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public EditSalaryForm()
        {
            InitializeComponent();
            ShowEditSalary();
        }
        private void ShowEditSalary()
        {
            using (IDbConnection cnn = new OleDbConnection(con))
            {

                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                string sql_Text = "SELECT  Employees.EMP_ID, Employees.EMP_NickName,Salary.Base_salary" +
                   ",Salary.Executive_Allowance,Salary.Travel_Allowance,Salary.housing_Allowance" +
                   ",Salary.Language_Allowance,Salary.Bonus,Salary.OT  " +
                   "FROM Salary INNER JOIN Employees ON Salary.Salary_ID = Employees.EMP_ID ";

                DataTable tbl = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, (OleDbConnection)cnn))
                {
                    try
                    {
                        adapter.Fill(tbl);
                        DataGridViewScrollHelper hScrollHelper = new DataGridViewScrollHelper(dataGridViewEditSalary, guna2HScrollBar1, true);
                        dataGridViewEditSalary.DataSource = tbl;
                        dataGridViewEditSalary.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dataGridViewEditSalary.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dataGridViewEditSalary.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dataGridViewEditSalary.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dataGridViewEditSalary.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dataGridViewEditSalary.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dataGridViewEditSalary.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                       
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void dataGridViewEditSalary_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                DataGridViewRow dgvRow = dataGridViewEditSalary.Rows[e.RowIndex];
                labelSalaryID.Text = dgvRow.Cells[0].Value.ToString();
                labelName.Text = dgvRow.Cells[1].Value.ToString();
                tbBaseSalary.Text = dgvRow.Cells[2].Value.ToString();
                tbExecutive.Text = dgvRow.Cells[3].Value.ToString();
                tbTraval.Text = dgvRow.Cells[4].Value.ToString();
                tbHosing.Text = dgvRow.Cells[5].Value.ToString();
                tbLanguage.Text = dgvRow.Cells[6].Value.ToString();
                tbBonus.Text = dgvRow.Cells[7].Value.ToString();
                tbOT.Text = dgvRow.Cells[8].Value.ToString();

            }
        }

        private void tbEditSalarySearch_TextChanged(object sender, EventArgs e)
        {
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                string sql_Text = "SELECT * FROM Salary";
                sql_Text += " WHERE Salary_ID LIKE '%" + tbEditSalarySearch.Text + "%'";
                sql_Text += " OR Base_salary LIKE '%" + tbEditSalarySearch.Text + "%'";
                sql_Text += " OR Executive_Allowance LIKE '%" + tbEditSalarySearch.Text + "%'";
                sql_Text += " OR Travel_Allowance LIKE '%" + tbEditSalarySearch.Text + "%'";
                sql_Text += " OR Housing_Allowance  LIKE '%" + tbEditSalarySearch.Text + "%'";
                sql_Text += " OR Bonus LIKE '%" + tbEditSalarySearch.Text + "%'";
                sql_Text += " OR OT  LIKE '%" + tbEditSalarySearch.Text + "%'";
                DataTable tbl = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, (OleDbConnection)cnn))
                {
                    adapter.Fill(tbl);
                    dataGridViewEditSalary.DataSource = tbl;
                }
            }
        }
        private void EditSalary(string Salary_ID)
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            IDbTransaction trans = null;
            using (IDbConnection cnn = new OleDbConnection(con))
            {


                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                string sql_Text = "UPDATE Salary SET Base_salary =" + tbBaseSalary.Text + ",Executive_Allowance = " + tbExecutive.Text
                    + ",Travel_Allowance =" + tbTraval.Text + ",housing_Allowance =" + tbHosing.Text + ",Language_Allowance =" + tbLanguage.Text
                    + ",Bonus = " + tbBonus.Text + ",OT =" + tbOT.Text + " WHERE Salary_ID = " + Salary_ID;


                using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                {
                    trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                    cmd_command.Connection = cnn;
                    cmd_command.Transaction = trans;

                    try
                    {
                        cmd_command.ExecuteNonQuery();
                        trans.Commit();
                        MessageBox.Show("Edit Complete", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (OleDbException ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }


            }
        }

        private void butEditSalary_Click(object sender, EventArgs e)
        {
            int x;
            if (string.IsNullOrEmpty(tbBaseSalary.Text))
            {
                MessageBox.Show("Please input employee Base Salary", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbBaseSalary.Text = "";
            }
            else if (!int.TryParse(tbBaseSalary.Text, out x))
            {
                MessageBox.Show("Base Salary is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbBaseSalary.Text = "";
            }
            else if (int.Parse(tbBaseSalary.Text) < 0)
            {
                MessageBox.Show("Base Salary is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbExecutive.Text))
            {
                MessageBox.Show("Please input employee Executive Alloance", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbExecutive.Text = "";
            }
            else if (int.Parse(tbExecutive.Text) < 0)
            {
                MessageBox.Show("Executive Alloance is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (!int.TryParse(tbExecutive.Text, out x))
            {
                MessageBox.Show("Executive Alloance is not number ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbExecutive.Text = "";
            }
            else if (string.IsNullOrEmpty(tbTraval.Text))
            {
                MessageBox.Show("Please input employee Traval Alloance", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbTraval.Text = "";
            }
            else if (!int.TryParse(tbTraval.Text, out x))
            {
                MessageBox.Show("Traval Alloance is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbTraval.Text = "";
            }
            else if (int.Parse(tbTraval.Text) < 0)
            {
                MessageBox.Show("Traval Alloance is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbHosing.Text))
            {
                MessageBox.Show("Please input employee Housing Alloance", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbHosing.Text = "";
            }
            else if (!int.TryParse(tbHosing.Text, out x))
            {
                MessageBox.Show("Housing Alloance is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbHosing.Text = "";
            }
            else if (int.Parse(tbHosing.Text) < 0)
            {
                MessageBox.Show("Housing Alloance is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbLanguage.Text))
            {
                MessageBox.Show("Please input employee Language Alloance", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbLanguage.Text = "";
            }
            else if (!int.TryParse(tbLanguage.Text, out x))
            {
                MessageBox.Show("Language Alloance is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbLanguage.Text = "";
            }
            else if (int.Parse(tbLanguage.Text) < 0)
            {
                MessageBox.Show("Language Alloance is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbBonus.Text))
            {
                MessageBox.Show("Please input employee Bonus", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbBonus.Text = "";
            }
            else if (!int.TryParse(tbBonus.Text, out x))
            {
                MessageBox.Show("Bonus is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbBonus.Text = "";
            }
            else if (int.Parse(tbBonus.Text) < 0)
            {
                MessageBox.Show("Bonus is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(tbOT.Text))
            {
                MessageBox.Show("Please input employee OT", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbOT.Text = "";
            }
            else if (!int.TryParse(tbOT.Text, out x))
            {
                MessageBox.Show("OT is not number", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbOT.Text = "";
            }
            else if (int.Parse(tbOT.Text) < 0)
            {
                MessageBox.Show("OT is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (!string.IsNullOrEmpty(labelSalaryID.Text))
                {
                    
                    DialogResult dialogResult = MessageBox.Show("Do you want to edit?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {

                        EditSalary(labelSalaryID.Text);
                        ShowEditSalary();
                    }
                }
                else
                {
                    MessageBox.Show("Please select employee", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
           

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
