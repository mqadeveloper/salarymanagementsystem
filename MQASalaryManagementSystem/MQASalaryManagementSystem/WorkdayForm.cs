﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MQASalaryManagementSystem
{
    public partial class WorkdayForm : Form
    {
        public WorkdayForm()
        {
            InitializeComponent();
            ComboBoxMY();
        }
        private string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        private void ComboBoxMY()
        {

            comboBoxM.DataSource = CultureInfo.InvariantCulture.DateTimeFormat.MonthNames.Take(12).ToList();
            comboBoxM.SelectedItem = CultureInfo.InvariantCulture.DateTimeFormat.MonthNames[DateTime.Now.AddMonths(-1).Month - 1];
            comboBoxY.DataSource = Enumerable.Range(2011, DateTime.Now.Year - 2011 + 1).ToList();
            comboBoxY.SelectedItem = DateTime.Now.Year;


        }
        public void ShowWorkDay()
        {
            using (IDbConnection cnn = new OleDbConnection(con))
            {
               

                string sql_Text = "SELECT Employees.EMP_ID,Employees.EMP_NickName,Work_Day.*";
                sql_Text += "FROM Work_Day INNER JOIN Employees ON Employees.EMP_ID = Work_Day.作業者NO ";
                sql_Text += "WHERE Work_Day.年月 = '" + comboBoxY.Text + ""+ ComboMonth(comboBoxM.Text) + "' ";
                sql_Text += " ORDER BY Employees.EMP_ID ASC";
                DataTable tbl = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, (OleDbConnection)cnn))
                {
                    adapter.Fill(tbl);
                    dataGridView1.DataSource = tbl;
                    
                }
            }
        }

        private void butShowWorkday_Click(object sender, EventArgs e)
        {
            ShowWorkDay();
        }
        private string ComboMonth(string month)
        {
            string mnum = "";
            if (comboBoxM.Text == "January")
            {
                mnum = "01";
            }
            else if (comboBoxM.Text == "February")
            {
                mnum = "02";
            }
            else if (comboBoxM.Text == "March")
            {
                mnum = "03";
            }
            else if (comboBoxM.Text == "April")
            {
                mnum = "04";
            }
            else if (comboBoxM.Text == "May")
            {
                mnum = "05";
            }
            else if (comboBoxM.Text == "June")
            {
                mnum = "06";
            }
            else if (comboBoxM.Text == "July")
            {
                mnum = "07";
            }
            else if (comboBoxM.Text == "August")
            {
                mnum = "08";
            }
            else if (comboBoxM.Text == "September")
            {
                mnum = "09";
            }
            else if (comboBoxM.Text == "October")
            {
                mnum = "10";
            }
            else if (comboBoxM.Text == "November")
            {
                mnum = "11";
            }
            else if (comboBoxM.Text == "December")
            {
                mnum = "12";
            }
            return mnum;
        }
    }
}
