﻿
namespace MQASalaryManagementSystem
{
    partial class EmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewEmp = new System.Windows.Forms.DataGridView();
            this.checkBoxOnlyActive = new System.Windows.Forms.CheckBox();
            this.butAddNewEmp = new System.Windows.Forms.Button();
            this.butEditEmp = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.guna2VScrollBar1 = new Guna.UI2.WinForms.Guna2VScrollBar();
            this.guna2HScrollBar1 = new Guna.UI2.WinForms.Guna2HScrollBar();
            this.butEditSalary = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmp)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewEmp
            // 
            this.dataGridViewEmp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewEmp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dataGridViewEmp.Location = new System.Drawing.Point(22, 120);
            this.dataGridViewEmp.Name = "dataGridViewEmp";
            this.dataGridViewEmp.ReadOnly = true;
            this.dataGridViewEmp.RowHeadersVisible = false;
            this.dataGridViewEmp.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewEmp.RowTemplate.ReadOnly = true;
            this.dataGridViewEmp.Size = new System.Drawing.Size(773, 313);
            this.dataGridViewEmp.TabIndex = 0;
            // 
            // checkBoxOnlyActive
            // 
            this.checkBoxOnlyActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxOnlyActive.AutoSize = true;
            this.checkBoxOnlyActive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.checkBoxOnlyActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxOnlyActive.Location = new System.Drawing.Point(672, 85);
            this.checkBoxOnlyActive.Name = "checkBoxOnlyActive";
            this.checkBoxOnlyActive.Size = new System.Drawing.Size(134, 29);
            this.checkBoxOnlyActive.TabIndex = 1;
            this.checkBoxOnlyActive.Text = "OnlyActive";
            this.checkBoxOnlyActive.UseVisualStyleBackColor = true;
            this.checkBoxOnlyActive.CheckedChanged += new System.EventHandler(this.checkBoxOnlyActive_CheckedChanged);
            // 
            // butAddNewEmp
            // 
            this.butAddNewEmp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.butAddNewEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butAddNewEmp.Location = new System.Drawing.Point(12, 477);
            this.butAddNewEmp.Name = "butAddNewEmp";
            this.butAddNewEmp.Size = new System.Drawing.Size(153, 59);
            this.butAddNewEmp.TabIndex = 2;
            this.butAddNewEmp.Text = "Add New Employee";
            this.butAddNewEmp.UseVisualStyleBackColor = true;
            this.butAddNewEmp.Click += new System.EventHandler(this.butAddNewEmp_Click);
            // 
            // butEditEmp
            // 
            this.butEditEmp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.butEditEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEditEmp.Location = new System.Drawing.Point(653, 477);
            this.butEditEmp.Name = "butEditEmp";
            this.butEditEmp.Size = new System.Drawing.Size(153, 59);
            this.butEditEmp.TabIndex = 3;
            this.butEditEmp.Text = "Edit Employee";
            this.butEditEmp.UseVisualStyleBackColor = true;
            this.butEditEmp.Click += new System.EventHandler(this.butEditEmp_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightCoral;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(818, 31);
            this.panel1.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(710, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Employees";
            // 
            // guna2VScrollBar1
            // 
            this.guna2VScrollBar1.AutoSize = true;
            this.guna2VScrollBar1.HoverState.Parent = null;
            this.guna2VScrollBar1.InUpdate = false;
            this.guna2VScrollBar1.LargeChange = 10;
            this.guna2VScrollBar1.Location = new System.Drawing.Point(800, 120);
            this.guna2VScrollBar1.Name = "guna2VScrollBar1";
            this.guna2VScrollBar1.PressedState.Parent = this.guna2VScrollBar1;
            this.guna2VScrollBar1.ScrollbarSize = 18;
            this.guna2VScrollBar1.Size = new System.Drawing.Size(18, 300);
            this.guna2VScrollBar1.TabIndex = 13;
            // 
            // guna2HScrollBar1
            // 
            this.guna2HScrollBar1.AutoSize = true;
            this.guna2HScrollBar1.HoverState.Parent = null;
            this.guna2HScrollBar1.InUpdate = false;
            this.guna2HScrollBar1.LargeChange = 10;
            this.guna2HScrollBar1.Location = new System.Drawing.Point(22, 439);
            this.guna2HScrollBar1.Name = "guna2HScrollBar1";
            this.guna2HScrollBar1.PressedState.Parent = this.guna2HScrollBar1;
            this.guna2HScrollBar1.ScrollbarSize = 18;
            this.guna2HScrollBar1.Size = new System.Drawing.Size(300, 18);
            this.guna2HScrollBar1.TabIndex = 12;
            this.guna2HScrollBar1.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // butEditSalary
            // 
            this.butEditSalary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.butEditSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEditSalary.Location = new System.Drawing.Point(494, 477);
            this.butEditSalary.Name = "butEditSalary";
            this.butEditSalary.Size = new System.Drawing.Size(153, 59);
            this.butEditSalary.TabIndex = 14;
            this.butEditSalary.Text = "Edit Salary";
            this.butEditSalary.UseVisualStyleBackColor = true;
            this.butEditSalary.Click += new System.EventHandler(this.butEditSalary_Click);
            // 
            // EmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 548);
            this.ControlBox = false;
            this.Controls.Add(this.butEditSalary);
            this.Controls.Add(this.guna2VScrollBar1);
            this.Controls.Add(this.guna2HScrollBar1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.butEditEmp);
            this.Controls.Add(this.butAddNewEmp);
            this.Controls.Add(this.checkBoxOnlyActive);
            this.Controls.Add(this.dataGridViewEmp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "EmployeeForm";
            this.Load += new System.EventHandler(this.EmployeeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmp)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewEmp;
        private System.Windows.Forms.CheckBox checkBoxOnlyActive;
        private System.Windows.Forms.Button butAddNewEmp;
        private System.Windows.Forms.Button butEditEmp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2VScrollBar guna2VScrollBar1;
        private Guna.UI2.WinForms.Guna2HScrollBar guna2HScrollBar1;
        private System.Windows.Forms.Button butEditSalary;
    }
}