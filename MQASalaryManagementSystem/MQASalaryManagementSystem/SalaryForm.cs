﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MQASalaryManagementSystem
{
    public partial class SalaryForm : Form
    {
        private string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public SalaryForm()
        {
            InitializeComponent();
            ComboBoxMY();
            butCreate.Enabled = false;
            butOutputExcel.Enabled = false;


        }

       
        private void ComboBoxMY()
        {
            
            comboBoxM.DataSource = CultureInfo.InvariantCulture.DateTimeFormat.MonthNames.Take(12).ToList();
            comboBoxM.SelectedItem =  CultureInfo.InvariantCulture.DateTimeFormat.MonthNames[DateTime.Now.AddMonths(-1).Month - 1];
            comboBoxY.DataSource = Enumerable.Range(2011, DateTime.Now.Year - 2011 + 1).ToList();
            comboBoxY.SelectedItem = DateTime.Now.Year;


        }
        
        private string ComboMonth(string month)
        {
            string mnum  = "";
            if (comboBoxM.Text == "January")
            {
                 mnum = "01";
            }
            else if(comboBoxM.Text == "February")
            {
                mnum = "02";
            }
            else if (comboBoxM.Text == "March")
            {
                mnum = "03";
            }
            else if (comboBoxM.Text == "April")
            {
                mnum = "04";
            }
            else if (comboBoxM.Text == "May")
            {
                mnum = "05";
            }
            else if (comboBoxM.Text == "June")
            {
                mnum = "06";
            }
            else if (comboBoxM.Text == "July")
            {
                mnum = "07";
            }
            else if (comboBoxM.Text == "August")
            {
                mnum = "08";
            }
            else if (comboBoxM.Text == "September")
            {
                mnum = "09";
            }
            else if (comboBoxM.Text == "October")
            {
                mnum = "10";
            }
            else if (comboBoxM.Text == "November")
            {
                mnum = "11";
            }
            else if (comboBoxM.Text == "December")
            {
                mnum = "12";
            }
            return mnum;
        }
        private void butShowSalary_Click(object sender, EventArgs e)
        {
            
            ShowSalary(1);
            
        }
        public void ShowSalary(int show)
        {
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


                int month = 0;
                int year = 0;
                string Checkmonth = DateTime.Today.ToString("MM");
                string Checkyear = DateTime.Today.ToString("yyyy");
                if (show == 1)
                {
                    month =int.Parse(ComboMonth(comboBoxM.Text));
                    year = int.Parse(comboBoxY.Text);
                }
                else if (show ==2)
                {
                    month = int.Parse(Checkmonth);
                    year = int.Parse(Checkyear);
                }

                string sql_Text = "SELECT Employees.EMP_ID,Employees.EMP_NickName, Net_Salary.Base_salary, ";
                sql_Text += "Net_Salary.Executive_Allowance, Net_Salary.Travel_Allowance, Net_Salary.Housing_Allowance, ";
                sql_Text += "Net_Salary.Language_Allowance, Net_Salary.Bonus, Net_Salary.OT, Net_Salary.Pay_Date ";
                sql_Text += "FROM Net_Salary INNER JOIN Employees ON Employees.EMP_ID = Net_Salary.Net_Salary_ID ";
                sql_Text += "WHERE MONTH(Pay_Date) = " + month + "AND  year(Pay_Date) = " + year;
                sql_Text += " ORDER BY Employees.EMP_ID ASC";
                DataTable tbl = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, (OleDbConnection)cnn))
                {
                    if (adapter.Fill(tbl) != 0)
                    {
                        butCreate.Enabled = false;
                        butOutputExcel.Enabled = true;
                    }
                    else
                    {
                        butCreate.Enabled = true;
                        butOutputExcel.Enabled = false;
                    }
                    if (this.WindowState == FormWindowState.Maximized)
                    {
                        dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    }
                    dataGridView1.DataSource = tbl;
                    dataGridView1.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    
                }
            }
        }
        public static string comboMonth = "";
        public static string comboYear = "";
        private void butOutputExcel_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Do you want to Export to Excel file?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {

                ShowSalary(1);
                comboMonth = ComboMonth(comboBoxM.Text).ToString();
                comboYear = comboBoxY.Text;

                MQASalarySlipWithTemplate.MQASalarySlipWithTemplateEX();
                MQASalaryListWithTemplate.SalaryListWithTemplateEx();
                MQATaxWithholdingWithTemplate.MQATaxWithholdingWithTemplateEx();

            }





        }
        
        private void CalculateSalary()
        {
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                string cbposition = "SELECT * From Salary ";
                DataTable tbl = new DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(cbposition, con);
                adapter.Fill(tbl);
                int totalsalary= 0;
                
                ArrayList testtb = new ArrayList();
                foreach (DataRow dr in tbl.Rows)
                {
                    int empid = int.Parse(dr[0].ToString());
                    int Base = int.Parse(dr[1].ToString());
                    int Executive = int.Parse(dr[2].ToString());
                    int Travel = int.Parse(dr[3].ToString());
                    int housing = int.Parse(dr[4].ToString());
                    int Language = int.Parse(dr[5].ToString());
                    int Bonus = int.Parse(dr[6].ToString());
                    int OT = int.Parse(dr[7].ToString());
                    totalsalary = Base + Executive + Travel + housing + Language + Bonus + OT;
                    int[] salarydetail = {Base,Executive,Travel,housing,Language,Bonus,OT,empid,totalsalary};
                    NetSalarySave(salarydetail);
                    
                }
            }
        }
        //----------------- sum Tax  Deduction --------------------
        static float Deduction(int empid)
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            float deducsum = 0;
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                string cbposition = "SELECT * From Tax_Deduction WHERE Tax_ID = "+empid;
                DataTable tbl = new DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(cbposition, con);
                adapter.Fill(tbl);
                int i = 0;
                foreach (DataRow dr in tbl.Rows)
                {

                  int Tax_ID = int.Parse(dr[0].ToString());
                    int Personal  = int.Parse(dr[1].ToString());
                  int Spouse    = int.Parse(dr[2].ToString());
                  int Child     = int.Parse(dr[3].ToString());
                  int Parental  = int.Parse(dr[4].ToString());
                  int Expense   = int.Parse(dr[5].ToString());
                  int LMF_RTF   = int.Parse(dr[6].ToString());
                  int Home_Loan = int.Parse(dr[7].ToString());
                  deducsum = Personal + Spouse + Child + Parental + Expense + LMF_RTF + Home_Loan;
                    i++;
                  
                }
                return deducsum;
            }

        }
        private void NetSalarySave(int[] salarydetail)
        {
           
            IDbTransaction trans = null;
            
            string pay_date = DateTime.Today.ToString("yyyy/MM/dd");
            string Checkmonth = DateTime.Today.ToString("MM");
            string Checkyear = DateTime.Today.ToString("yyyy");
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                int r = 0;
                int Base = 0;
                int Executive = 0;
                int Travel = 0;
                int housing = 0;
                int Language = 0;
                int Bonus = 0;
                int OT = 0;
                int empid = 0;
                int totalsalary = 0;
                foreach (int i in salarydetail)
                {
                    if (r == 0)
                    {
                        Base = i;
                        r = 1;
                      
                    }
                    else if (r == 1)
                    {
                        Executive = i;
                        r = 2;
                        
                    }
                    else if (r == 2)
                    {
                        Travel = i;
                        r = 3;
                        
                    }
                    else if (r == 3)
                    {
                        housing = i;
                        r = 4;
                        
                    }
                    else if (r == 4)
                    {
                        Language = i;
                        r = 5;
                        
                    }
                    else if (r == 5)
                    {
                        Bonus = i;
                        r = 6;
                       
                    }
                    else if (r == 6)
                    {
                        OT = i;
                        r = 7;
                       
                    }
                    else if (r == 7)
                    {
                        empid = i;
                        r = 8;
                        
                    }
                    else if (r == 8)
                    {
                        totalsalary = i;
                        r = 0;
                    }
                    
                }
                
                string sql = "Select * FROM Net_Salary WHERE MONTH(Pay_Date) = "+ ComboMonth(comboBoxM.Text) + " AND YEAR(Pay_Date) = "+int.Parse(comboBoxY.Text) +" AND EMP_ID = "+empid;
                DataTable tbl = new DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(sql, con);
                if(adapter.Fill(tbl) == 0)
                {
                   
                    int salarytax = (int)(SalaryTax(empid, totalsalary * 12) / 12);
                    int socialP = SocialPremiums(empid, 0);
                    int netsalary = totalsalary - (salarytax + socialP);
                    SalaryTax(empid, 0);
                    OtCheck(empid);
                    string sql_Text = "INSERT INTO Net_Salary (Net_Salary_ID,EMP_ID,Base_salary,Executive_Allowance,Travel_Allowance" +
                        ",Housing_Allowance,Language_Allowance,Bonus,OT,Total_Salary,Salary_Tax,Insurance,Net_Salary,Pay_Date) " +
                    "VALUES (" + empid + "," + empid + "," + Base + ","
                    + Executive + "," + Travel + ","
                    + housing + "," + Language + ","
                    + Bonus + "," + OT + "," + totalsalary + "," + salarytax + "," + socialP + "," + netsalary + ",'" + ComboMonth(comboBoxM.Text) + "/" + int.Parse(comboBoxY.Text) + "')";
                    using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                    {
                        trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                        cmd_command.Connection = cnn;
                        cmd_command.Transaction = trans;

                        try
                        {
                            cmd_command.ExecuteNonQuery();
                            trans.Commit();

                        }
                        catch (OleDbException ex)
                        {
                            trans.Rollback();
                            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }

                    }
                }

            }
        }
        public void NetSalaryEdit()
        {
            
            

            string pay_date = DateTime.Today.ToString("yyyy/MM/dd");
            string Checkmonth = DateTime.Today.ToString("MM");
            string Checkyear = DateTime.Today.ToString("yyyy");
            IDbTransaction trans = null;
            using (IDbConnection cnn = new OleDbConnection(con))
            {
              


                int salarytax = (int)(SalaryTax(int.Parse(EditNetSalaryForm.salaryID), EditNetSalaryForm.sum * 12) / 12);
                int socialP = SocialPremiums(int.Parse(EditNetSalaryForm.salaryID), 0);
                int netsalary = EditNetSalaryForm.sum - (salarytax + socialP);
                SalaryTax(int.Parse(EditNetSalaryForm.salaryID), 0);
                string sql_Text = "UPDATE Net_Salary SET Base_salary = " + EditNetSalaryForm.BaseSalary;
                sql_Text += ",Executive_Allowance = " + EditNetSalaryForm.Executive + ",Travel_Allowance = " + EditNetSalaryForm.Traval + ",Housing_Allowance = " + EditNetSalaryForm.Hosing;
                sql_Text += ",Language_Allowance = " + EditNetSalaryForm.Language + ",Bonus = " + EditNetSalaryForm.Bonus + ",OT = " + EditNetSalaryForm.OT;
                sql_Text += ",Total_Salary = " + EditNetSalaryForm.sum + ",Salary_Tax = " + salarytax + ",Insurance = " + socialP + ",Net_Salary = " + netsalary;
                sql_Text += " WHERE MONTH(Pay_Date) = " + EditNetSalaryForm.MN + " AND YEAR(Pay_Date) = " + EditNetSalaryForm.YN+ " AND EMP_ID = " + EditNetSalaryForm.salaryID;
                using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                {
                    try
                    {
                        cnn.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                    cmd_command.Connection = cnn;
                    cmd_command.Transaction = trans;

                    try
                    {
                        cmd_command.ExecuteNonQuery();
                        trans.Commit();
                        

                    }
                    catch (OleDbException ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                
            }
            ShowSalary(2);
        }
        //------------------------------ Calculate Salary Tax----------------------
        static float SalaryTax(int empid, int stax)
        {

            float msalary = (Deduction(empid) + SocialPremiums(empid, 1));
          
            float taxsalary = 0;
            if (stax * 0.5 > 100000)
            {
                msalary = stax - 100000 - msalary;
                if (msalary - 150000 > 0)
                {
                    msalary -= 150000;
                    if (msalary - 150000 > 0)
                    {
                        msalary -= 150000;
                        taxsalary += (float)(150000 * 0.05);
                        

                        if (msalary - 200000 > 0 || msalary - 200000 > 200000)
                        {
                            msalary -= 200000;
                            taxsalary += (float)(200000 * 0.10);

                            if (msalary - 250000 > 0 || msalary - 250000 > 250000)
                            {

                                msalary -= 250000;
                                taxsalary += (float)(250000 * 0.15);
                                if (msalary - 250000 > 0 || msalary - 250000 > 250000)
                                {

                                    msalary -= 250000;
                                    taxsalary += (float)(250000 * 0.20);
                                    if (msalary - 1000000 > 0 || msalary - 1000000 > 250000)
                                    {

                                        msalary -= 1000000;
                                        taxsalary += (float)(1000000 * 0.25);
                                        if (msalary - 2000000 > 0 || msalary - 2000000 > 250000)
                                        {

                                            msalary -= 250000;
                                            taxsalary += (float)(250000 * 0.30);
                                            if (msalary - 4000000 > 0 )
                                            {


                                                taxsalary += (float)(msalary * 0.35);

                                            }

                                        }
                                        else
                                        {
                                            taxsalary += (float)(msalary * 0.30);
                                        }
                                    }
                                    else
                                    {
                                        taxsalary += (float)(msalary * 0.25);
                                    }
                                }
                                else
                                {
                                    taxsalary += (float)(msalary * 0.20);
                                }
                            }
                            else
                            {
                                taxsalary += (float)(msalary * 0.15);
                            }
                        }
                        else
                        {
                            taxsalary += (float)(msalary * 0.10);

                        }

                    }
                    else
                    {
                        taxsalary += (float)(msalary * 0.05);
                    }
                }
            }
            else
            {
                msalary = stax - (60000 + 9000);
                if (msalary > 0)
                {
                    taxsalary = 1;
                }
                else
                {
                    taxsalary = 0;
                }
            }
            return taxsalary;
        }


        //-------------------- Calculate SocialPremiums-----------------
        static int SocialPremiums(int empid,int num)
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string month = DateTime.Today.ToString("MMM");
            string year = DateTime.Today.ToString("yyyy");
            string SQL_Text = " ";
            int socialP = 0;
            using (IDbConnection cnn = new OleDbConnection(con))
            {

                if(num == 0)
                {
                    SQL_Text = "SELECT " + month + " From Social_Security WHERE Social_Security_ID = " + empid + " AND Year_Premiums = " + year;
                }
                else if(num == 1)
                {
                    SQL_Text = "SELECT * FROM Social_Security WHERE Social_Security_ID = " + empid
                        ;
                }
                
                System.Data.DataTable tbl = new System.Data.DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
                adapter.Fill(tbl);
                
                foreach (DataRow dr in tbl.Rows)
                {

                    if (num == 0)
                    {
                        socialP = int.Parse(dr[0].ToString());
                    }
                    else if (num == 1)
                    {
                        socialP = int.Parse(dr[1].ToString());
                        socialP += int.Parse(dr[2].ToString());
                        socialP += int.Parse(dr[3].ToString());
                        socialP += int.Parse(dr[4].ToString());
                        socialP += int.Parse(dr[5].ToString());
                        socialP += int.Parse(dr[6].ToString());
                        socialP += int.Parse(dr[7].ToString());
                        socialP += int.Parse(dr[8].ToString());
                        socialP += int.Parse(dr[9].ToString());
                        socialP += int.Parse(dr[10].ToString());
                        socialP += int.Parse(dr[11].ToString());
                        socialP += int.Parse(dr[12].ToString());
                       
                    }
                }
                return socialP;
                
            }
        }

        private void butCreate_Click(object sender, EventArgs e)
        {
            string thismonth = DateTime.Today.ToString("MMMM");
            string thisyear = DateTime.Today.ToString("yyyy");
           
            DialogResult dialogResult = MessageBox.Show("Do you want to Create Salary in "+ comboBoxM.Text + " "+ int.Parse(comboBoxY.Text) + " ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {
                CalculateSalary();
                ShowSalary(2);
                //comboBoxM.Text = thismonth;
                //comboBoxY.Text = thisyear;
            }
        }
        public static string salaryID = "";
        public static string NName = "";
        public static string BaseSalary = "";
        public static string Executive = "";
        public static string Traval = "";
        public static string Hosing = "";
        public static string Language = "";
        public static string Bonus = "";
        public static string OT = "";
        public static string M = "";
        public static string Y = "";

        //---------------------- Send data from datagridview to EditNetSalary Form -------------- 
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string thisMonth = DateTime.Today.ToString("MMMM");
            string thisyear = DateTime.Today.ToString("yyyy");

            EditNetSalaryForm outputF = new EditNetSalaryForm();
            if (e.RowIndex != -1)
            {
                DataGridViewRow dgvRow = dataGridView1.Rows[e.RowIndex];

                salaryID = dgvRow.Cells[0].Value.ToString();
                NName = dgvRow.Cells[1].Value.ToString();
                BaseSalary = dgvRow.Cells[2].Value.ToString();
                Executive = dgvRow.Cells[3].Value.ToString();
                Traval = dgvRow.Cells[4].Value.ToString();
                Hosing = dgvRow.Cells[5].Value.ToString();
                Language = dgvRow.Cells[6].Value.ToString();
                Bonus = dgvRow.Cells[7].Value.ToString();
                OT = dgvRow.Cells[8].Value.ToString();
                M = ComboMonth(comboBoxM.Text);
                Y = comboBoxY.Text;

                if (salaryID != "")
                {
                    EditNetSalaryForm editNetSalaryForm = new EditNetSalaryForm();
                    editNetSalaryForm.ShowDialog();
                }
            }
        }
        private void SendEMP()
        {
           
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                string SQL = "SELECT Employees.EMP_ID,Employees.Join_Date,Employees.Resign_date,Salary.Base_salary,Salary.Executive_Allowance,Salary.Travel_Allowance,Salary.Housing_Allowance,Salary.Language_Allowance ";
                SQL += "FROM Salary INNER JOIN Employees ON Employees.EMP_ID = Salary.Salary_ID ";
                DataTable tbl = new DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(SQL, con);
                adapter.Fill(tbl);

                foreach (DataRow dr in tbl.Rows)
                {
                    int id = int.Parse(dr[0].ToString());
                    string joindate = dr[1].ToString();
                    string resigndate = dr[2].ToString();
                    int basesalary = int.Parse(dr[3].ToString());
                    int executive = int.Parse(dr[4].ToString());
                    int travel = int.Parse(dr[5].ToString());
                    int housing = int.Parse(dr[6].ToString());
                    int language = int.Parse(dr[7].ToString());
                    
                    UpdateOt(id);
                    CheckJoin(joindate,id,basesalary,executive,travel,housing,language);
                    CheckResign(resigndate,id, basesalary, executive, travel, housing, language);

                }

            }
        }
        private void UpdateOt(int id) 
        {
            string month = ComboMonth(comboBoxM.Text);
            string year = comboBoxY.Text;
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            IDbTransaction trans = null;
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                string sql_Text = "UPDATE Net_Salary SET OT =  " + OtCheck(id);
                  sql_Text +=  " WHERE Net_Salary_ID = "+id+" AND MONTH(Pay_Date) = "+month+" AND YEAR(Pay_Date) = " + year;
                
                using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                {
                    try
                    {
                        cnn.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                    cmd_command.Connection = cnn;
                    cmd_command.Transaction = trans;

                    try
                    {

                        cmd_command.ExecuteNonQuery();
                        trans.Commit();
                        
                       
                    }
                    catch (OleDbException ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        
                    }

                }
            }
        }
       
        
        public float OtCheck(int id)
        {
           string month = ComboMonth(comboBoxM.Text);
           string year = comboBoxY.Text;
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                string SQL = "SELECT Salary.Base_salary ,Work_Day.* FROM Work_Day ";
                SQL += "INNER JOIN Salary ON Salary.Salary_ID = Work_Day.作業者NO ";
                SQL += "WHERE  年月 = '"+year+month+ "' AND 作業者NO = "+id;
                DataTable tbl = new DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(SQL, con);
                adapter.Fill(tbl);
                DateTime day1 = new DateTime(int.Parse(year), int.Parse(month), 1);
                DateTime day2 = new DateTime(int.Parse(year), int.Parse(month), 2);
                DateTime day3 = new DateTime(int.Parse(year), int.Parse(month), 3);
                DateTime day4 = new DateTime(int.Parse(year), int.Parse(month), 4);
                DateTime day5 = new DateTime(int.Parse(year), int.Parse(month), 5);
                DateTime day6 = new DateTime(int.Parse(year), int.Parse(month), 6);
                DateTime day7 = new DateTime(int.Parse(year), int.Parse(month), 7);
                DateTime day8 = new DateTime(int.Parse(year), int.Parse(month), 8);
                DateTime day9 = new DateTime(int.Parse(year), int.Parse(month), 9);
                DateTime day10 = new DateTime(int.Parse(year), int.Parse(month), 10);
                DateTime day11 = new DateTime(int.Parse(year), int.Parse(month), 11);
                DateTime day12 = new DateTime(int.Parse(year), int.Parse(month), 12);
                DateTime day13 = new DateTime(int.Parse(year), int.Parse(month), 13);
                DateTime day14 = new DateTime(int.Parse(year), int.Parse(month), 14);
                DateTime day15 = new DateTime(int.Parse(year), int.Parse(month), 15);
                DateTime day16 = new DateTime(int.Parse(year), int.Parse(month), 16);
                DateTime day17 = new DateTime(int.Parse(year), int.Parse(month), 17);
                DateTime day18 = new DateTime(int.Parse(year), int.Parse(month), 18);
                DateTime day19 = new DateTime(int.Parse(year), int.Parse(month), 19);
                DateTime day20 = new DateTime(int.Parse(year), int.Parse(month), 20);
                DateTime day21 = new DateTime(int.Parse(year), int.Parse(month), 21);
                DateTime day22 = new DateTime(int.Parse(year), int.Parse(month), 22);
                DateTime day23 = new DateTime(int.Parse(year), int.Parse(month), 23);
                DateTime day24 = new DateTime(int.Parse(year), int.Parse(month), 24);
                DateTime day25 = new DateTime(int.Parse(year), int.Parse(month), 25);
                DateTime day26 = new DateTime(int.Parse(year), int.Parse(month), 26);
                DateTime day27 = new DateTime(int.Parse(year), int.Parse(month), 27);
                DateTime day28 = new DateTime(int.Parse(year), int.Parse(month), 28);
                DateTime day29 = new DateTime();
                DateTime day30 = new DateTime();
                DateTime day31 = new DateTime();
                int basesalary = 0;
                int no1 = 0;
                int no2 = 0;
                int no3 = 0;
                int no4 = 0;
                int no5 = 0;
                int no6 = 0;
                int no7 = 0;
                int no8 = 0;
                int no9 = 0;
                int no10 = 0;
                int no11 = 0;
                int no12 = 0;
                int no13 = 0;
                int no14 = 0;
                int no15 =0;
                int no16 = 0;
                int no17 = 0;
                int no18 = 0;
                int no19 = 0;
                int no20 = 0;
                int no21 = 0;
                int no22 = 0;
                int no23 = 0;
                int no24 = 0;
                int no25 = 0;
                int no26 = 0;
                int no27 = 0;
                int no28 = 0;
                int no29 = 0;
                int no30 = 0;
                int no31 = 0;

                float otsum = 0;

                float ot1 = 0;
                float ot2 = 0;
                float ot3 = 0;
                float ot4 = 0;
                float ot5 = 0;
                float ot6 = 0;
                float ot7 = 0;
                float ot8 = 0;
                float ot9 = 0;
                float ot10 = 0;
                float ot11 = 0;
                float ot12 = 0;
                float ot13 = 0;
                float ot14 = 0;
                float ot15 = 0;
                float ot16 = 0;
                float ot17 = 0;
                float ot18 = 0;
                float ot19 = 0;
                float ot20 = 0;
                float ot21 = 0;
                float ot22 = 0;
                float ot23 = 0;
                float ot24 = 0;
                float ot25 = 0;
                float ot26 = 0;
                float ot27 = 0;
                float ot28 = 0;
                float ot29 = 0;
                float ot30 = 0;
                float ot31 = 0;
                foreach (DataRow dr in tbl.Rows)
                {
                  
                     basesalary = int.Parse(dr[0].ToString());
                     no1 = int.Parse(dr[11].ToString());
                     no2 = int.Parse(dr[12].ToString());
                     no3 = int.Parse(dr[13].ToString());
                     no4 = int.Parse(dr[14].ToString());
                     no5 = int.Parse(dr[15].ToString());
                     no6 = int.Parse(dr[16].ToString());
                     no7 = int.Parse(dr[17].ToString());
                     no8 = int.Parse(dr[18].ToString());
                     no9 = int.Parse(dr[19].ToString());
                     no10 = int.Parse(dr[20].ToString());
                     no11 = int.Parse(dr[21].ToString());
                     no12 = int.Parse(dr[22].ToString());
                     no13 = int.Parse(dr[23].ToString());
                     no14 = int.Parse(dr[24].ToString());
                     no15 = int.Parse(dr[25].ToString());
                     no16 = int.Parse(dr[26].ToString());
                     no17 = int.Parse(dr[27].ToString());
                     no18 = int.Parse(dr[28].ToString());
                     no19 = int.Parse(dr[29].ToString());
                     no20 = int.Parse(dr[30].ToString());
                     no21 = int.Parse(dr[31].ToString());
                     no22 = int.Parse(dr[32].ToString());
                     no23 = int.Parse(dr[33].ToString());
                     no24 = int.Parse(dr[34].ToString());
                     no25 = int.Parse(dr[35].ToString());
                     no26 = int.Parse(dr[36].ToString());
                     no27 = int.Parse(dr[37].ToString());
                     no28 = int.Parse(dr[38].ToString());
                     no29 = 0;
                     no30 = 0;
                     no31 = 0;


                    
                    if (month == "02")
                    {
                        try
                        {
                            day29 = new DateTime(int.Parse(year), int.Parse(month), 29);
                            no29 = int.Parse(dr[39].ToString());
                        }catch(Exception ex)
                        {
                            //MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        day29 = new DateTime(int.Parse(year), int.Parse(month), 29);
                        day30 = new DateTime(int.Parse(year), int.Parse(month), 30);
                        
                        no29 = int.Parse(dr[39].ToString());
                        no30 = int.Parse(dr[40].ToString());
                        if (month == "01" || month == "03" || month == "05" || month == "07" || month == "08" || month == "10" || month == "12")
                        {
                            no31 = int.Parse(dr[41].ToString());
                            day31 = new DateTime(int.Parse(year), int.Parse(month), 31);
                        }
                    }
                   



                    if ((no1 != 0 && day1.ToString("dddd") == "Saturday") || (no1 != 0 && day1.ToString("dddd") == "Sunday") || no1 != 0 && CheckHoliday(1) == 1)
                    {
                        if ((no1 <= 8 && day1.ToString("dddd") == "Saturday") || (no1 <= 8 && day1.ToString("dddd") == "Sunday") || no1 <= 8 && CheckHoliday(1) == 1)
                        {
                            ot1 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no1);
                        }
                        else if ((no1 > 8 && day1.ToString("dddd") == "Saturday") || (no1 > 8 && day1.ToString("dddd") == "Sunday") || no1 > 8 && CheckHoliday(1) == 1)
                        {
                            ot1 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no1 -= 8;
                            ot1 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no1);
                            
                        }
                        
                    }
                    else if(no1 > 8)
                    {
                        ot1 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no1-8));

                    }
                    if (no2 != 0 && day2.ToString("dddd") == "Saturday" || no2 != 0 && day2.ToString("dddd") == "Sunday" || no2 != 0 && CheckHoliday(2) == 1)
                    {
                        if (no2 <= 8 && day2.ToString("dddd") == "Saturday" || day2.ToString("dddd") == "Sunday" || no2 <= 8 && CheckHoliday(2) == 1)
                        {
                            ot2 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no2);
                        }
                        else if (no2 > 8 && day2.ToString("dddd") == "Saturday" || no2 > 8 && day2.ToString("dddd") == "Sunday" || no2 > 8 && CheckHoliday(2) == 1)
                        {
                            ot2 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no2 -= 8;
                            ot2 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no2);
                        }

                    }
                    else if (no2 > 8)
                    {
                        ot2 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no2 - 8));
                    }

                    if (no3 != 0 && day3.ToString("dddd") == "Saturday"|| no3 != 0 && day3.ToString("dddd") == "Sunday" || no3 != 0 && CheckHoliday(3) == 1)
                    {
                        if (no3 <= 8 && day3.ToString("dddd") == "Saturday" || no3 <= 8 && day3.ToString("dddd") == "Sunday" || no3 <= 8 && CheckHoliday(3) == 1)
                        {
                            ot3 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no3);
                        }
                        else if (no3 > 8 && day3.ToString("dddd") == "Saturday" || no3 > 8 && day3.ToString("dddd") == "Sunday" || no3 > 8 && CheckHoliday(3) == 1)
                        {
                            ot3 = ((float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00));
                            
                            no3 -= 8;
                            ot3 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no3);
                        }

                    }
                    else if (no3 > 8)
                    {
                        ot3 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no3 - 8));
                    }
                    if (no4 != 0 && day4.ToString("dddd") == "Saturday" || no4 != 0 && day4.ToString("dddd") == "Sunday" || no4 != 0 && CheckHoliday(4) == 1)
                    {
                        if (no4 <= 8 && day4.ToString("dddd") == "Saturday" || no4 <= 8 && day4.ToString("dddd") == "Sunday" || no4 <= 8 && CheckHoliday(4) == 1)
                        {
                            ot4 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no4);
                        }
                        else if (no4 > 8 && day4.ToString("dddd") == "Saturday" || no4 > 8 && day4.ToString("dddd") == "Sunday" || no4 > 8 && CheckHoliday(4) == 1)
                        {
                            ot4 = ((float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00));

                            no4 -= 8;
                            ot4 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no4);
                        }

                    }
                    else if (no4 > 8)
                    {
                        ot4 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no4- 8));
                    }


                    if (no5 != 0 && day5.ToString("dddd") == "Saturday" || no5 != 0 && day5.ToString("dddd") == "Sunday" || no5 != 0 && CheckHoliday(5) == 1)
                    {
                        if (no5 <= 8 && day5.ToString("dddd") == "Saturday" || no5 <= 8 && day5.ToString("dddd") == "Sunday" || no5 <= 8 && CheckHoliday(5) == 1)
                        {
                            ot5 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no5);
                        }
                        else if (no5 > 8 && day5.ToString("dddd") == "Saturday" || no5 > 8 && day5.ToString("dddd") == "Sunday" || no5 > 8 && CheckHoliday(5) == 1)
                        {
                            ot5 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no5 -= 8;
                            ot5 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no5);
                        }

                    }
                    else if (no5 > 8)
                    {
                        ot5 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no5 - 8));
                    }
                    if (no6 != 0 && day6.ToString("dddd") == "Saturday" || no6 != 0 && day6.ToString("dddd") == "Sunday" || no6 != 0 && CheckHoliday(6) == 1)
                    {
                        if (no6 <= 8 && day6.ToString("dddd") == "Saturday" || no6 <= 8 && day6.ToString("dddd") == "Sunday" || no6 <= 8 && CheckHoliday(6) == 1)
                        {
                            ot6 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no6);
                        }
                        else if (no6 > 8 && day6.ToString("dddd") == "Saturday" || no6 > 8 && day6.ToString("dddd") == "Sunday" || no6 > 8 && CheckHoliday(6) == 1)
                        {
                            ot6 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no6 -= 8;
                            ot6 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no6);
                        }

                    }
                    else if (no6 > 8)
                    {
                        ot6 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no6 - 8));
                    }
                    if (no7 != 0 && day7.ToString("dddd") == "Saturday" || no7 != 0 && day7.ToString("dddd") == "Sunday" || no7 != 0 && CheckHoliday(7) == 1)
                    {
                        if (no7 <= 8 && day7.ToString("dddd") == "Saturday" || no7 <= 8 && day7.ToString("dddd") == "Sunday" || no7 <= 8 && CheckHoliday(7) == 1)
                        {
                            ot7 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no7);
                        }
                        else if (no7 > 8 && day7.ToString("dddd") == "Saturday" || no7 > 8 && day7.ToString("dddd") == "Sunday" || no7 > 8 && CheckHoliday(7) == 1)
                        {
                            ot7 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no7 -= 8;
                            ot7 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no7);
                        }

                    }
                    else if (no7 > 8)
                    {
                        ot7 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no7 - 8));
                    }
                    if (no8 != 0 && day8.ToString("dddd") == "Saturday" || no8 != 0 && day8.ToString("dddd") == "Sunday" || no8 != 0 && CheckHoliday(8) == 1)
                    {
                        if (no8 <= 8 && day8.ToString("dddd") == "Saturday" || day8.ToString("dddd") == "Sunday" || no8 <= 8 && CheckHoliday(8) == 1)
                        {
                            ot8 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no8);
                        }
                        else if (no8 > 8 && day8.ToString("dddd") == "Saturday" || no8 > 8 && day8.ToString("dddd") == "Sunday" || no8 > 8 && CheckHoliday(8) == 1)
                        {
                            ot8 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no8 -= 8;
                            ot8 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no8);
                        }

                    }
                    else if (no8 > 8)
                    {
                        ot8 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no8 - 8));
                    }
                    if (no9 != 0 && day9.ToString("dddd") == "Saturday" || no9 != 0 && day9.ToString("dddd") == "Sunday" || no9 != 0 && CheckHoliday(9) == 1)
                    {
                        if (no9 <= 8 && day9.ToString("dddd") == "Saturday" || no9 <= 8 && day9.ToString("dddd") == "Sunday" || no9 <= 8 && CheckHoliday(9) == 1)
                        {
                            ot9 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no9);
                        }
                        else if (no9 > 8 && day9.ToString("dddd") == "Saturday" || no9 > 8 && day9.ToString("dddd") == "Sunday" || no9 > 8 && CheckHoliday(9) == 1)
                        {
                            ot9 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no9 -= 8;
                            ot9 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no9);
                        }

                    }
                    else if (no9 > 8)
                    {
                        ot9 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no9 - 8));
                    }
                    if (no10 != 0 && day10.ToString("dddd") == "Saturday" || no10 != 0 && day10.ToString("dddd") == "Sunday" || no10 != 0 && CheckHoliday(10) == 1)
                    {
                        if (no10 <= 8 && day4.ToString("dddd") == "Saturday" || no10 <= 8 && day10.ToString("dddd") == "Sunday" || no10 <= 8 && CheckHoliday(10) == 1)
                        {
                            ot10 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no10);
                        }
                        else if (no10 > 8 && day10.ToString("dddd") == "Saturday" || no10 > 8 && day10.ToString("dddd") == "Sunday" || no10 > 8 && CheckHoliday(10) == 1)
                        {
                            ot10 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no10 -= 8;
                            ot10 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no10);
                        }

                    }
                    else if (no10 > 8)
                    {
                        ot10 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no10 - 8));
                    }
                    if (no11 != 0 && day1.ToString("dddd") == "Saturday" || no11 != 0 && day11.ToString("dddd") == "Sunday" || no11 != 0 && CheckHoliday(11) == 1)
                    {
                        if (no11 <= 8 && day11.ToString("dddd") == "Saturday" || no11 <= 8 && day11.ToString("dddd") == "Sunday" || no11 <= 8 && CheckHoliday(11) == 1)
                        {
                            ot11 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no11);
                        }
                        else if (no11 > 8 && day11.ToString("dddd") == "Saturday" || no11 > 8 && day11.ToString("dddd") == "Sunday" || no11 > 8 && CheckHoliday(11) == 1)
                        {
                            ot11 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no11 -= 8;
                            ot11 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no11);
                        }

                    }
                    else if (no11 > 8)
                    {
                        ot11 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no11 - 8));

                    }
                    if (no12 != 0 && day12.ToString("dddd") == "Saturday" || no12 != 0 && day12.ToString("dddd") == "Sunday" || no12 != 0 && CheckHoliday(12) == 1)
                    {
                        if (no12 <= 8 && day12.ToString("dddd") == "Saturday" || no12 <= 8 && day12.ToString("dddd") == "Sunday" || no12 <= 8 && CheckHoliday(12) == 1)
                        {
                            ot12 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no12);
                        }
                        else if (no12 > 8 && day12.ToString("dddd") == "Saturday" || no12 > 8 && day12.ToString("dddd") == "Sunday" || no12 > 8 && CheckHoliday(12) == 1)
                        {
                            ot12 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no12 -= 8;
                            ot12 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no12);
                        }

                    }
                    else if (no12 > 8)
                    {
                        ot12 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no12 - 8));

                    }
                    if (no13 != 0 && day13.ToString("dddd") == "Saturday" || no13 != 0 && day13.ToString("dddd") == "Sunday" || no13 != 0 && CheckHoliday(13) == 1)
                    {
                        if (no13 <= 8 && day13.ToString("dddd") == "Saturday" || no13 <= 8 && day13.ToString("dddd") == "Sunday" || no13 <= 8 && CheckHoliday(13) == 1)
                        {
                            ot13 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no13);
                        }
                        else if (no13 > 8 && day1.ToString("dddd") == "Saturday" || no13 > 8 && day13.ToString("dddd") == "Sunday" || no13 > 8 && CheckHoliday(13) == 1)
                        {
                            ot13 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no13 -= 8;
                            ot13 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no13);
                        }

                    }
                    else if (no13 > 8)
                    {
                        ot13 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no13 - 8));

                    }
                    if (no14 != 0 && day14.ToString("dddd") == "Saturday" || no14 != 0 && day14.ToString("dddd") == "Sunday" || no14 != 0 && CheckHoliday(14) == 1)
                    {
                        if (no14 <= 8 && day14.ToString("dddd") == "Saturday" || no14 <= 8 && day14.ToString("dddd") == "Sunday" || no14 <= 8 && CheckHoliday(14) == 1)
                        {
                            ot14 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no14);
                        }
                        else if (no14 > 8 && day14.ToString("dddd") == "Saturday" || no14 > 8 && day14.ToString("dddd") == "Sunday" || no14 > 8 && CheckHoliday(14) == 1)
                        {
                            ot14 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no14 -= 8;
                            ot14 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no14);
                        }

                    }
                    else if (no14 > 8)
                    {
                        ot14 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no14 - 8));

                    }
                    if (no15 != 0 && day15.ToString("dddd") == "Saturday" || no15 != 0 && day15.ToString("dddd") == "Sunday" || no15 != 0 && CheckHoliday(15) == 1)
                    {
                        if (no15 <= 8 && day15.ToString("dddd") == "Saturday" || no15 <= 8 && day15.ToString("dddd") == "Sunday" || no15 <= 8 && CheckHoliday(15) == 1)
                        {
                            ot15 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no15);
                        }
                        else if (no15 > 8 && day15.ToString("dddd") == "Saturday" || no15 > 8 && day15.ToString("dddd") == "Sunday" || no15 > 8 && CheckHoliday(15) == 1)
                        {
                            ot15 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no15 -= 8;
                            ot15 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no15);
                        }

                    }
                    else if (no15 > 8)
                    {
                        ot15 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no15 - 8));

                    }
                    if (no16 != 0 && day16.ToString("dddd") == "Saturday" || no16 != 0 && day16.ToString("dddd") == "Sunday" || no16 != 0 && CheckHoliday(16) == 1)
                    {
                        if (no16 <= 8 && day16.ToString("dddd") == "Saturday" || no16 <= 8 && day16.ToString("dddd") == "Sunday" || no16 <= 8 && CheckHoliday(16) == 1)
                        {
                            ot16 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no16);
                        }
                        else if (no16 > 8 && day1.ToString("dddd") == "Saturday" || no16 > 8 && day16.ToString("dddd") == "Sunday" || no16 > 8 && CheckHoliday(16) == 1)
                        {
                            ot16 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no16 -= 8;
                            ot16 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no16);
                        }

                    }
                    else if (no16 > 8)
                    {
                        ot16 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no16 - 8));

                    }
                    if (no17 != 0 && day17.ToString("dddd") == "Saturday" || no17 != 0 && day17.ToString("dddd") == "Sunday" || no17 != 0 && CheckHoliday(17) == 1)
                    {
                        if (no17 <= 8 && day17.ToString("dddd") == "Saturday" || no17 <= 8 && day17.ToString("dddd") == "Sunday" || no17 <= 8 && CheckHoliday(17) == 1)
                        {
                            ot17 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no17);
                        }
                        else if (no17 > 8 && day17.ToString("dddd") == "Saturday" || no17 > 8 && day17.ToString("dddd") == "Sunday" || no17 > 8 && CheckHoliday(17) == 1)
                        {
                            ot17 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no17 -= 8;
                            ot17 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no17);
                        }

                    }
                    else if (no17 > 8)
                    {
                        ot17 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no17 - 8));

                    }
                    if (no18 != 0 && day18.ToString("dddd") == "Saturday" || no18 != 0 && day18.ToString("dddd") == "Sunday" || no18 != 0 && CheckHoliday(18) == 1)
                    {
                        if (no18 <= 8 && day18.ToString("dddd") == "Saturday" || no18 <= 8 && day18.ToString("dddd") == "Sunday" || no18 <= 8 && CheckHoliday(18) == 1)
                        {
                            ot18 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no18);
                        }
                        else if (no18 > 8 && day18.ToString("dddd") == "Saturday" || no18 > 8 && day18.ToString("dddd") == "Sunday" || no18 > 8 && CheckHoliday(18) == 1)
                        {
                            ot18 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no18 -= 8;
                            ot18 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no18);
                        }

                    }
                    else if (no18 > 8)
                    {
                        ot18 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no18 - 8));

                    }
                    if (no19 != 0 && day19.ToString("dddd") == "Saturday" || no19 != 0 && day19.ToString("dddd") == "Sunday" || no19 != 0 && CheckHoliday(19) == 1)
                    {
                        if (no19 <= 8 && day19.ToString("dddd") == "Saturday" || no19 <= 8 && day19.ToString("dddd") == "Sunday" || no19 <= 8 && CheckHoliday(19) == 1)
                        {
                            ot19 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no19);
                        }
                        else if (no19 > 8 && day19.ToString("dddd") == "Saturday" || no19 > 8 && day19.ToString("dddd") == "Sunday" || no19 > 8 && CheckHoliday(19) == 1)
                        {
                            ot19 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no19 -= 8;
                            ot19 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no19);
                        }

                    }
                    else if (no19 > 8)
                    {
                        ot19 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no19 - 8));

                    }
                    if (no20 != 0 && day20.ToString("dddd") == "Saturday" || no20 != 0 && day20.ToString("dddd") == "Sunday" || no20 != 0 && CheckHoliday(20) == 1)
                    {
                        if (no20 <= 8 && day20.ToString("dddd") == "Saturday" || no20 <= 8 && day20.ToString("dddd") == "Sunday" || no20 <= 8 && CheckHoliday(20) == 1)
                        {
                            ot20 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no20);
                        }
                        else if (no20 > 8 && day20.ToString("dddd") == "Saturday" || no20 > 8 && day20.ToString("dddd") == "Sunday" || no20 > 8 && CheckHoliday(20) == 1)
                        {
                            ot20 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8);
                            no20 -= 8;
                            ot20 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no20);
                        }

                    }
                    else if (no20 > 8)
                    {
                        ot20 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no20 - 8));

                    }
                    if (no21 != 0 && day2.ToString("dddd") == "Saturday" || no21 != 0 && day21.ToString("dddd") == "Sunday" || no21 != 0 && CheckHoliday(21) == 1)
                    {
                        if (no21 <= 8 && day21.ToString("dddd") == "Saturday" || no21 <= 8 && day21.ToString("dddd") == "Sunday" || no21 <= 8 && CheckHoliday(21) == 1)
                        {
                            ot21 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no21);
                        }
                        else if (no21 > 8 && day21.ToString("dddd") == "Saturday" || no21 > 8 && day21.ToString("dddd") == "Sunday" || no21 > 8 && CheckHoliday(21) == 1)
                        {
                            ot21 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no21 -= 8;
                            ot21 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no2);
                        }

                    }
                    else if (no21 > 8)
                    {
                        ot21 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no21 - 8));
                    }
                    if (no22 != 0 && day22.ToString("dddd") == "Saturday" || no22 != 0 && day22.ToString("dddd") == "Sunday" || no22 != 0 && CheckHoliday(21) == 1)
                    {
                        if (no22 <= 8 && day22.ToString("dddd") == "Saturday" || no22 <= 8 &&  day22.ToString("dddd") == "Sunday" || no22 <= 8 && CheckHoliday(22) == 1)
                        {
                            ot22 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no22);
                        }
                        else if (no22 > 8 && day22.ToString("dddd") == "Saturday" || day22.ToString("dddd") == "Sunday" || no22 > 8 && CheckHoliday(22) == 1)
                        {
                            ot22 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no22 -= 8;
                            ot22 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no22);
                        }

                    }
                    else if (no22 > 8)
                    {
                        ot22 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no22 - 8));
                    }
                    if (no23 != 0 && day23.ToString("dddd") == "Saturday" || no23 != 0 && day23.ToString("dddd") == "Sunday" || no23 != 0 && CheckHoliday(23) == 1)
                    {
                        if (no23 <= 8 && day23.ToString("dddd") == "Saturday" || no23 <= 8 && day23.ToString("dddd") == "Sunday" || no23 <= 8 && CheckHoliday(23) == 1)
                        {
                            ot23 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no23);
                        }
                        else if (no23 > 8 && day23.ToString("dddd") == "Saturday" || no23 > 8 && day23.ToString("dddd") == "Sunday" || no23 > 8 && CheckHoliday(23) == 1)
                        {
                            ot23 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no23 -= 8;
                            ot23 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no23);
                        }

                    }
                    else if (no23 > 8)
                    {
                        ot23 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no23 - 8));
                    }
                    if (no24 != 0 && day24.ToString("dddd") == "Saturday" || no24 != 0 && day24.ToString("dddd") == "Sunday" || no24 != 0 && CheckHoliday(24) == 1)
                    {
                        if (no24 <= 8 && day24.ToString("dddd") == "Saturday" || no24 <= 8 && day24.ToString("dddd") == "Sunday" || no24 <= 8 && CheckHoliday(24) == 1)
                        {
                            ot24 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no24);
                        }
                        else if (no24 > 8 && day24.ToString("dddd") == "Saturday" || no24 > 8 && day24.ToString("dddd") == "Sunday" || no24 > 8 && CheckHoliday(24) == 1)
                        {
                            ot24 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no24-= 8;
                            ot24 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no24);
                        }

                    }
                    else if (no24 > 8)
                    {
                        ot24 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no24 - 8));
                    }
                    if (no25 != 0 && day25.ToString("dddd") == "Saturday" || no25 != 0 && day25.ToString("dddd") == "Sunday" || no25 != 0 && CheckHoliday(25) == 1)
                    {
                        if (no25 <= 8 && day25.ToString("dddd") == "Saturday" || no25 <= 8 && day25.ToString("dddd") == "Sunday" || no25 <= 8 && CheckHoliday(25) == 1)
                        {
                            ot25 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no25);
                        }
                        else if (no25 > 8 && day25.ToString("dddd") == "Saturday" || no25 > 8 && day25.ToString("dddd") == "Sunday" || no25 > 8 && CheckHoliday(25) == 1)
                        {
                            ot25 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no25 -= 8;
                            ot25 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no25);
                        }

                    }
                    else if (no25 > 8)
                    {
                        ot25 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no25 - 8));
                    }
                    if (no26 != 0 && day26.ToString("dddd") == "Saturday" || no26 != 0 && day26.ToString("dddd") == "Sunday" || no26 != 0 && CheckHoliday(26) == 1)
                    {
                        if (no26 <= 8 && day2.ToString("dddd") == "Saturday" || no26 <= 8 && day26.ToString("dddd") == "Sunday" || no26 <= 8 && CheckHoliday(26) == 1)
                        {
                            ot26 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no26);
                        }
                        else if (no26 > 8 && day2.ToString("dddd") == "Saturday" || no26 > 8 && day26.ToString("dddd") == "Sunday" || no26 > 8 && CheckHoliday(26) == 1)
                        {
                            ot26 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no26 -= 8;
                            ot26 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no26);
                        }

                    }
                    else if (no26 > 8)
                    {
                        ot26 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no26 - 8));
                    }
                    if (no27 != 0 && day27.ToString("dddd") == "Saturday" || no27 != 0 && day27.ToString("dddd") == "Sunday" || no27 != 0 && CheckHoliday(27) == 1)
                    {
                        if (no27 <= 8 && day27.ToString("dddd") == "Saturday" || no27 <= 8 && day27.ToString("dddd") == "Sunday" || no27 <= 8 && CheckHoliday(27) == 1)
                        {
                            ot27 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no27);
                        }
                        else if (no27 > 8 && day2.ToString("dddd") == "Saturday" || no27 > 8 && day27.ToString("dddd") == "Sunday" || no27 > 8 && CheckHoliday(27) == 1)
                        {
                            ot27 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no27 -= 8;
                            ot27 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no27);
                        }

                    }
                    else if (no27 > 8)
                    {
                        ot27 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no27 - 8));
                    }
                    if (no28 != 0 && day28.ToString("dddd") == "Saturday" || no28 != 0 && day28.ToString("dddd") == "Sunday" || no28 != 0 && CheckHoliday(28) == 1)
                    {
                        if (no28 <= 8 && day28.ToString("dddd") == "Saturday" || no28 <= 8 && day28.ToString("dddd") == "Sunday" || no28 <= 8 && CheckHoliday(28) == 1)
                        {
                            ot28 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no28);
                        }
                        else if (no28 > 8 && day28.ToString("dddd") == "Saturday" || no28 > 8 && day28.ToString("dddd") == "Sunday" || no28 > 8 && CheckHoliday(28) == 1)
                        {
                            ot28 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no28 -= 8;
                            ot28 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no28);
                        }

                    }
                    else if (no28 > 8)
                    {
                        ot28 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no28 - 8));
                    }
                    if (no29 != 0 && day29.ToString("dddd") == "Saturday" || no29 != 0 && day29.ToString("dddd") == "Sunday" || no29 != 0 && CheckHoliday(29) == 1)
                    {
                        if (no29 <= 8 && day29.ToString("dddd") == "Saturday" || no29 <= 8 && day29.ToString("dddd") == "Sunday" || no29 <= 8 && CheckHoliday(29) == 1)
                        {
                            ot29 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no29);
                        }
                        else if (no29 > 8 && day29.ToString("dddd") == "Saturday" || no29 > 8 && day29.ToString("dddd") == "Sunday" || no29 > 8 && CheckHoliday(29) == 1)
                        {
                            ot29 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no29 -= 8;
                            ot29+= (float)(basesalary / 30.00 / 8.00 * 3.00 * no29);
                        }

                    }
                    else if (no29 > 8)
                    {
                        ot29 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no29 - 8));
                    }
                    if (no30 != 0 && day30.ToString("dddd") == "Saturday" || no30 != 0 && day30.ToString("dddd") == "Sunday" || no30 != 0 && CheckHoliday(30) == 1)
                    {
                        if (no30 <= 8 && day30.ToString("dddd") == "Saturday" || no30 <= 8 && day30.ToString("dddd") == "Sunday" || no30 <= 8 && CheckHoliday(30) == 1)
                        {
                            ot30 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no30);
                        }
                        else if (no30 > 8 && day30.ToString("dddd") == "Saturday" || no30 > 8 && day30.ToString("dddd") == "Sunday" || no30 > 8 && CheckHoliday(30) == 1)
                        {
                            ot30 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no30 -= 8;
                            ot30 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no30);
                        }

                    }
                    else if (no30 > 8)
                    {
                        ot30 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no30 - 8));
                    }

                    if (no31 != 0 && day31.ToString("dddd") == "Saturday" || no31 != 0 && day31.ToString("dddd") == "Sunday" || no31 != 0 && CheckHoliday(31) == 1)
                    {
                        
                        if (no31 <= 8 && day31.ToString("dddd") == "Saturday" || no31 <= 8 && day31.ToString("dddd") == "Sunday" || no31 <= 8 && CheckHoliday(31) == 1)
                        {
                            ot31 = (float)(basesalary / 30.00 / 8.00 * 2.00 * no31);
                        }
                        else if (no31 > 8 && day31.ToString("dddd") == "Saturday" || no31 > 8 && day31.ToString("dddd") == "Sunday" || no31 > 8 && CheckHoliday(31) == 1)
                        {
                            ot31 = (float)(basesalary / 30.00 / 8.00 * 2.00 * 8.00);
                            no31 -= 8;
                            ot31 += (float)(basesalary / 30.00 / 8.00 * 3.00 * no31);
                        }

                    }
                    else if (no31 > 8)
                    {
                        ot31 = (float)(basesalary / 30.00 / 8.00 * 1.5 * (no31 - 8));
                    }

                    otsum = ot1 + ot2 + ot3 + ot4 + ot5 + ot6 + ot7 + ot8 + ot9 + ot10;
                    otsum += ot11 + ot12 + ot13 + ot14 + ot15 + ot16 + ot17 + ot18 + ot19 + ot20;
                    otsum += ot21 + ot22 + ot23 + ot24 + ot25 + ot26 + ot27 + ot28 + ot29 + ot30 + ot31;
                    
                }
                //MessageBox.Show(ot4.ToString());
                //MessageBox.Show(no1 + day1.ToString("dddd") + " " + ot1 + "\n" + no2 + day2.ToString("dddd") + " " + ot2 + "\n" + no3 + day3.ToString("dddd") + " " + ot3 + "\n" + no4 + day4.ToString("dddd") + " " + ot4 + "\n" + no5 + day5.ToString("dddd") + ot5 + "\n" + no6 + day6.ToString("dddd") + ot6 + "\n" + no7 + day7.ToString("dddd") + ot7 + "\n" + no8 + day8.ToString("dddd") + ot8 + "\n" + no9 + day9.ToString("dddd") + ot9 + "\n" + no10 + day10.ToString("dddd") + ot10 + "\n");
                //MessageBox.Show(no11 + day11.ToString("dddd") + ot11 + "\n" + no12 + day12.ToString("dddd") + ot12 + "\n" + no13 + day13.ToString("dddd") + ot13 + "\n" + no14 + day14.ToString("dddd") + ot14 + "\n" + no15 + day15.ToString("dddd") + ot15 + "\n" + no16 + day16.ToString("dddd") + ot16 + "\n" + no17 + day17.ToString("dddd") + ot17 + "\n" + no18 + day18.ToString("dddd") + ot18 + "\n" + no19 + day19.ToString("dddd") + ot19 + "\n" + no20 + day20.ToString("dddd") + ot20 + "\n");
                //MessageBox.Show(no21 + day21.ToString("dddd") + ot21 + "\n" + no22 + day22.ToString("dddd") + ot23 + "\n" + no23 + day23.ToString("dddd") + ot23 + "\n" + no24 + day24.ToString("dddd") + ot24 + "\n" + no25 + day25.ToString("dddd") + ot25 + "\n" + no26 + day26.ToString("dddd") + ot26 + "\n" + no27 + day27.ToString("dddd") + ot27 + "\n" + no28 + day28.ToString("dddd") + ot28 + "\n" + no29 + day29.ToString("dddd") + ot29 + "\n" + no30 + day30.ToString("dddd") + ot30 + "\n" + no31 + day31.ToString("dddd") + ot31);
                return otsum;
               
            } 
        }
        public int CheckHoliday(int date)
        {
            int yes = 0;
            int m = int.Parse(ComboMonth(comboBoxM.Text));
            int y = int.Parse(comboBoxY.Text);
            


                using (IDbConnection cnn = new OleDbConnection(con))
                {

                    string sql_Text = "SELECT *";
                    sql_Text += "FROM Holiday ";
                    sql_Text += "WHERE MONTH(HoliDay_Date) = " + m + " AND YEAR(HoliDay_Date) = " + y + " AND Day(HoliDay_Date) = " + date;


                    DataTable tbl = new DataTable();
                    DateTime day = new DateTime(y, m, date);

                    using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, (OleDbConnection)cnn))
                    {
                        adapter.Fill(tbl);
                        foreach (DataRow dr in tbl.Rows)
                        {

                            if (dr[0].ToString() == day.ToString())
                            {
                                yes = 1;

                            }
                        }

                    }

                }
            
            return yes;
        }

        private void butUpdate_Click(object sender, EventArgs e)
        {
            SendEMP();
            ShowSalary(1);

        }

        private void CheckJoin(string jdate,int id, int basesalary, int executive, int travel, int housing, int language)
        {
           
            DateTime joindate = Convert.ToDateTime(jdate);
            //MessageBox.Show(joindate.ToString("yyyy/MM/dd"));
            int m = int.Parse(ComboMonth(comboBoxM.Text));
            int y = int.Parse(comboBoxY.Text);
            DateTime Fdate = new DateTime(y, m, 1);

            if (joindate.ToString("yyyy/MM") == Fdate.ToString("yyyy/MM"))
            {
                if (joindate.ToString("dd") != Fdate.ToString("dd"))
                {
                    basesalary = ((int)(basesalary / 30.00 * workday(id, jdate, "")));
                    executive = ((int)(executive / 30.00 * workday(id, jdate, "")));
                    travel = ((int)(travel / 30.00 * workday(id, jdate, "")));
                    housing = ((int)(housing / 30.00 * workday(id, jdate, "")));
                    language = ((int)(language / 30.00 * workday(id, jdate, "")));
                    int sum = basesalary + executive + travel + housing + language;
                   
                    IDbTransaction trans = null;
                    using (IDbConnection cnn = new OleDbConnection(con))
                    {



                        int salarytax = (int)(SalaryTax(0, sum * 12) / 12);
                        int socialP = SocialPremiums(id, 0);
                        int netsalary = sum - (salarytax + socialP);
                        SalaryTax(id, 0);
                        string sql_Text = "UPDATE Net_Salary SET Base_salary = " + basesalary;
                        sql_Text += ",Executive_Allowance = " + executive + ",Travel_Allowance = " + travel + ",Housing_Allowance = " + housing;
                        sql_Text += ",Language_Allowance = " + language;
                        sql_Text += ",Total_Salary = " + sum + ",Salary_Tax = " + salarytax + ",Insurance = " + socialP + ",Net_Salary = " + netsalary;
                        sql_Text += " WHERE MONTH(Pay_Date) = " + m + " AND YEAR(Pay_Date) = " + y + " AND EMP_ID = " + id;
                        using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                        {
                            try
                            {
                                cnn.Open();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                            cmd_command.Connection = cnn;
                            cmd_command.Transaction = trans;

                            try
                            {
                                cmd_command.ExecuteNonQuery();
                                trans.Commit();


                            }
                            catch (OleDbException ex)
                            {
                                trans.Rollback();
                                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }

                        }

                    }

                }
            }
        }
        private void CheckResign(string rdate, int id, int basesalary, int executive, int travel, int housing, int language)
        {
            if (rdate != "")
            {
                DateTime resigndate = Convert.ToDateTime(rdate);
                //MessageBox.Show( resigndate.ToString("yyyy/MM/dd"));
                int m = int.Parse(ComboMonth(comboBoxM.Text));

                int y = int.Parse(comboBoxY.Text);
                DateTime Ldate = new DateTime();

                if (m == 2)
                {
                    try
                    {
                        Ldate = new DateTime(y, m, 29);
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {

                    if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12)
                    {
                        Ldate = new DateTime(y, m, 31);
                    }
                    else
                    {
                        Ldate = new DateTime(y, m, 30);
                    }

                        
                }
                 

                if (resigndate.ToString("yyyy/MM") == Ldate.ToString("yyyy/MM"))
                {
                    if (resigndate.ToString("dd") != Ldate.ToString("dd"))
                    {
                       
                        basesalary = ((int)(basesalary / 30.00 * workday(id, "", rdate)));
                      
                        executive = ((int)(executive / 30.00 * workday(id, "", rdate)));
                        travel = ((int)(travel / 30.00 * workday(id, "", rdate)));
                        housing = ((int)(housing / 30.00 * workday(id, "", rdate)));
                        language = ((int)(language / 30.00 * workday(id, "", rdate)));
                        int sum = basesalary + executive + travel + housing + language;
                        IDbTransaction trans = null;
                        using (IDbConnection cnn = new OleDbConnection(con))
                        {



                            int salarytax = (int)(SalaryTax(0, sum * 12) / 12);
                            int socialP = SocialPremiums(id, 0);
                            int netsalary = sum - (salarytax + socialP);
                            SalaryTax(id, 0);
                            string sql_Text = "UPDATE Net_Salary SET Base_salary = " + basesalary;
                            sql_Text += ",Executive_Allowance = " + executive + ",Travel_Allowance = " + travel + ",Housing_Allowance = " + housing;
                            sql_Text += ",Language_Allowance = " + language;
                            sql_Text += ",Total_Salary = " + sum + ",Salary_Tax = " + salarytax + ",Insurance = " + socialP + ",Net_Salary = " + netsalary;
                            sql_Text += " WHERE MONTH(Pay_Date) = " + m + " AND YEAR(Pay_Date) = " + y + " AND EMP_ID = " + id;
                            using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                            {
                                try
                                {
                                    cnn.Open();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                                cmd_command.Connection = cnn;
                                cmd_command.Transaction = trans;

                                try
                                {
                                    cmd_command.ExecuteNonQuery();
                                    trans.Commit();


                                }
                                catch (OleDbException ex)
                                {
                                    trans.Rollback();
                                    MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }

                            }

                        }
                    }
                }
            }
        }


        public int workday(int id,string jdate ,string rdate)
        {
            string month = ComboMonth(comboBoxM.Text);
            string year = comboBoxY.Text;
            int dayjoin =0;
            int dayresign = 0;
            if (jdate != "")
            {
                DateTime joindate = Convert.ToDateTime(jdate);
                dayjoin = int.Parse(joindate.ToString("dd"));
            }
            else if (rdate != "") {

                DateTime resigndate = Convert.ToDateTime(rdate);
                dayresign = int.Parse(resigndate.ToString("dd"));
            }
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                string SQL = "SELECT Salary.Base_salary ,Work_Day.* FROM Work_Day ";
                SQL += "INNER JOIN Salary ON Salary.Salary_ID = Work_Day.作業者NO ";
                SQL += "WHERE  年月 = '" + year + month + "' AND 作業者NO = " + id;
                DataTable tbl = new DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(SQL, con);
                adapter.Fill(tbl);
                DateTime day1 = new DateTime(int.Parse(year), int.Parse(month), 1);
                int work1 = int.Parse(day1.ToString("dd"));
                DateTime day2 = new DateTime(int.Parse(year), int.Parse(month), 2);
                int work2 = int.Parse(day2.ToString("dd"));
                DateTime day3 = new DateTime(int.Parse(year), int.Parse(month), 3);
                int work3 = int.Parse(day3.ToString("dd"));
                DateTime day4 = new DateTime(int.Parse(year), int.Parse(month), 4);
                int work4 = int.Parse(day4.ToString("dd"));
                DateTime day5 = new DateTime(int.Parse(year), int.Parse(month), 5);
                int work5 = int.Parse(day5.ToString("dd"));
                DateTime day6 = new DateTime(int.Parse(year), int.Parse(month), 6);
                int work6 = int.Parse(day6.ToString("dd"));
                DateTime day7 = new DateTime(int.Parse(year), int.Parse(month), 7);
                int work7 = int.Parse(day7.ToString("dd"));
                DateTime day8 = new DateTime(int.Parse(year), int.Parse(month), 8);
                int work8 = int.Parse(day8.ToString("dd"));
                DateTime day9 = new DateTime(int.Parse(year), int.Parse(month), 9);
                int work9 = int.Parse(day9.ToString("dd"));
                DateTime day10 = new DateTime(int.Parse(year), int.Parse(month), 10);
                int work10 = int.Parse(day10.ToString("dd"));
                DateTime day11 = new DateTime(int.Parse(year), int.Parse(month), 11);
                int work11 = int.Parse(day11.ToString("dd"));
                DateTime day12 = new DateTime(int.Parse(year), int.Parse(month), 12);
                int work12 = int.Parse(day12.ToString("dd"));
                DateTime day13 = new DateTime(int.Parse(year), int.Parse(month), 13);
                int work13 = int.Parse(day13.ToString("dd"));
                DateTime day14 = new DateTime(int.Parse(year), int.Parse(month), 14);
                int work14 = int.Parse(day14.ToString("dd"));
                DateTime day15 = new DateTime(int.Parse(year), int.Parse(month), 15);
                int work15 = int.Parse(day15.ToString("dd"));
                DateTime day16 = new DateTime(int.Parse(year), int.Parse(month), 16);
                int work16 = int.Parse(day16.ToString("dd"));
                DateTime day17 = new DateTime(int.Parse(year), int.Parse(month), 17);
                int work17 = int.Parse(day17.ToString("dd"));
                DateTime day18 = new DateTime(int.Parse(year), int.Parse(month), 18);
                int work18 = int.Parse(day18.ToString("dd"));
                DateTime day19 = new DateTime(int.Parse(year), int.Parse(month), 19);
                int work19 = int.Parse(day19.ToString("dd"));
                DateTime day20 = new DateTime(int.Parse(year), int.Parse(month), 20);
                int work20 = int.Parse(day20.ToString("dd"));
                DateTime day21 = new DateTime(int.Parse(year), int.Parse(month), 21);
                int work21 = int.Parse(day21.ToString("dd"));
                DateTime day22 = new DateTime(int.Parse(year), int.Parse(month), 22);
                int work22 = int.Parse(day22.ToString("dd"));
                DateTime day23 = new DateTime(int.Parse(year), int.Parse(month), 23);
                int work23 = int.Parse(day23.ToString("dd"));
                DateTime day24 = new DateTime(int.Parse(year), int.Parse(month), 24);
                int work24 = int.Parse(day24.ToString("dd"));
                DateTime day25 = new DateTime(int.Parse(year), int.Parse(month), 25);
                int work25 = int.Parse(day25.ToString("dd"));
                DateTime day26 = new DateTime(int.Parse(year), int.Parse(month), 26);
                int work26 = int.Parse(day26.ToString("dd"));
                DateTime day27 = new DateTime(int.Parse(year), int.Parse(month), 27);
                int work27 = int.Parse(day27.ToString("dd"));
                DateTime day28 = new DateTime(int.Parse(year), int.Parse(month), 28);
                int work28 = int.Parse(day28.ToString("dd"));
                int work29 = 0;
                int work30 = 0;
                int work31 = 0;

                if (int.Parse(month) == 2)
                {
                    try
                    {
                        DateTime day29 = new DateTime(int.Parse(year), int.Parse(month), 29);
                         work29 = int.Parse(day29.ToString("dd"));
                    }
                    catch (Exception ex)
                    {
                       
                    }

                }
                else
                {
                    DateTime day29 = new DateTime(int.Parse(year), int.Parse(month), 29);
                     work29 = int.Parse(day29.ToString("dd"));
                    DateTime day30 = new DateTime(int.Parse(year), int.Parse(month), 30);
                     work30 = int.Parse(day30.ToString("dd"));
                    if (int.Parse(month) == 1 || int.Parse(month) == 3 || int.Parse(month) == 5 || int.Parse(month) == 7 || int.Parse(month) == 8 || int.Parse(month) == 10 || int.Parse(month) == 12)
                    {
                        DateTime day31 = new DateTime(int.Parse(year), int.Parse(month), 31);
                        work31 = int.Parse(day31.ToString("dd"));
                    }

                }


                int no1 = 0;
                int no2 = 0;
                int no3 = 0;
                int no4 = 0;
                int no5 = 0;
                int no6 = 0;
                int no7 = 0;
                int no8 = 0;
                int no9 = 0;
                int no10 = 0;
                int no11 = 0;
                int no12 = 0;
                int no13 = 0;
                int no14 = 0;
                int no15 = 0;
                int no16 = 0;
                int no17 = 0;
                int no18 = 0;
                int no19 = 0;
                int no20 = 0;
                int no21 = 0;
                int no22 = 0;
                int no23 = 0;
                int no24 = 0;
                int no25 = 0;
                int no26 = 0;
                int no27 = 0;
                int no28 = 0;
                int no29 = 0;
                int no30 = 0;
                int no31 = 0;
                int t = 0;
                foreach (DataRow dr in tbl.Rows)
                {

                    
                    no1 = int.Parse(dr[11].ToString());
                    no2 = int.Parse(dr[12].ToString());
                    no3 = int.Parse(dr[13].ToString());
                    no4 = int.Parse(dr[14].ToString());
                    no5 = int.Parse(dr[15].ToString());
                    no6 = int.Parse(dr[16].ToString());
                    no7 = int.Parse(dr[17].ToString());
                    no8 = int.Parse(dr[18].ToString());
                    no9 = int.Parse(dr[19].ToString());
                    no10 = int.Parse(dr[20].ToString());
                    no11 = int.Parse(dr[21].ToString());
                    no12 = int.Parse(dr[22].ToString());
                    no13 = int.Parse(dr[23].ToString());
                    no14 = int.Parse(dr[24].ToString());
                    no15 = int.Parse(dr[25].ToString());
                    no16 = int.Parse(dr[26].ToString());
                    no17 = int.Parse(dr[27].ToString());
                    no18 = int.Parse(dr[28].ToString());
                    no19 = int.Parse(dr[29].ToString());
                    no20 = int.Parse(dr[30].ToString());
                    no21 = int.Parse(dr[31].ToString());
                    no22 = int.Parse(dr[32].ToString());
                    no23 = int.Parse(dr[33].ToString());
                    no24 = int.Parse(dr[34].ToString());
                    no25 = int.Parse(dr[35].ToString());
                    no26 = int.Parse(dr[36].ToString());
                    no27 = int.Parse(dr[37].ToString());
                    no28 = int.Parse(dr[38].ToString());
                    no29 = 0;
                    no30 = 0;
                    no31 = 0;
                    if (month == "02")
                    {
                        try
                        {
                           
                            no29 = int.Parse(dr[39].ToString());
                        }
                        catch (Exception ex)
                        {
                            //MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        

                        no29 = int.Parse(dr[39].ToString());
                        no30 = int.Parse(dr[40].ToString());
                        if (month == "01" || month == "03" || month == "05" || month == "07" || month == "08" || month == "10" || month == "12")
                        {
                            no31 = int.Parse(dr[41].ToString());
                           
                        }
                    }

                    if (work1 >= dayjoin && dayjoin != 0 || work1 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work2 >= dayjoin && dayjoin != 0 || work2 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work3 >= dayjoin && dayjoin != 0 || work3 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work4 >= dayjoin && dayjoin != 0 || work4 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work5 >= dayjoin && dayjoin != 0 || work5 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work6 >= dayjoin && dayjoin != 0 || work6 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work7 >= dayjoin && dayjoin != 0 || work7 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work8 >= dayjoin && dayjoin != 0 || work8 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work9 >= dayjoin && dayjoin != 0 || work9 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work10 >= dayjoin && dayjoin != 0 || work10 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work11 >= dayjoin && dayjoin != 0 || work11 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work12 >= dayjoin && dayjoin != 0 || work12 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work13 >= dayjoin && dayjoin != 0 || work13 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work14 >= dayjoin && dayjoin != 0 || work14 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work15 >= dayjoin && dayjoin != 0 || work15 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work16 >= dayjoin && dayjoin != 0 || work16 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work17 >= dayjoin && dayjoin != 0 || work17 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work18 >= dayjoin && dayjoin != 0 || work18 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work19 >= dayjoin && dayjoin != 0 || work19 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work20 >= dayjoin && dayjoin != 0 || work20 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work21 >= dayjoin && dayjoin != 0 || work21 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work22 >= dayjoin && dayjoin != 0 || work22 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work23 >= dayjoin && dayjoin != 0 || work23 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work24 >= dayjoin && dayjoin != 0 || work24 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work25 >= dayjoin && dayjoin != 0 || work25 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work26 >= dayjoin && dayjoin != 0 || work26 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work27 >= dayjoin && dayjoin != 0 || work27 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work28 >= dayjoin && dayjoin != 0 || work28 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work29 >= dayjoin && dayjoin != 0 || work29 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work30 >= dayjoin && dayjoin != 0 || work30 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }
                    if (work31 >= dayjoin && dayjoin != 0 || work31 <= dayresign && dayresign != 0)
                    {
                        t++;
                    }

                }
                return t;

                }

            }

        private void button1_Click(object sender, EventArgs e)
        {
            int salarytax = (int)(SalaryTax(1, 52000 * 12) / 12);
            MessageBox.Show(salarytax.ToString());
            MessageBox.Show("ประกัน "+SocialPremiums(1, 1).ToString());
        }
    }
}
