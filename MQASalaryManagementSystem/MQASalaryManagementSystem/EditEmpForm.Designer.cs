﻿
namespace MQASalaryManagementSystem
{
    partial class EditEmpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbPosition = new System.Windows.Forms.ComboBox();
            this.butclose = new System.Windows.Forms.Button();
            this.tbSearchEdit = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.butedit = new System.Windows.Forms.Button();
            this.dataGridViewEditEmp = new System.Windows.Forms.DataGridView();
            this.dateJoin = new System.Windows.Forms.DateTimePicker();
            this.dateBirth = new System.Windows.Forms.DateTimePicker();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.tbPhone = new System.Windows.Forms.TextBox();
            this.tbNname = new System.Windows.Forms.TextBox();
            this.tbLname = new System.Windows.Forms.TextBox();
            this.tbFname = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateResign = new System.Windows.Forms.DateTimePicker();
            this.checkBoxResign = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.labelID = new System.Windows.Forms.Label();
            this.guna2HScrollBar1 = new Guna.UI2.WinForms.Guna2HScrollBar();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEditEmp)).BeginInit();
            this.SuspendLayout();
            // 
            // cbPosition
            // 
            this.cbPosition.FormattingEnabled = true;
            this.cbPosition.Location = new System.Drawing.Point(542, 174);
            this.cbPosition.Name = "cbPosition";
            this.cbPosition.Size = new System.Drawing.Size(161, 21);
            this.cbPosition.TabIndex = 79;
            // 
            // butclose
            // 
            this.butclose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butclose.Location = new System.Drawing.Point(12, 468);
            this.butclose.Name = "butclose";
            this.butclose.Size = new System.Drawing.Size(75, 35);
            this.butclose.TabIndex = 78;
            this.butclose.Text = "Close";
            this.butclose.UseVisualStyleBackColor = true;
            this.butclose.Click += new System.EventHandler(this.butclose_Click);
            // 
            // tbSearchEdit
            // 
            this.tbSearchEdit.Location = new System.Drawing.Point(69, 37);
            this.tbSearchEdit.Multiline = true;
            this.tbSearchEdit.Name = "tbSearchEdit";
            this.tbSearchEdit.Size = new System.Drawing.Size(331, 29);
            this.tbSearchEdit.TabIndex = 77;
            this.tbSearchEdit.TextChanged += new System.EventHandler(this.tbSearchEdit_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 16);
            this.label9.TabIndex = 76;
            this.label9.Text = "Search";
            // 
            // butedit
            // 
            this.butedit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Location = new System.Drawing.Point(628, 468);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(75, 35);
            this.butedit.TabIndex = 75;
            this.butedit.Text = "Edit";
            this.butedit.UseVisualStyleBackColor = true;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // dataGridViewEditEmp
            // 
            this.dataGridViewEditEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEditEmp.Location = new System.Drawing.Point(12, 83);
            this.dataGridViewEditEmp.Name = "dataGridViewEditEmp";
            this.dataGridViewEditEmp.ReadOnly = true;
            this.dataGridViewEditEmp.RowHeadersVisible = false;
            this.dataGridViewEditEmp.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewEditEmp.Size = new System.Drawing.Size(388, 355);
            this.dataGridViewEditEmp.TabIndex = 74;
            this.dataGridViewEditEmp.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewEditEmp_CellClick);
            // 
            // dateJoin
            // 
            this.dateJoin.CustomFormat = "yyyy-MM-dd";
            this.dateJoin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateJoin.Location = new System.Drawing.Point(542, 288);
            this.dateJoin.Name = "dateJoin";
            this.dateJoin.Size = new System.Drawing.Size(161, 20);
            this.dateJoin.TabIndex = 73;
            // 
            // dateBirth
            // 
            this.dateBirth.CustomFormat = "yyyy-MM-dd";
            this.dateBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateBirth.Location = new System.Drawing.Point(542, 254);
            this.dateBirth.Name = "dateBirth";
            this.dateBirth.Size = new System.Drawing.Size(161, 20);
            this.dateBirth.TabIndex = 72;
            // 
            // tbAddress
            // 
            this.tbAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAddress.Location = new System.Drawing.Point(542, 328);
            this.tbAddress.Multiline = true;
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(161, 96);
            this.tbAddress.TabIndex = 71;
            // 
            // tbPhone
            // 
            this.tbPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPhone.Location = new System.Drawing.Point(542, 208);
            this.tbPhone.MaxLength = 10;
            this.tbPhone.Multiline = true;
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(161, 29);
            this.tbPhone.TabIndex = 70;
           
            // 
            // tbNname
            // 
            this.tbNname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNname.Location = new System.Drawing.Point(542, 138);
            this.tbNname.Multiline = true;
            this.tbNname.Name = "tbNname";
            this.tbNname.Size = new System.Drawing.Size(161, 29);
            this.tbNname.TabIndex = 69;
            // 
            // tbLname
            // 
            this.tbLname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLname.Location = new System.Drawing.Point(542, 103);
            this.tbLname.Multiline = true;
            this.tbLname.Name = "tbLname";
            this.tbLname.Size = new System.Drawing.Size(161, 29);
            this.tbLname.TabIndex = 68;
            // 
            // tbFname
            // 
            this.tbFname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFname.Location = new System.Drawing.Point(542, 62);
            this.tbFname.Multiline = true;
            this.tbFname.Name = "tbFname";
            this.tbFname.Size = new System.Drawing.Size(161, 29);
            this.tbFname.TabIndex = 67;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(460, 292);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 16);
            this.label8.TabIndex = 66;
            this.label8.Text = "Join date :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(460, 254);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 16);
            this.label7.TabIndex = 65;
            this.label7.Text = "Birth date :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(465, 329);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 16);
            this.label6.TabIndex = 64;
            this.label6.Text = "Adress :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(460, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 16);
            this.label5.TabIndex = 63;
            this.label5.Text = "Phone :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(460, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 16);
            this.label4.TabIndex = 62;
            this.label4.Text = "Position :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(458, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 16);
            this.label3.TabIndex = 61;
            this.label3.Text = "Nick name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(460, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 16);
            this.label2.TabIndex = 60;
            this.label2.Text = "Last name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(460, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 16);
            this.label1.TabIndex = 59;
            this.label1.Text = "Frist name :";
            // 
            // dateResign
            // 
            this.dateResign.CustomFormat = " ";
            this.dateResign.Enabled = false;
            this.dateResign.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateResign.Location = new System.Drawing.Point(542, 430);
            this.dateResign.Name = "dateResign";
            this.dateResign.Size = new System.Drawing.Size(161, 20);
            this.dateResign.TabIndex = 81;
            this.dateResign.Value = new System.DateTime(2021, 7, 5, 0, 0, 0, 0);
            this.dateResign.ValueChanged += new System.EventHandler(this.dateResign_ValueChanged);
            // 
            // checkBoxResign
            // 
            this.checkBoxResign.AutoSize = true;
            this.checkBoxResign.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxResign.Location = new System.Drawing.Point(444, 431);
            this.checkBoxResign.Name = "checkBoxResign";
            this.checkBoxResign.Size = new System.Drawing.Size(92, 19);
            this.checkBoxResign.TabIndex = 82;
            this.checkBoxResign.Text = "Resign date";
            this.checkBoxResign.UseVisualStyleBackColor = true;
            this.checkBoxResign.CheckedChanged += new System.EventHandler(this.checkBoxResign_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(458, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 16);
            this.label10.TabIndex = 83;
            this.label10.Text = "Employee ID :";
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelID.Location = new System.Drawing.Point(556, 37);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(0, 16);
            this.labelID.TabIndex = 84;
            // 
            // guna2HScrollBar1
            // 
            this.guna2HScrollBar1.HoverState.Parent = null;
            this.guna2HScrollBar1.InUpdate = false;
            this.guna2HScrollBar1.LargeChange = 10;
            this.guna2HScrollBar1.Location = new System.Drawing.Point(15, 420);
            this.guna2HScrollBar1.Name = "guna2HScrollBar1";
            this.guna2HScrollBar1.PressedState.Parent = this.guna2HScrollBar1;
            this.guna2HScrollBar1.ScrollbarSize = 18;
            this.guna2HScrollBar1.Size = new System.Drawing.Size(300, 18);
            this.guna2HScrollBar1.TabIndex = 85;
            this.guna2HScrollBar1.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // EditEmpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 515);
            this.Controls.Add(this.guna2HScrollBar1);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.checkBoxResign);
            this.Controls.Add(this.dateResign);
            this.Controls.Add(this.cbPosition);
            this.Controls.Add(this.butclose);
            this.Controls.Add(this.tbSearchEdit);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.butedit);
            this.Controls.Add(this.dataGridViewEditEmp);
            this.Controls.Add(this.dateJoin);
            this.Controls.Add(this.dateBirth);
            this.Controls.Add(this.tbAddress);
            this.Controls.Add(this.tbPhone);
            this.Controls.Add(this.tbNname);
            this.Controls.Add(this.tbLname);
            this.Controls.Add(this.tbFname);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "EditEmpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Employee";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEditEmp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbPosition;
        private System.Windows.Forms.Button butclose;
        private System.Windows.Forms.TextBox tbSearchEdit;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.DataGridView dataGridViewEditEmp;
        private System.Windows.Forms.DateTimePicker dateJoin;
        private System.Windows.Forms.DateTimePicker dateBirth;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.TextBox tbPhone;
        private System.Windows.Forms.TextBox tbNname;
        private System.Windows.Forms.TextBox tbLname;
        private System.Windows.Forms.TextBox tbFname;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateResign;
        private System.Windows.Forms.CheckBox checkBoxResign;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelID;
        private Guna.UI2.WinForms.Guna2HScrollBar guna2HScrollBar1;
    }
}