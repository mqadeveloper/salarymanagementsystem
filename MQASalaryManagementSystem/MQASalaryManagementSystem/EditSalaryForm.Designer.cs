﻿
namespace MQASalaryManagementSystem
{
    partial class EditSalaryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.tbOT = new System.Windows.Forms.TextBox();
            this.tbBonus = new System.Windows.Forms.TextBox();
            this.tbLanguage = new System.Windows.Forms.TextBox();
            this.tbHosing = new System.Windows.Forms.TextBox();
            this.tbTraval = new System.Windows.Forms.TextBox();
            this.tbExecutive = new System.Windows.Forms.TextBox();
            this.tbBaseSalary = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.tbEditSalarySearch = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.butEditSalary = new System.Windows.Forms.Button();
            this.dataGridViewEditSalary = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelSalaryID = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.guna2HScrollBar1 = new Guna.UI2.WinForms.Guna2HScrollBar();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEditSalary)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(608, 107);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(0, 16);
            this.labelName.TabIndex = 66;
            // 
            // tbOT
            // 
            this.tbOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOT.Location = new System.Drawing.Point(547, 309);
            this.tbOT.Multiline = true;
            this.tbOT.Name = "tbOT";
            this.tbOT.Size = new System.Drawing.Size(156, 24);
            this.tbOT.TabIndex = 65;
            // 
            // tbBonus
            // 
            this.tbBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBonus.Location = new System.Drawing.Point(547, 279);
            this.tbBonus.Multiline = true;
            this.tbBonus.Name = "tbBonus";
            this.tbBonus.Size = new System.Drawing.Size(156, 24);
            this.tbBonus.TabIndex = 64;
            // 
            // tbLanguage
            // 
            this.tbLanguage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLanguage.Location = new System.Drawing.Point(547, 249);
            this.tbLanguage.Multiline = true;
            this.tbLanguage.Name = "tbLanguage";
            this.tbLanguage.Size = new System.Drawing.Size(156, 24);
            this.tbLanguage.TabIndex = 63;
            // 
            // tbHosing
            // 
            this.tbHosing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbHosing.Location = new System.Drawing.Point(547, 219);
            this.tbHosing.Multiline = true;
            this.tbHosing.Name = "tbHosing";
            this.tbHosing.Size = new System.Drawing.Size(156, 24);
            this.tbHosing.TabIndex = 62;
            // 
            // tbTraval
            // 
            this.tbTraval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTraval.Location = new System.Drawing.Point(547, 190);
            this.tbTraval.Multiline = true;
            this.tbTraval.Name = "tbTraval";
            this.tbTraval.Size = new System.Drawing.Size(156, 24);
            this.tbTraval.TabIndex = 61;
            // 
            // tbExecutive
            // 
            this.tbExecutive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbExecutive.Location = new System.Drawing.Point(547, 160);
            this.tbExecutive.Multiline = true;
            this.tbExecutive.Name = "tbExecutive";
            this.tbExecutive.Size = new System.Drawing.Size(156, 24);
            this.tbExecutive.TabIndex = 60;
            // 
            // tbBaseSalary
            // 
            this.tbBaseSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBaseSalary.Location = new System.Drawing.Point(547, 131);
            this.tbBaseSalary.Multiline = true;
            this.tbBaseSalary.Name = "tbBaseSalary";
            this.tbBaseSalary.Size = new System.Drawing.Size(156, 24);
            this.tbBaseSalary.TabIndex = 59;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(12, 472);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 35);
            this.button2.TabIndex = 58;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tbEditSalarySearch
            // 
            this.tbEditSalarySearch.Location = new System.Drawing.Point(69, 50);
            this.tbEditSalarySearch.Multiline = true;
            this.tbEditSalarySearch.Name = "tbEditSalarySearch";
            this.tbEditSalarySearch.Size = new System.Drawing.Size(331, 29);
            this.tbEditSalarySearch.TabIndex = 57;
            this.tbEditSalarySearch.TextChanged += new System.EventHandler(this.tbEditSalarySearch_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 16);
            this.label9.TabIndex = 56;
            this.label9.Text = "Search";
            // 
            // butEditSalary
            // 
            this.butEditSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEditSalary.Location = new System.Drawing.Point(628, 472);
            this.butEditSalary.Name = "butEditSalary";
            this.butEditSalary.Size = new System.Drawing.Size(75, 35);
            this.butEditSalary.TabIndex = 55;
            this.butEditSalary.Text = "Edit";
            this.butEditSalary.UseVisualStyleBackColor = true;
            this.butEditSalary.Click += new System.EventHandler(this.butEditSalary_Click);
            // 
            // dataGridViewEditSalary
            // 
            this.dataGridViewEditSalary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEditSalary.Location = new System.Drawing.Point(12, 93);
            this.dataGridViewEditSalary.Name = "dataGridViewEditSalary";
            this.dataGridViewEditSalary.ReadOnly = true;
            this.dataGridViewEditSalary.RowHeadersVisible = false;
            this.dataGridViewEditSalary.Size = new System.Drawing.Size(388, 355);
            this.dataGridViewEditSalary.TabIndex = 54;
            this.dataGridViewEditSalary.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewEditSalary_CellClick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(489, 287);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 16);
            this.label8.TabIndex = 53;
            this.label8.Text = "Bonus :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(410, 257);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 16);
            this.label7.TabIndex = 52;
            this.label7.Text = "Language Alloance :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(508, 317);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 16);
            this.label6.TabIndex = 51;
            this.label6.Text = "OT :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(427, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 16);
            this.label5.TabIndex = 50;
            this.label5.Text = "Housing Alloance :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(432, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 16);
            this.label4.TabIndex = 49;
            this.label4.Text = "Traval Alloance :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(413, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 16);
            this.label3.TabIndex = 48;
            this.label3.Text = "Executive Alloance :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(453, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 47;
            this.label2.Text = "Base Salary :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(489, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 46;
            this.label1.Text = "Name :";
            // 
            // labelSalaryID
            // 
            this.labelSalaryID.AutoSize = true;
            this.labelSalaryID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSalaryID.Location = new System.Drawing.Point(608, 75);
            this.labelSalaryID.Name = "labelSalaryID";
            this.labelSalaryID.Size = new System.Drawing.Size(0, 16);
            this.labelSalaryID.TabIndex = 68;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(489, 75);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 16);
            this.label12.TabIndex = 67;
            this.label12.Text = "Salary ID :";
            // 
            // guna2HScrollBar1
            // 
            this.guna2HScrollBar1.HoverState.Parent = null;
            this.guna2HScrollBar1.InUpdate = false;
            this.guna2HScrollBar1.LargeChange = 10;
            this.guna2HScrollBar1.Location = new System.Drawing.Point(12, 430);
            this.guna2HScrollBar1.Name = "guna2HScrollBar1";
            this.guna2HScrollBar1.PressedState.Parent = this.guna2HScrollBar1;
            this.guna2HScrollBar1.ScrollbarSize = 18;
            this.guna2HScrollBar1.Size = new System.Drawing.Size(300, 18);
            this.guna2HScrollBar1.TabIndex = 69;
            this.guna2HScrollBar1.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // EditSalaryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 515);
            this.Controls.Add(this.guna2HScrollBar1);
            this.Controls.Add(this.labelSalaryID);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.tbOT);
            this.Controls.Add(this.tbBonus);
            this.Controls.Add(this.tbLanguage);
            this.Controls.Add(this.tbHosing);
            this.Controls.Add(this.tbTraval);
            this.Controls.Add(this.tbExecutive);
            this.Controls.Add(this.tbBaseSalary);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tbEditSalarySearch);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.butEditSalary);
            this.Controls.Add(this.dataGridViewEditSalary);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "EditSalaryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SalaryEditForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEditSalary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox tbOT;
        private System.Windows.Forms.TextBox tbBonus;
        private System.Windows.Forms.TextBox tbLanguage;
        private System.Windows.Forms.TextBox tbHosing;
        private System.Windows.Forms.TextBox tbTraval;
        private System.Windows.Forms.TextBox tbExecutive;
        private System.Windows.Forms.TextBox tbBaseSalary;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tbEditSalarySearch;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button butEditSalary;
        private System.Windows.Forms.DataGridView dataGridViewEditSalary;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelSalaryID;
        private System.Windows.Forms.Label label12;
        private Guna.UI2.WinForms.Guna2HScrollBar guna2HScrollBar1;
    }
}