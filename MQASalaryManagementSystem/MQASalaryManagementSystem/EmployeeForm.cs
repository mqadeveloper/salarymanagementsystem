﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration;
using Guna.UI2.WinForms.Helpers;

namespace MQASalaryManagementSystem
{
   
    public partial class EmployeeForm : Form
    {
        private string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public EmployeeForm()
        {
            InitializeComponent();
           
        }

        private void butAddNewEmp_Click(object sender, EventArgs e)
        {
            AddNewEmpForm newemp = new AddNewEmpForm();
            newemp.ShowDialog();
        }

        private void butEditEmp_Click(object sender, EventArgs e)
        {
            EditEmpForm editemp = new EditEmpForm();
            editemp.ShowDialog();
        }

        private void EmployeeForm_Load(object sender, EventArgs e)
        {

            DataGridViewScrollHelper hScrollHelper = new DataGridViewScrollHelper(dataGridViewEmp, guna2HScrollBar1, true);
            DataGridViewScrollHelper vScrollHelper = new DataGridViewScrollHelper(dataGridViewEmp, guna2VScrollBar1, true);
            IDbConnection cnn = new OleDbConnection(con);
            try
            {
                cnn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            string sql_Text = "select * from Employees";
            
            DataTable tbl = new DataTable();
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, (OleDbConnection)cnn))
            {

                adapter.Fill(tbl);
               
                if (this.WindowState == FormWindowState.Maximized)
                {
                    dataGridViewEmp.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                }
               
                dataGridViewEmp.DataSource = tbl;
                dataGridViewEmp.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewEmp.Columns[6].DefaultCellStyle.Format = "yyyy/MM/dd";
                dataGridViewEmp.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewEmp.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridViewEmp.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
        }
       //--------------------------------------------- Only Active Check Box----------------
        private void checkBoxOnlyActive_CheckedChanged(object sender, EventArgs e)
        {
            

            OleDbConnection cnn = new OleDbConnection(con);
            try
            {
                cnn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            string sql_Text = "select * from Employees";
            if (checkBoxOnlyActive.Checked == true)
            {
                sql_Text += " WHERE Resign_date IS Null ";
            }

            DataTable tbl = new DataTable();
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, cnn))
            {

                adapter.Fill(tbl);
                dataGridViewEmp.DataSource = tbl;
            }
        }

        private void butEditSalary_Click(object sender, EventArgs e)
        {
            EditSalaryForm editsalary = new EditSalaryForm();
            editsalary.ShowDialog();
        }
    }
}
