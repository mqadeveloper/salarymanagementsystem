﻿
namespace MQASalaryManagementSystem
{
    partial class MonthlyHolidaysForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxY = new System.Windows.Forms.ComboBox();
            this.comboBoxM = new System.Windows.Forms.ComboBox();
            this.butShowSalary = new System.Windows.Forms.Button();
            this.tbNo1 = new System.Windows.Forms.TextBox();
            this.tbNo2 = new System.Windows.Forms.TextBox();
            this.tbNo3 = new System.Windows.Forms.TextBox();
            this.tbNo4 = new System.Windows.Forms.TextBox();
            this.tbNo5 = new System.Windows.Forms.TextBox();
            this.tbNo6 = new System.Windows.Forms.TextBox();
            this.tbNo7 = new System.Windows.Forms.TextBox();
            this.tbNo8 = new System.Windows.Forms.TextBox();
            this.tbNo9 = new System.Windows.Forms.TextBox();
            this.tbNo10 = new System.Windows.Forms.TextBox();
            this.tbNo11 = new System.Windows.Forms.TextBox();
            this.tbNo12 = new System.Windows.Forms.TextBox();
            this.tbNo13 = new System.Windows.Forms.TextBox();
            this.tbNo14 = new System.Windows.Forms.TextBox();
            this.tbNo15 = new System.Windows.Forms.TextBox();
            this.tbNo16 = new System.Windows.Forms.TextBox();
            this.tbNo17 = new System.Windows.Forms.TextBox();
            this.tbNo18 = new System.Windows.Forms.TextBox();
            this.tbNo19 = new System.Windows.Forms.TextBox();
            this.tbNo20 = new System.Windows.Forms.TextBox();
            this.tbNo21 = new System.Windows.Forms.TextBox();
            this.tbNo22 = new System.Windows.Forms.TextBox();
            this.tbNo27 = new System.Windows.Forms.TextBox();
            this.tbNo26 = new System.Windows.Forms.TextBox();
            this.tbNo25 = new System.Windows.Forms.TextBox();
            this.tbNo24 = new System.Windows.Forms.TextBox();
            this.tbNo23 = new System.Windows.Forms.TextBox();
            this.tbday1 = new System.Windows.Forms.TextBox();
            this.tbday2 = new System.Windows.Forms.TextBox();
            this.tbday3 = new System.Windows.Forms.TextBox();
            this.tbday4 = new System.Windows.Forms.TextBox();
            this.tbday5 = new System.Windows.Forms.TextBox();
            this.tbday6 = new System.Windows.Forms.TextBox();
            this.tbday7 = new System.Windows.Forms.TextBox();
            this.tbday8 = new System.Windows.Forms.TextBox();
            this.tbday9 = new System.Windows.Forms.TextBox();
            this.tbday10 = new System.Windows.Forms.TextBox();
            this.tbday11 = new System.Windows.Forms.TextBox();
            this.tbday12 = new System.Windows.Forms.TextBox();
            this.tbday13 = new System.Windows.Forms.TextBox();
            this.tbday14 = new System.Windows.Forms.TextBox();
            this.tbday15 = new System.Windows.Forms.TextBox();
            this.tbday16 = new System.Windows.Forms.TextBox();
            this.tbday17 = new System.Windows.Forms.TextBox();
            this.tbday18 = new System.Windows.Forms.TextBox();
            this.tbday19 = new System.Windows.Forms.TextBox();
            this.tbday20 = new System.Windows.Forms.TextBox();
            this.tbday21 = new System.Windows.Forms.TextBox();
            this.tbday22 = new System.Windows.Forms.TextBox();
            this.tbday23 = new System.Windows.Forms.TextBox();
            this.tbday24 = new System.Windows.Forms.TextBox();
            this.tbday25 = new System.Windows.Forms.TextBox();
            this.tbday26 = new System.Windows.Forms.TextBox();
            this.tbday27 = new System.Windows.Forms.TextBox();
            this.butupdate = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.panelday31 = new System.Windows.Forms.Panel();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.tbday31 = new System.Windows.Forms.TextBox();
            this.tbNo31 = new System.Windows.Forms.TextBox();
            this.panelday30 = new System.Windows.Forms.Panel();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.tbday30 = new System.Windows.Forms.TextBox();
            this.tbNo30 = new System.Windows.Forms.TextBox();
            this.tbNo28 = new System.Windows.Forms.TextBox();
            this.tbday28 = new System.Windows.Forms.TextBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.panelday29 = new System.Windows.Forms.Panel();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.tbday29 = new System.Windows.Forms.TextBox();
            this.tbNo29 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panelday31.SuspendLayout();
            this.panelday30.SuspendLayout();
            this.panelday29.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MistyRose;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(818, 31);
            this.panel1.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(671, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Monthly Holidays";
            // 
            // comboBoxY
            // 
            this.comboBoxY.FormattingEnabled = true;
            this.comboBoxY.Location = new System.Drawing.Point(139, 37);
            this.comboBoxY.Name = "comboBoxY";
            this.comboBoxY.Size = new System.Drawing.Size(121, 21);
            this.comboBoxY.TabIndex = 14;
            // 
            // comboBoxM
            // 
            this.comboBoxM.FormattingEnabled = true;
            this.comboBoxM.Location = new System.Drawing.Point(12, 37);
            this.comboBoxM.Name = "comboBoxM";
            this.comboBoxM.Size = new System.Drawing.Size(121, 21);
            this.comboBoxM.TabIndex = 13;
            // 
            // butShowSalary
            // 
            this.butShowSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butShowSalary.Location = new System.Drawing.Point(275, 37);
            this.butShowSalary.Name = "butShowSalary";
            this.butShowSalary.Size = new System.Drawing.Size(153, 59);
            this.butShowSalary.TabIndex = 12;
            this.butShowSalary.Text = "Show";
            this.butShowSalary.UseVisualStyleBackColor = true;
            this.butShowSalary.Click += new System.EventHandler(this.butShowSalary_Click);
            // 
            // tbNo1
            // 
            this.tbNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo1.Location = new System.Drawing.Point(36, 122);
            this.tbNo1.Multiline = true;
            this.tbNo1.Name = "tbNo1";
            this.tbNo1.Size = new System.Drawing.Size(39, 32);
            this.tbNo1.TabIndex = 15;
            this.tbNo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo2
            // 
            this.tbNo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo2.Location = new System.Drawing.Point(36, 160);
            this.tbNo2.Multiline = true;
            this.tbNo2.Name = "tbNo2";
            this.tbNo2.Size = new System.Drawing.Size(39, 32);
            this.tbNo2.TabIndex = 16;
            this.tbNo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo3
            // 
            this.tbNo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo3.Location = new System.Drawing.Point(36, 198);
            this.tbNo3.Multiline = true;
            this.tbNo3.Name = "tbNo3";
            this.tbNo3.Size = new System.Drawing.Size(39, 32);
            this.tbNo3.TabIndex = 17;
            this.tbNo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo4
            // 
            this.tbNo4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo4.Location = new System.Drawing.Point(36, 236);
            this.tbNo4.Multiline = true;
            this.tbNo4.Name = "tbNo4";
            this.tbNo4.Size = new System.Drawing.Size(39, 32);
            this.tbNo4.TabIndex = 18;
            this.tbNo4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo5
            // 
            this.tbNo5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo5.Location = new System.Drawing.Point(36, 274);
            this.tbNo5.Multiline = true;
            this.tbNo5.Name = "tbNo5";
            this.tbNo5.Size = new System.Drawing.Size(39, 32);
            this.tbNo5.TabIndex = 19;
            this.tbNo5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo6
            // 
            this.tbNo6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo6.Location = new System.Drawing.Point(36, 312);
            this.tbNo6.Multiline = true;
            this.tbNo6.Name = "tbNo6";
            this.tbNo6.Size = new System.Drawing.Size(39, 32);
            this.tbNo6.TabIndex = 20;
            this.tbNo6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo7
            // 
            this.tbNo7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo7.Location = new System.Drawing.Point(36, 350);
            this.tbNo7.Multiline = true;
            this.tbNo7.Name = "tbNo7";
            this.tbNo7.Size = new System.Drawing.Size(39, 32);
            this.tbNo7.TabIndex = 21;
            this.tbNo7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo8
            // 
            this.tbNo8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo8.Location = new System.Drawing.Point(36, 388);
            this.tbNo8.Multiline = true;
            this.tbNo8.Name = "tbNo8";
            this.tbNo8.Size = new System.Drawing.Size(39, 32);
            this.tbNo8.TabIndex = 22;
            this.tbNo8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo9
            // 
            this.tbNo9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo9.Location = new System.Drawing.Point(36, 426);
            this.tbNo9.Multiline = true;
            this.tbNo9.Name = "tbNo9";
            this.tbNo9.Size = new System.Drawing.Size(39, 32);
            this.tbNo9.TabIndex = 23;
            this.tbNo9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo10
            // 
            this.tbNo10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo10.Location = new System.Drawing.Point(36, 464);
            this.tbNo10.Multiline = true;
            this.tbNo10.Name = "tbNo10";
            this.tbNo10.Size = new System.Drawing.Size(39, 32);
            this.tbNo10.TabIndex = 24;
            this.tbNo10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo11
            // 
            this.tbNo11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo11.Location = new System.Drawing.Point(242, 122);
            this.tbNo11.Multiline = true;
            this.tbNo11.Name = "tbNo11";
            this.tbNo11.Size = new System.Drawing.Size(39, 32);
            this.tbNo11.TabIndex = 25;
            this.tbNo11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo12
            // 
            this.tbNo12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo12.Location = new System.Drawing.Point(242, 160);
            this.tbNo12.Multiline = true;
            this.tbNo12.Name = "tbNo12";
            this.tbNo12.Size = new System.Drawing.Size(39, 32);
            this.tbNo12.TabIndex = 26;
            this.tbNo12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo13
            // 
            this.tbNo13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo13.Location = new System.Drawing.Point(242, 198);
            this.tbNo13.Multiline = true;
            this.tbNo13.Name = "tbNo13";
            this.tbNo13.Size = new System.Drawing.Size(39, 32);
            this.tbNo13.TabIndex = 27;
            this.tbNo13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo14
            // 
            this.tbNo14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo14.Location = new System.Drawing.Point(242, 236);
            this.tbNo14.Multiline = true;
            this.tbNo14.Name = "tbNo14";
            this.tbNo14.Size = new System.Drawing.Size(39, 32);
            this.tbNo14.TabIndex = 28;
            this.tbNo14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo15
            // 
            this.tbNo15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo15.Location = new System.Drawing.Point(242, 274);
            this.tbNo15.Multiline = true;
            this.tbNo15.Name = "tbNo15";
            this.tbNo15.Size = new System.Drawing.Size(39, 32);
            this.tbNo15.TabIndex = 29;
            this.tbNo15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo16
            // 
            this.tbNo16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo16.Location = new System.Drawing.Point(242, 312);
            this.tbNo16.Multiline = true;
            this.tbNo16.Name = "tbNo16";
            this.tbNo16.Size = new System.Drawing.Size(39, 32);
            this.tbNo16.TabIndex = 30;
            this.tbNo16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo17
            // 
            this.tbNo17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo17.Location = new System.Drawing.Point(242, 350);
            this.tbNo17.Multiline = true;
            this.tbNo17.Name = "tbNo17";
            this.tbNo17.Size = new System.Drawing.Size(39, 32);
            this.tbNo17.TabIndex = 31;
            this.tbNo17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo18
            // 
            this.tbNo18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo18.Location = new System.Drawing.Point(242, 388);
            this.tbNo18.Multiline = true;
            this.tbNo18.Name = "tbNo18";
            this.tbNo18.Size = new System.Drawing.Size(39, 32);
            this.tbNo18.TabIndex = 32;
            this.tbNo18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo19
            // 
            this.tbNo19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo19.Location = new System.Drawing.Point(242, 426);
            this.tbNo19.Multiline = true;
            this.tbNo19.Name = "tbNo19";
            this.tbNo19.Size = new System.Drawing.Size(39, 32);
            this.tbNo19.TabIndex = 33;
            this.tbNo19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo20
            // 
            this.tbNo20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo20.Location = new System.Drawing.Point(242, 464);
            this.tbNo20.Multiline = true;
            this.tbNo20.Name = "tbNo20";
            this.tbNo20.Size = new System.Drawing.Size(39, 32);
            this.tbNo20.TabIndex = 34;
            this.tbNo20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo21
            // 
            this.tbNo21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo21.Location = new System.Drawing.Point(444, 122);
            this.tbNo21.Multiline = true;
            this.tbNo21.Name = "tbNo21";
            this.tbNo21.Size = new System.Drawing.Size(39, 32);
            this.tbNo21.TabIndex = 35;
            this.tbNo21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo22
            // 
            this.tbNo22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo22.Location = new System.Drawing.Point(444, 160);
            this.tbNo22.Multiline = true;
            this.tbNo22.Name = "tbNo22";
            this.tbNo22.Size = new System.Drawing.Size(39, 32);
            this.tbNo22.TabIndex = 36;
            this.tbNo22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo27
            // 
            this.tbNo27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo27.Location = new System.Drawing.Point(444, 350);
            this.tbNo27.Multiline = true;
            this.tbNo27.Name = "tbNo27";
            this.tbNo27.Size = new System.Drawing.Size(39, 32);
            this.tbNo27.TabIndex = 41;
            this.tbNo27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo26
            // 
            this.tbNo26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo26.Location = new System.Drawing.Point(444, 312);
            this.tbNo26.Multiline = true;
            this.tbNo26.Name = "tbNo26";
            this.tbNo26.Size = new System.Drawing.Size(39, 32);
            this.tbNo26.TabIndex = 40;
            this.tbNo26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo25
            // 
            this.tbNo25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo25.Location = new System.Drawing.Point(444, 274);
            this.tbNo25.Multiline = true;
            this.tbNo25.Name = "tbNo25";
            this.tbNo25.Size = new System.Drawing.Size(39, 32);
            this.tbNo25.TabIndex = 39;
            this.tbNo25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo24
            // 
            this.tbNo24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo24.Location = new System.Drawing.Point(444, 236);
            this.tbNo24.Multiline = true;
            this.tbNo24.Name = "tbNo24";
            this.tbNo24.Size = new System.Drawing.Size(39, 32);
            this.tbNo24.TabIndex = 38;
            this.tbNo24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo23
            // 
            this.tbNo23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo23.Location = new System.Drawing.Point(444, 198);
            this.tbNo23.Multiline = true;
            this.tbNo23.Name = "tbNo23";
            this.tbNo23.Size = new System.Drawing.Size(39, 32);
            this.tbNo23.TabIndex = 37;
            this.tbNo23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday1
            // 
            this.tbday1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday1.Location = new System.Drawing.Point(81, 122);
            this.tbday1.Multiline = true;
            this.tbday1.Name = "tbday1";
            this.tbday1.Size = new System.Drawing.Size(100, 32);
            this.tbday1.TabIndex = 46;
            this.tbday1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday2
            // 
            this.tbday2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday2.Location = new System.Drawing.Point(81, 160);
            this.tbday2.Multiline = true;
            this.tbday2.Name = "tbday2";
            this.tbday2.Size = new System.Drawing.Size(100, 32);
            this.tbday2.TabIndex = 47;
            this.tbday2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday3
            // 
            this.tbday3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday3.Location = new System.Drawing.Point(81, 198);
            this.tbday3.Multiline = true;
            this.tbday3.Name = "tbday3";
            this.tbday3.Size = new System.Drawing.Size(100, 32);
            this.tbday3.TabIndex = 48;
            this.tbday3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday4
            // 
            this.tbday4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday4.Location = new System.Drawing.Point(81, 236);
            this.tbday4.Multiline = true;
            this.tbday4.Name = "tbday4";
            this.tbday4.Size = new System.Drawing.Size(100, 32);
            this.tbday4.TabIndex = 49;
            this.tbday4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday5
            // 
            this.tbday5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday5.Location = new System.Drawing.Point(81, 274);
            this.tbday5.Multiline = true;
            this.tbday5.Name = "tbday5";
            this.tbday5.Size = new System.Drawing.Size(100, 32);
            this.tbday5.TabIndex = 50;
            this.tbday5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday6
            // 
            this.tbday6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday6.Location = new System.Drawing.Point(81, 312);
            this.tbday6.Multiline = true;
            this.tbday6.Name = "tbday6";
            this.tbday6.Size = new System.Drawing.Size(100, 32);
            this.tbday6.TabIndex = 51;
            this.tbday6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday7
            // 
            this.tbday7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday7.Location = new System.Drawing.Point(81, 350);
            this.tbday7.Multiline = true;
            this.tbday7.Name = "tbday7";
            this.tbday7.Size = new System.Drawing.Size(100, 32);
            this.tbday7.TabIndex = 52;
            this.tbday7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday8
            // 
            this.tbday8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday8.Location = new System.Drawing.Point(81, 388);
            this.tbday8.Multiline = true;
            this.tbday8.Name = "tbday8";
            this.tbday8.Size = new System.Drawing.Size(100, 32);
            this.tbday8.TabIndex = 53;
            this.tbday8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday9
            // 
            this.tbday9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday9.Location = new System.Drawing.Point(81, 426);
            this.tbday9.Multiline = true;
            this.tbday9.Name = "tbday9";
            this.tbday9.Size = new System.Drawing.Size(100, 32);
            this.tbday9.TabIndex = 54;
            this.tbday9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday10
            // 
            this.tbday10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday10.Location = new System.Drawing.Point(81, 464);
            this.tbday10.Multiline = true;
            this.tbday10.Name = "tbday10";
            this.tbday10.Size = new System.Drawing.Size(100, 32);
            this.tbday10.TabIndex = 55;
            this.tbday10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday11
            // 
            this.tbday11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday11.Location = new System.Drawing.Point(287, 122);
            this.tbday11.Multiline = true;
            this.tbday11.Name = "tbday11";
            this.tbday11.Size = new System.Drawing.Size(100, 32);
            this.tbday11.TabIndex = 56;
            this.tbday11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday12
            // 
            this.tbday12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday12.Location = new System.Drawing.Point(287, 160);
            this.tbday12.Multiline = true;
            this.tbday12.Name = "tbday12";
            this.tbday12.Size = new System.Drawing.Size(100, 32);
            this.tbday12.TabIndex = 57;
            this.tbday12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday13
            // 
            this.tbday13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday13.Location = new System.Drawing.Point(287, 198);
            this.tbday13.Multiline = true;
            this.tbday13.Name = "tbday13";
            this.tbday13.Size = new System.Drawing.Size(100, 32);
            this.tbday13.TabIndex = 58;
            this.tbday13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday14
            // 
            this.tbday14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday14.Location = new System.Drawing.Point(287, 236);
            this.tbday14.Multiline = true;
            this.tbday14.Name = "tbday14";
            this.tbday14.Size = new System.Drawing.Size(100, 32);
            this.tbday14.TabIndex = 59;
            this.tbday14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday15
            // 
            this.tbday15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday15.Location = new System.Drawing.Point(287, 274);
            this.tbday15.Multiline = true;
            this.tbday15.Name = "tbday15";
            this.tbday15.Size = new System.Drawing.Size(100, 32);
            this.tbday15.TabIndex = 60;
            this.tbday15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday16
            // 
            this.tbday16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday16.Location = new System.Drawing.Point(287, 312);
            this.tbday16.Multiline = true;
            this.tbday16.Name = "tbday16";
            this.tbday16.Size = new System.Drawing.Size(100, 32);
            this.tbday16.TabIndex = 61;
            this.tbday16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday17
            // 
            this.tbday17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday17.Location = new System.Drawing.Point(287, 350);
            this.tbday17.Multiline = true;
            this.tbday17.Name = "tbday17";
            this.tbday17.Size = new System.Drawing.Size(100, 32);
            this.tbday17.TabIndex = 62;
            this.tbday17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday18
            // 
            this.tbday18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday18.Location = new System.Drawing.Point(287, 388);
            this.tbday18.Multiline = true;
            this.tbday18.Name = "tbday18";
            this.tbday18.Size = new System.Drawing.Size(100, 32);
            this.tbday18.TabIndex = 63;
            this.tbday18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday19
            // 
            this.tbday19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday19.Location = new System.Drawing.Point(287, 426);
            this.tbday19.Multiline = true;
            this.tbday19.Name = "tbday19";
            this.tbday19.Size = new System.Drawing.Size(100, 32);
            this.tbday19.TabIndex = 64;
            this.tbday19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday20
            // 
            this.tbday20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday20.Location = new System.Drawing.Point(287, 464);
            this.tbday20.Multiline = true;
            this.tbday20.Name = "tbday20";
            this.tbday20.Size = new System.Drawing.Size(100, 32);
            this.tbday20.TabIndex = 65;
            this.tbday20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday21
            // 
            this.tbday21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday21.Location = new System.Drawing.Point(489, 122);
            this.tbday21.Multiline = true;
            this.tbday21.Name = "tbday21";
            this.tbday21.Size = new System.Drawing.Size(100, 32);
            this.tbday21.TabIndex = 66;
            this.tbday21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday22
            // 
            this.tbday22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday22.Location = new System.Drawing.Point(489, 160);
            this.tbday22.Multiline = true;
            this.tbday22.Name = "tbday22";
            this.tbday22.Size = new System.Drawing.Size(100, 32);
            this.tbday22.TabIndex = 67;
            this.tbday22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday23
            // 
            this.tbday23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday23.Location = new System.Drawing.Point(489, 198);
            this.tbday23.Multiline = true;
            this.tbday23.Name = "tbday23";
            this.tbday23.Size = new System.Drawing.Size(100, 32);
            this.tbday23.TabIndex = 68;
            this.tbday23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday24
            // 
            this.tbday24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday24.Location = new System.Drawing.Point(489, 236);
            this.tbday24.Multiline = true;
            this.tbday24.Name = "tbday24";
            this.tbday24.Size = new System.Drawing.Size(100, 32);
            this.tbday24.TabIndex = 69;
            this.tbday24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday25
            // 
            this.tbday25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday25.Location = new System.Drawing.Point(489, 274);
            this.tbday25.Multiline = true;
            this.tbday25.Name = "tbday25";
            this.tbday25.Size = new System.Drawing.Size(100, 32);
            this.tbday25.TabIndex = 70;
            this.tbday25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday26
            // 
            this.tbday26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday26.Location = new System.Drawing.Point(489, 312);
            this.tbday26.Multiline = true;
            this.tbday26.Name = "tbday26";
            this.tbday26.Size = new System.Drawing.Size(100, 32);
            this.tbday26.TabIndex = 71;
            this.tbday26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday27
            // 
            this.tbday27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday27.Location = new System.Drawing.Point(489, 350);
            this.tbday27.Multiline = true;
            this.tbday27.Name = "tbday27";
            this.tbday27.Size = new System.Drawing.Size(100, 32);
            this.tbday27.TabIndex = 72;
            this.tbday27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // butupdate
            // 
            this.butupdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butupdate.Location = new System.Drawing.Point(653, 477);
            this.butupdate.Name = "butupdate";
            this.butupdate.Size = new System.Drawing.Size(153, 59);
            this.butupdate.TabIndex = 77;
            this.butupdate.Text = "Update";
            this.butupdate.UseVisualStyleBackColor = true;
            this.butupdate.Click += new System.EventHandler(this.butupdate_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(188, 136);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 78;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(188, 178);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 79;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(188, 216);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 80;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(188, 254);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 81;
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(188, 292);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 82;
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(188, 330);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 83;
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(188, 368);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 84;
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(188, 406);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 85;
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(188, 444);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 86;
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(188, 482);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 87;
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(393, 140);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 88;
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(393, 178);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 89;
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(393, 216);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(15, 14);
            this.checkBox13.TabIndex = 90;
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(393, 254);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(15, 14);
            this.checkBox14.TabIndex = 91;
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(393, 292);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(15, 14);
            this.checkBox15.TabIndex = 92;
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(393, 330);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(15, 14);
            this.checkBox16.TabIndex = 93;
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(393, 368);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(15, 14);
            this.checkBox17.TabIndex = 94;
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(393, 406);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(15, 14);
            this.checkBox18.TabIndex = 95;
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Location = new System.Drawing.Point(393, 444);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(15, 14);
            this.checkBox19.TabIndex = 96;
            this.checkBox19.UseVisualStyleBackColor = true;
            this.checkBox19.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(393, 482);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(15, 14);
            this.checkBox20.TabIndex = 97;
            this.checkBox20.UseVisualStyleBackColor = true;
            this.checkBox20.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Location = new System.Drawing.Point(595, 140);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(15, 14);
            this.checkBox21.TabIndex = 98;
            this.checkBox21.UseVisualStyleBackColor = true;
            this.checkBox21.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Location = new System.Drawing.Point(595, 178);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(15, 14);
            this.checkBox22.TabIndex = 99;
            this.checkBox22.UseVisualStyleBackColor = true;
            this.checkBox22.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Location = new System.Drawing.Point(595, 216);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(15, 14);
            this.checkBox23.TabIndex = 100;
            this.checkBox23.UseVisualStyleBackColor = true;
            this.checkBox23.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Location = new System.Drawing.Point(595, 254);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(15, 14);
            this.checkBox24.TabIndex = 101;
            this.checkBox24.UseVisualStyleBackColor = true;
            this.checkBox24.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Location = new System.Drawing.Point(595, 292);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(15, 14);
            this.checkBox25.TabIndex = 102;
            this.checkBox25.UseVisualStyleBackColor = true;
            this.checkBox25.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.Location = new System.Drawing.Point(595, 330);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(15, 14);
            this.checkBox26.TabIndex = 103;
            this.checkBox26.UseVisualStyleBackColor = true;
            this.checkBox26.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Location = new System.Drawing.Point(595, 368);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(15, 14);
            this.checkBox27.TabIndex = 104;
            this.checkBox27.UseVisualStyleBackColor = true;
            this.checkBox27.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // panelday31
            // 
            this.panelday31.Controls.Add(this.checkBox31);
            this.panelday31.Controls.Add(this.tbday31);
            this.panelday31.Controls.Add(this.tbNo31);
            this.panelday31.Location = new System.Drawing.Point(631, 104);
            this.panelday31.Name = "panelday31";
            this.panelday31.Size = new System.Drawing.Size(175, 64);
            this.panelday31.TabIndex = 108;
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.Location = new System.Drawing.Point(152, 32);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(15, 14);
            this.checkBox31.TabIndex = 111;
            this.checkBox31.UseVisualStyleBackColor = true;
            this.checkBox31.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // tbday31
            // 
            this.tbday31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday31.Location = new System.Drawing.Point(46, 16);
            this.tbday31.Multiline = true;
            this.tbday31.Name = "tbday31";
            this.tbday31.Size = new System.Drawing.Size(100, 32);
            this.tbday31.TabIndex = 110;
            this.tbday31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo31
            // 
            this.tbNo31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo31.Location = new System.Drawing.Point(1, 16);
            this.tbNo31.Multiline = true;
            this.tbNo31.Name = "tbNo31";
            this.tbNo31.Size = new System.Drawing.Size(39, 32);
            this.tbNo31.TabIndex = 109;
            this.tbNo31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panelday30
            // 
            this.panelday30.Controls.Add(this.checkBox30);
            this.panelday30.Controls.Add(this.tbday30);
            this.panelday30.Controls.Add(this.tbNo30);
            this.panelday30.Location = new System.Drawing.Point(444, 464);
            this.panelday30.Name = "panelday30";
            this.panelday30.Size = new System.Drawing.Size(174, 42);
            this.panelday30.TabIndex = 109;
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.Location = new System.Drawing.Point(151, 18);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(15, 14);
            this.checkBox30.TabIndex = 110;
            this.checkBox30.UseVisualStyleBackColor = true;
            this.checkBox30.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // tbday30
            // 
            this.tbday30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday30.Location = new System.Drawing.Point(45, 0);
            this.tbday30.Multiline = true;
            this.tbday30.Name = "tbday30";
            this.tbday30.Size = new System.Drawing.Size(100, 32);
            this.tbday30.TabIndex = 109;
            this.tbday30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo30
            // 
            this.tbNo30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo30.Location = new System.Drawing.Point(0, 0);
            this.tbNo30.Multiline = true;
            this.tbNo30.Name = "tbNo30";
            this.tbNo30.Size = new System.Drawing.Size(39, 32);
            this.tbNo30.TabIndex = 108;
            this.tbNo30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo28
            // 
            this.tbNo28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo28.Location = new System.Drawing.Point(444, 388);
            this.tbNo28.Multiline = true;
            this.tbNo28.Name = "tbNo28";
            this.tbNo28.Size = new System.Drawing.Size(39, 32);
            this.tbNo28.TabIndex = 42;
            this.tbNo28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbday28
            // 
            this.tbday28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday28.Location = new System.Drawing.Point(489, 388);
            this.tbday28.Multiline = true;
            this.tbday28.Name = "tbday28";
            this.tbday28.Size = new System.Drawing.Size(100, 32);
            this.tbday28.TabIndex = 73;
            this.tbday28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Location = new System.Drawing.Point(595, 406);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(15, 14);
            this.checkBox28.TabIndex = 105;
            this.checkBox28.UseVisualStyleBackColor = true;
            this.checkBox28.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // panelday29
            // 
            this.panelday29.Controls.Add(this.checkBox29);
            this.panelday29.Controls.Add(this.tbday29);
            this.panelday29.Controls.Add(this.tbNo29);
            this.panelday29.Location = new System.Drawing.Point(445, 426);
            this.panelday29.Name = "panelday29";
            this.panelday29.Size = new System.Drawing.Size(173, 38);
            this.panelday29.TabIndex = 110;
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Location = new System.Drawing.Point(151, 18);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(15, 14);
            this.checkBox29.TabIndex = 109;
            this.checkBox29.UseVisualStyleBackColor = true;
            this.checkBox29.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            // 
            // tbday29
            // 
            this.tbday29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbday29.Location = new System.Drawing.Point(45, 0);
            this.tbday29.Multiline = true;
            this.tbday29.Name = "tbday29";
            this.tbday29.Size = new System.Drawing.Size(100, 32);
            this.tbday29.TabIndex = 108;
            this.tbday29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbNo29
            // 
            this.tbNo29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNo29.Location = new System.Drawing.Point(0, 0);
            this.tbNo29.Multiline = true;
            this.tbNo29.Name = "tbNo29";
            this.tbNo29.Size = new System.Drawing.Size(39, 32);
            this.tbNo29.TabIndex = 107;
            this.tbNo29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MonthlyHolidaysForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 548);
            this.ControlBox = false;
            this.Controls.Add(this.panelday29);
            this.Controls.Add(this.panelday30);
            this.Controls.Add(this.panelday31);
            this.Controls.Add(this.checkBox28);
            this.Controls.Add(this.checkBox27);
            this.Controls.Add(this.checkBox26);
            this.Controls.Add(this.checkBox25);
            this.Controls.Add(this.checkBox24);
            this.Controls.Add(this.checkBox23);
            this.Controls.Add(this.checkBox22);
            this.Controls.Add(this.checkBox21);
            this.Controls.Add(this.checkBox20);
            this.Controls.Add(this.checkBox19);
            this.Controls.Add(this.checkBox18);
            this.Controls.Add(this.checkBox17);
            this.Controls.Add(this.checkBox16);
            this.Controls.Add(this.checkBox15);
            this.Controls.Add(this.checkBox14);
            this.Controls.Add(this.checkBox13);
            this.Controls.Add(this.checkBox12);
            this.Controls.Add(this.checkBox11);
            this.Controls.Add(this.checkBox10);
            this.Controls.Add(this.checkBox9);
            this.Controls.Add(this.checkBox8);
            this.Controls.Add(this.checkBox7);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.butupdate);
            this.Controls.Add(this.tbday28);
            this.Controls.Add(this.tbday27);
            this.Controls.Add(this.tbday26);
            this.Controls.Add(this.tbday25);
            this.Controls.Add(this.tbday24);
            this.Controls.Add(this.tbday23);
            this.Controls.Add(this.tbday22);
            this.Controls.Add(this.tbday21);
            this.Controls.Add(this.tbday20);
            this.Controls.Add(this.tbday19);
            this.Controls.Add(this.tbday18);
            this.Controls.Add(this.tbday17);
            this.Controls.Add(this.tbday16);
            this.Controls.Add(this.tbday15);
            this.Controls.Add(this.tbday14);
            this.Controls.Add(this.tbday13);
            this.Controls.Add(this.tbday12);
            this.Controls.Add(this.tbday11);
            this.Controls.Add(this.tbday10);
            this.Controls.Add(this.tbday9);
            this.Controls.Add(this.tbday8);
            this.Controls.Add(this.tbday7);
            this.Controls.Add(this.tbday6);
            this.Controls.Add(this.tbday5);
            this.Controls.Add(this.tbday4);
            this.Controls.Add(this.tbday3);
            this.Controls.Add(this.tbday2);
            this.Controls.Add(this.tbday1);
            this.Controls.Add(this.tbNo28);
            this.Controls.Add(this.tbNo27);
            this.Controls.Add(this.tbNo26);
            this.Controls.Add(this.tbNo25);
            this.Controls.Add(this.tbNo24);
            this.Controls.Add(this.tbNo23);
            this.Controls.Add(this.tbNo22);
            this.Controls.Add(this.tbNo21);
            this.Controls.Add(this.tbNo20);
            this.Controls.Add(this.tbNo19);
            this.Controls.Add(this.tbNo18);
            this.Controls.Add(this.tbNo17);
            this.Controls.Add(this.tbNo16);
            this.Controls.Add(this.tbNo15);
            this.Controls.Add(this.tbNo14);
            this.Controls.Add(this.tbNo13);
            this.Controls.Add(this.tbNo12);
            this.Controls.Add(this.tbNo11);
            this.Controls.Add(this.tbNo10);
            this.Controls.Add(this.tbNo9);
            this.Controls.Add(this.tbNo8);
            this.Controls.Add(this.tbNo7);
            this.Controls.Add(this.tbNo6);
            this.Controls.Add(this.tbNo5);
            this.Controls.Add(this.tbNo4);
            this.Controls.Add(this.tbNo3);
            this.Controls.Add(this.tbNo2);
            this.Controls.Add(this.tbNo1);
            this.Controls.Add(this.comboBoxY);
            this.Controls.Add(this.comboBoxM);
            this.Controls.Add(this.butShowSalary);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MonthlyHolidaysForm";
            this.Click += new System.EventHandler(this.MonthlyHolidaysForm_Click);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelday31.ResumeLayout(false);
            this.panelday31.PerformLayout();
            this.panelday30.ResumeLayout(false);
            this.panelday30.PerformLayout();
            this.panelday29.ResumeLayout(false);
            this.panelday29.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxY;
        private System.Windows.Forms.ComboBox comboBoxM;
        private System.Windows.Forms.Button butShowSalary;
        private System.Windows.Forms.TextBox tbNo1;
        private System.Windows.Forms.TextBox tbNo2;
        private System.Windows.Forms.TextBox tbNo3;
        private System.Windows.Forms.TextBox tbNo4;
        private System.Windows.Forms.TextBox tbNo5;
        private System.Windows.Forms.TextBox tbNo6;
        private System.Windows.Forms.TextBox tbNo7;
        private System.Windows.Forms.TextBox tbNo8;
        private System.Windows.Forms.TextBox tbNo9;
        private System.Windows.Forms.TextBox tbNo10;
        private System.Windows.Forms.TextBox tbNo11;
        private System.Windows.Forms.TextBox tbNo12;
        private System.Windows.Forms.TextBox tbNo13;
        private System.Windows.Forms.TextBox tbNo14;
        private System.Windows.Forms.TextBox tbNo15;
        private System.Windows.Forms.TextBox tbNo16;
        private System.Windows.Forms.TextBox tbNo17;
        private System.Windows.Forms.TextBox tbNo18;
        private System.Windows.Forms.TextBox tbNo19;
        private System.Windows.Forms.TextBox tbNo20;
        private System.Windows.Forms.TextBox tbNo21;
        private System.Windows.Forms.TextBox tbNo22;
        private System.Windows.Forms.TextBox tbNo27;
        private System.Windows.Forms.TextBox tbNo26;
        private System.Windows.Forms.TextBox tbNo25;
        private System.Windows.Forms.TextBox tbNo24;
        private System.Windows.Forms.TextBox tbNo23;
        private System.Windows.Forms.TextBox tbday1;
        private System.Windows.Forms.TextBox tbday2;
        private System.Windows.Forms.TextBox tbday3;
        private System.Windows.Forms.TextBox tbday4;
        private System.Windows.Forms.TextBox tbday5;
        private System.Windows.Forms.TextBox tbday6;
        private System.Windows.Forms.TextBox tbday7;
        private System.Windows.Forms.TextBox tbday8;
        private System.Windows.Forms.TextBox tbday9;
        private System.Windows.Forms.TextBox tbday10;
        private System.Windows.Forms.TextBox tbday11;
        private System.Windows.Forms.TextBox tbday12;
        private System.Windows.Forms.TextBox tbday13;
        private System.Windows.Forms.TextBox tbday14;
        private System.Windows.Forms.TextBox tbday15;
        private System.Windows.Forms.TextBox tbday16;
        private System.Windows.Forms.TextBox tbday17;
        private System.Windows.Forms.TextBox tbday18;
        private System.Windows.Forms.TextBox tbday19;
        private System.Windows.Forms.TextBox tbday20;
        private System.Windows.Forms.TextBox tbday21;
        private System.Windows.Forms.TextBox tbday22;
        private System.Windows.Forms.TextBox tbday23;
        private System.Windows.Forms.TextBox tbday24;
        private System.Windows.Forms.TextBox tbday25;
        private System.Windows.Forms.TextBox tbday26;
        private System.Windows.Forms.TextBox tbday27;
        private System.Windows.Forms.Button butupdate;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.Panel panelday31;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.TextBox tbday31;
        private System.Windows.Forms.TextBox tbNo31;
        private System.Windows.Forms.Panel panelday30;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.TextBox tbday30;
        private System.Windows.Forms.TextBox tbNo30;
        private System.Windows.Forms.TextBox tbNo28;
        private System.Windows.Forms.TextBox tbday28;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.Panel panelday29;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.TextBox tbday29;
        private System.Windows.Forms.TextBox tbNo29;
    }
}