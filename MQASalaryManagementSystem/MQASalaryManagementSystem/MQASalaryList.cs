﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace MQASalaryManagementSystem
{
    class MQASalaryList
    {
       

        public static void SalaryList()
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            DateTime time = DateTime.Now;
            string timef = time.ToString("yyyyMM");
            Application excApp;
            Workbook excBook;
            Worksheet excSheet;
            Range rg;
            excApp = new Application();
            excApp.Visible = false;
            excApp.UserControl = false;
            excBook = (Workbook)(excApp.Workbooks.Add(Missing.Value));
            string tr = DateTime.Today.ToString("MM");
            int rt = int.Parse(tr);
            for (int o = 1; o <= rt; o++)
            {


                using (IDbConnection cnn = new OleDbConnection(con))

                {
                    string SQL_Text = "SELECT Employees.EMP_ID,Employees.EMP_FristName,Employees.EMP_LastName, Employees.EMP_NickName, Net_Salary.* ";
                    SQL_Text += "FROM Net_Salary INNER JOIN Employees ON Employees.EMP_ID = Net_Salary.Net_Salary_ID ";
                    SQL_Text += "WHERE MONTH(Pay_Date) = " + o;
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
                    adapter.Fill(tbl);
                    

                    int i = 3;

                     excSheet = (Worksheet)excBook.Worksheets.Add();
                    
                   
                    foreach (DataRow dr in tbl.Rows)
                    {
                        int empid = int.Parse(dr[0].ToString());
                        string Fname = dr[1].ToString();
                        string Lname = dr[2].ToString();
                        string Nname = dr[3].ToString();
                        int Base = int.Parse(dr[7].ToString());
                        int Executive = int.Parse(dr[8].ToString());
                        int Travel = int.Parse(dr[9].ToString());
                        int housing = int.Parse(dr[10].ToString());
                        int Language = int.Parse(dr[11].ToString());
                        int Bonus = int.Parse(dr[12].ToString());
                        int OT = int.Parse(dr[13].ToString());
                        int totalsalary = int.Parse(dr[14].ToString());
                        int salarytax = int.Parse(dr[15].ToString());
                        int insurance = int.Parse(dr[16].ToString());
                        int netsalary = int.Parse(dr[17].ToString());
                        string paydate = dr[18].ToString();
                        string salarydate = ((DateTime)dr[18]).ToString("yyyy.MM");
                        


                        i++;
                        excSheet.Name = salarydate;
                        excSheet.Cells[1, 1] = "MQA給与明細";
                        excSheet.Cells[3, 1] = " 支給年月日 ";
                        excSheet.Cells[3, 2] = " 社員番号 ";
                        excSheet.Cells[3, 3] = " 氏名 ";
                        excSheet.Cells[3, 5] = " 基本給 ";
                        excSheet.Cells[3, 6] = " 役職手当 ";
                        excSheet.Cells[3, 7] = " その他手当 ";
                        excSheet.Cells[3, 8] = " 通勤手当 ";
                        excSheet.Cells[3, 9] = " 住宅手当 ";
                        excSheet.Cells[3, 10] = " 語学手当 ";
                        excSheet.Cells[3, 11] = " 手当合計 ";
                        excSheet.Cells[3, 12] = " 賞与 ";
                        excSheet.Cells[3, 13] = " 支給総額 ";
                        excSheet.Cells[3, 14] = " 社会保険料 ";
                        excSheet.Cells[3, 15] = " 給与所得税 ";
                        excSheet.Cells[3, 16] = " 控除総額 ";
                        excSheet.Cells[3, 17] = " 差引支給額 ";

                        rg = excSheet.get_Range("A" + i.ToString());
                        rg.NumberFormat = "yyyy/MM/dd";
                        excSheet.Cells[i, 1] = paydate;
                        excSheet.Cells[i, 2] = empid;
                        excSheet.Cells[i, 3] = Fname + " " + Lname;
                        excSheet.Cells[i, 4] = Nname;
                        excSheet.Cells[i, 5] = Base;
                        excSheet.Cells[i, 6] = Executive;
                        excSheet.Cells[i, 7] = OT;
                        excSheet.Cells[i, 8] = Travel;
                        excSheet.Cells[i, 9] = housing;
                        excSheet.Cells[i, 10] = Language;
                        excSheet.Cells[i, 11].Formula = "=SUM(F" + i + ":J" + i + ")";
                        excSheet.Cells[i, 12] = Bonus;
                        excSheet.Cells[i, 13] = totalsalary;
                        excSheet.Cells[i, 14] = insurance;
                        excSheet.Cells[i, 15] = salarytax;
                        excSheet.Cells[i, 16].Formula = "=SUM(N" + i + ":O" + i + ")";
                        excSheet.Cells[i, 17] = netsalary;

                        excSheet.Cells[i+1, 3] = "合計";
                        excSheet.Cells[i + 1, 5].Formula = "=SUM(E4:E" + i +")";
                        excSheet.Cells[i + 1, 6].Formula = "=SUM(F4:F" + i + ")";
                        excSheet.Cells[i + 1, 7].Formula = "=SUM(G4:G" + i + ")";
                        excSheet.Cells[i + 1, 8].Formula = "=SUM(H4:H" + i + ")";
                        excSheet.Cells[i + 1, 9].Formula = "=SUM(I4:I" + i + ")";
                        excSheet.Cells[i + 1, 10].Formula = "=SUM(J4:J" + i + ")";
                        excSheet.Cells[i + 1, 11].Formula = "=SUM(K4:K" + i + ")";
                        excSheet.Cells[i + 1, 12].Formula = "=SUM(L4:L" + i + ")";
                        excSheet.Cells[i + 1, 13].Formula = "=SUM(M4:M" + i + ")";
                        excSheet.Cells[i + 1, 14].Formula = "=SUM(N4:N" + i + ")";
                        excSheet.Cells[i + 1, 15].Formula = "=SUM(O4:O" + i + ")";
                        excSheet.Cells[i + 1, 16].Formula = "=SUM(P4:P" + i + ")";
                        excSheet.Cells[i + 1, 17].Formula = "=SUM(Q4:Q" + i + ")";




                        // format excel
                        excSheet.Range["A3"].Columns.AutoFit();
                        excSheet.Range["B3"].Columns.AutoFit();
                        excSheet.Range["E3"].Columns.AutoFit();
                        excSheet.Range["F3"].Columns.AutoFit();
                        excSheet.Range["G3"].Columns.AutoFit();
                        excSheet.Range["H3"].Columns.AutoFit();
                        excSheet.Range["I3"].Columns.AutoFit();
                        excSheet.Range["J3"].Columns.AutoFit();
                        excSheet.Range["K3"].Columns.AutoFit();
                        excSheet.Range["L3"].Columns.AutoFit();
                        excSheet.Range["M3"].Columns.AutoFit();
                        excSheet.Range["N3"].Columns.AutoFit();
                        excSheet.Range["O3"].Columns.AutoFit();
                        excSheet.Range["P3"].Columns.AutoFit();
                        excSheet.Range["Q3"].Columns.AutoFit();
                        excSheet.Range["C:C"].ColumnWidth = 30;
                        excSheet.Range["C3:D3"].Merge();
                        excSheet.Range["A3:A" + i.ToString()].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        excSheet.Range["B3:B" + i.ToString()].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        excSheet.Range["A3:Q3"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        excSheet.get_Range("A3", "Q3").Interior.Color = Color.FromArgb(191, 191, 191);
                        excSheet.get_Range("A3", "Q" + i.ToString()).Cells.Borders.Weight = XlBorderWeight.xlThin;
                        excSheet.Range["A" + (i + 1), "Q" + i].Borders.Weight = XlBorderWeight.xlThin;
                        excSheet.Range["E4:Q"+(i+1)+""].NumberFormat = "_(* #,##0.00_);_(* (#,##0.00)";
                        
                    }


                    

                }
                
            }
                
                excBook.SaveAs(@"C:\MQASalarylist"+timef+".xlsx");
                excBook.Close();
            

        }
       
        
    }
}
