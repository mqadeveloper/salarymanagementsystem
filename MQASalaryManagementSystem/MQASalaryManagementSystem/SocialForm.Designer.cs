﻿
namespace MQASalaryManagementSystem
{
    partial class SocialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.butUpdateSocial = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.labelNname = new System.Windows.Forms.Label();
            this.labelEmpId = new System.Windows.Forms.Label();
            this.tbDec = new System.Windows.Forms.TextBox();
            this.tbNov = new System.Windows.Forms.TextBox();
            this.tbOct = new System.Windows.Forms.TextBox();
            this.tbSep = new System.Windows.Forms.TextBox();
            this.tbAug = new System.Windows.Forms.TextBox();
            this.tbJul = new System.Windows.Forms.TextBox();
            this.tbJun = new System.Windows.Forms.TextBox();
            this.tbMay = new System.Windows.Forms.TextBox();
            this.tbApr = new System.Windows.Forms.TextBox();
            this.tbMar = new System.Windows.Forms.TextBox();
            this.tbFeb = new System.Windows.Forms.TextBox();
            this.tbJan = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.butShowSocial = new System.Windows.Forms.Button();
            this.comboBoxSelectY = new System.Windows.Forms.ComboBox();
            this.guna2VScrollBar1 = new Guna.UI2.WinForms.Guna2VScrollBar();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // butUpdateSocial
            // 
            this.butUpdateSocial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.butUpdateSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butUpdateSocial.Location = new System.Drawing.Point(653, 477);
            this.butUpdateSocial.Name = "butUpdateSocial";
            this.butUpdateSocial.Size = new System.Drawing.Size(153, 59);
            this.butUpdateSocial.TabIndex = 28;
            this.butUpdateSocial.Text = "Update";
            this.butUpdateSocial.UseVisualStyleBackColor = true;
            this.butUpdateSocial.Click += new System.EventHandler(this.butUpdateSocial_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Salmon;
            this.panel1.Controls.Add(this.label13);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(818, 31);
            this.panel1.TabIndex = 33;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(663, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(143, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Social Insurance";
            // 
            // labelNname
            // 
            this.labelNname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNname.AutoSize = true;
            this.labelNname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNname.Location = new System.Drawing.Point(121, 226);
            this.labelNname.Name = "labelNname";
            this.labelNname.Size = new System.Drawing.Size(0, 20);
            this.labelNname.TabIndex = 84;
            // 
            // labelEmpId
            // 
            this.labelEmpId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelEmpId.AutoSize = true;
            this.labelEmpId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmpId.Location = new System.Drawing.Point(58, 226);
            this.labelEmpId.Name = "labelEmpId";
            this.labelEmpId.Size = new System.Drawing.Size(0, 20);
            this.labelEmpId.TabIndex = 83;
            // 
            // tbDec
            // 
            this.tbDec.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbDec.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDec.Location = new System.Drawing.Point(679, 420);
            this.tbDec.MaxLength = 4;
            this.tbDec.Multiline = true;
            this.tbDec.Name = "tbDec";
            this.tbDec.Size = new System.Drawing.Size(100, 25);
            this.tbDec.TabIndex = 82;
            // 
            // tbNov
            // 
            this.tbNov.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbNov.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNov.Location = new System.Drawing.Point(492, 420);
            this.tbNov.MaxLength = 4;
            this.tbNov.Multiline = true;
            this.tbNov.Name = "tbNov";
            this.tbNov.Size = new System.Drawing.Size(100, 25);
            this.tbNov.TabIndex = 81;
            // 
            // tbOct
            // 
            this.tbOct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbOct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOct.Location = new System.Drawing.Point(296, 420);
            this.tbOct.MaxLength = 4;
            this.tbOct.Multiline = true;
            this.tbOct.Name = "tbOct";
            this.tbOct.Size = new System.Drawing.Size(100, 25);
            this.tbOct.TabIndex = 80;
            // 
            // tbSep
            // 
            this.tbSep.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbSep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSep.Location = new System.Drawing.Point(97, 420);
            this.tbSep.MaxLength = 4;
            this.tbSep.Multiline = true;
            this.tbSep.Name = "tbSep";
            this.tbSep.Size = new System.Drawing.Size(100, 25);
            this.tbSep.TabIndex = 79;
            // 
            // tbAug
            // 
            this.tbAug.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbAug.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAug.Location = new System.Drawing.Point(679, 342);
            this.tbAug.MaxLength = 4;
            this.tbAug.Multiline = true;
            this.tbAug.Name = "tbAug";
            this.tbAug.Size = new System.Drawing.Size(100, 25);
            this.tbAug.TabIndex = 78;
            // 
            // tbJul
            // 
            this.tbJul.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbJul.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJul.Location = new System.Drawing.Point(492, 342);
            this.tbJul.MaxLength = 4;
            this.tbJul.Multiline = true;
            this.tbJul.Name = "tbJul";
            this.tbJul.Size = new System.Drawing.Size(100, 25);
            this.tbJul.TabIndex = 77;
            // 
            // tbJun
            // 
            this.tbJun.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbJun.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJun.Location = new System.Drawing.Point(296, 342);
            this.tbJun.MaxLength = 4;
            this.tbJun.Multiline = true;
            this.tbJun.Name = "tbJun";
            this.tbJun.Size = new System.Drawing.Size(100, 25);
            this.tbJun.TabIndex = 76;
            // 
            // tbMay
            // 
            this.tbMay.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbMay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMay.Location = new System.Drawing.Point(97, 342);
            this.tbMay.MaxLength = 4;
            this.tbMay.Multiline = true;
            this.tbMay.Name = "tbMay";
            this.tbMay.Size = new System.Drawing.Size(100, 25);
            this.tbMay.TabIndex = 75;
            // 
            // tbApr
            // 
            this.tbApr.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbApr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbApr.Location = new System.Drawing.Point(679, 263);
            this.tbApr.MaxLength = 4;
            this.tbApr.Multiline = true;
            this.tbApr.Name = "tbApr";
            this.tbApr.Size = new System.Drawing.Size(100, 25);
            this.tbApr.TabIndex = 74;
            // 
            // tbMar
            // 
            this.tbMar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbMar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMar.Location = new System.Drawing.Point(492, 263);
            this.tbMar.MaxLength = 4;
            this.tbMar.Multiline = true;
            this.tbMar.Name = "tbMar";
            this.tbMar.Size = new System.Drawing.Size(100, 25);
            this.tbMar.TabIndex = 73;
            // 
            // tbFeb
            // 
            this.tbFeb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbFeb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFeb.Location = new System.Drawing.Point(296, 263);
            this.tbFeb.MaxLength = 4;
            this.tbFeb.Multiline = true;
            this.tbFeb.Name = "tbFeb";
            this.tbFeb.Size = new System.Drawing.Size(100, 25);
            this.tbFeb.TabIndex = 72;
            // 
            // tbJan
            // 
            this.tbJan.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbJan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJan.Location = new System.Drawing.Point(97, 263);
            this.tbJan.MaxLength = 4;
            this.tbJan.Multiline = true;
            this.tbJan.Name = "tbJan";
            this.tbJan.Size = new System.Drawing.Size(100, 25);
            this.tbJan.TabIndex = 71;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(628, 420);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 25);
            this.label9.TabIndex = 70;
            this.label9.Text = "Dec";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(437, 420);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 25);
            this.label10.TabIndex = 69;
            this.label10.Text = "Nov";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(241, 420);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 25);
            this.label11.TabIndex = 68;
            this.label11.Text = "Oct";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(44, 420);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 25);
            this.label12.TabIndex = 67;
            this.label12.Text = "Sep";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(628, 342);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 25);
            this.label8.TabIndex = 66;
            this.label8.Text = "Aug";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(437, 342);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 25);
            this.label7.TabIndex = 65;
            this.label7.Text = "Jul";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(243, 342);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 25);
            this.label6.TabIndex = 64;
            this.label6.Text = "Jun";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(44, 342);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 25);
            this.label5.TabIndex = 63;
            this.label5.Text = "May";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(628, 263);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 25);
            this.label4.TabIndex = 62;
            this.label4.Text = "Apr";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(437, 263);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 25);
            this.label3.TabIndex = 61;
            this.label3.Text = "Mar";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(241, 263);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 25);
            this.label2.TabIndex = 60;
            this.label2.Text = "Feb";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(44, 263);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 25);
            this.label1.TabIndex = 59;
            this.label1.Text = "Jan";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(42, 37);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(434, 156);
            this.dataGridView1.TabIndex = 30;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // butShowSocial
            // 
            this.butShowSocial.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.butShowSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butShowSocial.Location = new System.Drawing.Point(609, 37);
            this.butShowSocial.Name = "butShowSocial";
            this.butShowSocial.Size = new System.Drawing.Size(153, 59);
            this.butShowSocial.TabIndex = 3;
            this.butShowSocial.Text = "Show";
            this.butShowSocial.UseVisualStyleBackColor = true;
            this.butShowSocial.Click += new System.EventHandler(this.butShowSocial_Click);
            // 
            // comboBoxSelectY
            // 
            this.comboBoxSelectY.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBoxSelectY.FormattingEnabled = true;
            this.comboBoxSelectY.Location = new System.Drawing.Point(482, 37);
            this.comboBoxSelectY.Name = "comboBoxSelectY";
            this.comboBoxSelectY.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSelectY.TabIndex = 29;
            // 
            // guna2VScrollBar1
            // 
            this.guna2VScrollBar1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.guna2VScrollBar1.AutoSize = true;
            this.guna2VScrollBar1.HoverState.Parent = null;
            this.guna2VScrollBar1.InUpdate = false;
            this.guna2VScrollBar1.LargeChange = 10;
            this.guna2VScrollBar1.Location = new System.Drawing.Point(458, 37);
            this.guna2VScrollBar1.Name = "guna2VScrollBar1";
            this.guna2VScrollBar1.PressedState.Parent = this.guna2VScrollBar1;
            this.guna2VScrollBar1.ScrollbarSize = 18;
            this.guna2VScrollBar1.Size = new System.Drawing.Size(18, 156);
            this.guna2VScrollBar1.TabIndex = 35;
            // 
            // SocialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 548);
            this.ControlBox = false;
            this.Controls.Add(this.labelNname);
            this.Controls.Add(this.labelEmpId);
            this.Controls.Add(this.tbDec);
            this.Controls.Add(this.tbNov);
            this.Controls.Add(this.tbOct);
            this.Controls.Add(this.tbSep);
            this.Controls.Add(this.tbAug);
            this.Controls.Add(this.tbJul);
            this.Controls.Add(this.tbJun);
            this.Controls.Add(this.tbMay);
            this.Controls.Add(this.tbApr);
            this.Controls.Add(this.tbMar);
            this.Controls.Add(this.tbFeb);
            this.Controls.Add(this.tbJan);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.guna2VScrollBar1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.comboBoxSelectY);
            this.Controls.Add(this.butUpdateSocial);
            this.Controls.Add(this.butShowSocial);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SocialForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button butUpdateSocial;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelNname;
        private System.Windows.Forms.Label labelEmpId;
        private System.Windows.Forms.TextBox tbDec;
        private System.Windows.Forms.TextBox tbNov;
        private System.Windows.Forms.TextBox tbOct;
        private System.Windows.Forms.TextBox tbSep;
        private System.Windows.Forms.TextBox tbAug;
        private System.Windows.Forms.TextBox tbJul;
        private System.Windows.Forms.TextBox tbJun;
        private System.Windows.Forms.TextBox tbMay;
        private System.Windows.Forms.TextBox tbApr;
        private System.Windows.Forms.TextBox tbMar;
        private System.Windows.Forms.TextBox tbFeb;
        private System.Windows.Forms.TextBox tbJan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button butShowSocial;
        private System.Windows.Forms.ComboBox comboBoxSelectY;
        private Guna.UI2.WinForms.Guna2VScrollBar guna2VScrollBar1;
    }
}