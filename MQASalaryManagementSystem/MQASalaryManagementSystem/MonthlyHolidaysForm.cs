﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MQASalaryManagementSystem
{
    public partial class MonthlyHolidaysForm : Form
    {
        private string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public MonthlyHolidaysForm()
        {
            InitializeComponent();
            ComboBoxMY();
            //CheckBoxColor();
        }
        public void ShowMonthly()
        {
            int m = int.Parse(ComboMonth(comboBoxM.Text));
            int y = int.Parse(comboBoxY.Text);
          

            try
            {
                DateTime day1 = new DateTime(y, m, 1);
                tbNo1.Text = day1.ToString("dd");
                tbday1.Text = day1.ToString("dddd");
                DateTime day2 = new DateTime(y, m, 2);
                tbNo2.Text = day2.ToString("dd");
                tbday2.Text = day2.ToString("dddd");
                DateTime day3 = new DateTime(y, m, 3);
                tbNo3.Text = day3.ToString("dd");
                tbday3.Text = day3.ToString("dddd");
                DateTime day4 = new DateTime(y, m, 4);
                tbNo4.Text = day4.ToString("dd");
                tbday4.Text = day4.ToString("dddd");
                DateTime day5 = new DateTime(y, m, 5);
                tbNo5.Text = day5.ToString("dd");
                tbday5.Text = day5.ToString("dddd");
                DateTime day6 = new DateTime(y, m, 6);
                tbNo6.Text = day6.ToString("dd");
                tbday6.Text = day6.ToString("dddd");
                DateTime day7 = new DateTime(y, m, 7);
                tbNo7.Text = day7.ToString("dd");
                tbday7.Text = day7.ToString("dddd");
                DateTime day8 = new DateTime(y, m, 8);
                tbNo8.Text = day8.ToString("dd");
                tbday8.Text = day8.ToString("dddd");
                DateTime day9 = new DateTime(y, m, 9);
                tbNo9.Text = day9.ToString("dd");
                tbday9.Text = day9.ToString("dddd");
                DateTime day10 = new DateTime(y, m, 10);
                tbNo10.Text = day10.ToString("dd");
                tbday10.Text = day10.ToString("dddd");
                DateTime day11 = new DateTime(y, m, 11);
                tbNo11.Text = day11.ToString("dd");
                tbday11.Text = day11.ToString("dddd");
                DateTime day12 = new DateTime(y, m, 12);
                tbNo12.Text = day12.ToString("dd");
                tbday12.Text = day12.ToString("dddd");
                DateTime day13 = new DateTime(y, m, 13);
                tbNo13.Text = day13.ToString("dd");
                tbday13.Text = day13.ToString("dddd");
                DateTime day14 = new DateTime(y, m, 14);
                tbNo14.Text = day14.ToString("dd");
                tbday14.Text = day14.ToString("dddd");
                DateTime day15 = new DateTime(y, m, 15);
                tbNo15.Text = day15.ToString("dd");
                tbday15.Text = day15.ToString("dddd");
                DateTime day16 = new DateTime(y, m, 16);
                tbNo16.Text = day16.ToString("dd");
                tbday16.Text = day16.ToString("dddd");
                DateTime day17 = new DateTime(y, m, 17);
                tbNo17.Text = day17.ToString("dd");
                tbday17.Text = day17.ToString("dddd");
                DateTime day18 = new DateTime(y, m, 18);
                tbNo18.Text = day18.ToString("dd");
                tbday18.Text = day18.ToString("dddd");
                DateTime day19 = new DateTime(y, m, 19);
                tbNo19.Text = day19.ToString("dd");
                tbday19.Text = day19.ToString("dddd");
                DateTime day20 = new DateTime(y, m, 20);
                tbNo20.Text = day20.ToString("dd");
                tbday20.Text = day20.ToString("dddd");
                DateTime day21 = new DateTime(y, m, 21);
                tbNo21.Text = day21.ToString("dd");
                tbday21.Text = day21.ToString("dddd");
                DateTime day22 = new DateTime(y, m, 22);
                tbNo22.Text = day22.ToString("dd");
                tbday22.Text = day22.ToString("dddd");
                DateTime day23 = new DateTime(y, m, 23);
                tbNo23.Text = day23.ToString("dd");
                tbday23.Text = day23.ToString("dddd");
                DateTime day24 = new DateTime(y, m, 24);
                tbNo24.Text = day24.ToString("dd");
                tbday24.Text = day24.ToString("dddd");
                DateTime day25 = new DateTime(y, m, 25);
                tbNo25.Text = day25.ToString("dd");
                tbday25.Text = day25.ToString("dddd");
                DateTime day26 = new DateTime(y, m, 26);
                tbNo26.Text = day26.ToString("dd");
                tbday26.Text = day26.ToString("dddd");
                DateTime day27 = new DateTime(y, m, 27);
                tbNo27.Text = day27.ToString("dd");
                tbday27.Text = day27.ToString("dddd");
                DateTime day28 = new DateTime(y, m, 28);
                tbNo28.Text = day28.ToString("dd");
                tbday28.Text = day28.ToString("dddd");




                tbday1.ForeColor = DayColor(day1.ToString("dddd"));
                tbday2.ForeColor = DayColor(day2.ToString("dddd"));
                tbday3.ForeColor = DayColor(day3.ToString("dddd"));
                tbday4.ForeColor = DayColor(day4.ToString("dddd"));
                tbday5.ForeColor = DayColor(day5.ToString("dddd"));
                tbday6.ForeColor = DayColor(day6.ToString("dddd"));
                tbday7.ForeColor = DayColor(day7.ToString("dddd"));
                tbday8.ForeColor = DayColor(day8.ToString("dddd"));
                tbday9.ForeColor = DayColor(day9.ToString("dddd"));
                tbday10.ForeColor = DayColor(day10.ToString("dddd"));
                tbday11.ForeColor = DayColor(day11.ToString("dddd"));
                tbday12.ForeColor = DayColor(day12.ToString("dddd"));
                tbday13.ForeColor = DayColor(day13.ToString("dddd"));
                tbday14.ForeColor = DayColor(day14.ToString("dddd"));
                tbday15.ForeColor = DayColor(day15.ToString("dddd"));
                tbday16.ForeColor = DayColor(day16.ToString("dddd"));
                tbday17.ForeColor = DayColor(day17.ToString("dddd"));
                tbday18.ForeColor = DayColor(day18.ToString("dddd"));
                tbday19.ForeColor = DayColor(day19.ToString("dddd"));
                tbday20.ForeColor = DayColor(day20.ToString("dddd"));
                tbday21.ForeColor = DayColor(day21.ToString("dddd"));
                tbday22.ForeColor = DayColor(day22.ToString("dddd"));
                tbday23.ForeColor = DayColor(day23.ToString("dddd"));
                tbday24.ForeColor = DayColor(day24.ToString("dddd"));
                tbday25.ForeColor = DayColor(day25.ToString("dddd"));
                tbday26.ForeColor = DayColor(day26.ToString("dddd"));
                tbday27.ForeColor = DayColor(day27.ToString("dddd"));
                tbday28.ForeColor = DayColor(day28.ToString("dddd"));
                











                if (m == 2)
                {
                    try
                    {
                        DateTime day29 = new DateTime(y, m, 29);
                        tbNo29.Text = day29.ToString("dd");
                        tbday29.Text = day29.ToString("dddd");
                        tbday29.ForeColor = DayColor(day29.ToString("dddd"));
                       
                        panelday30.Visible = false;
                        panelday31.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        panelday29.Visible = false;
                        panelday30.Visible = false;
                        panelday31.Visible = false;
                    }
                   
                }
                else
                {
                    DateTime day29 = new DateTime(y, m, 29);
                    tbNo29.Text = day29.ToString("dd");
                    tbday29.Text = day29.ToString("dddd");
                    DateTime day30 = new DateTime(y, m, 30);
                    tbNo30.Text = day30.ToString("dd");
                    tbday30.Text = day30.ToString("dddd");

                    tbday29.ForeColor = DayColor(day29.ToString("dddd"));
                    tbday30.ForeColor = DayColor(day30.ToString("dddd"));

                    panelday31.Visible = false;
                    if (m==1||m==3||m==5 || m == 7 || m==8||m==10||m==12)
                    {
                        DateTime day31 = new DateTime(y, m, 31);
                        tbNo31.Text = day31.ToString("dd");
                        tbday31.Text = day31.ToString("dddd");
                        tbday31.ForeColor = DayColor(day31.ToString("dddd"));
                        panelday31.Visible = true;
                    }
                    
                }
               

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                
            }

        }

        private void WeekendCheck()
        {
           

            if (tbday1.Text == "Saturday" || tbday1.Text == "Sunday" )
            {
                tbday1.BackColor = Color.LightPink;
                tbNo1.BackColor = Color.LightPink;
               // checkBox1.Visible = false;
            }
            if (tbday2.Text == "Saturday" || tbday2.Text == "Sunday" )
            {
                tbday2.BackColor = Color.LightPink;
                tbNo2.BackColor = Color.LightPink;
                //checkBox2.Visible = false;
            }
             if (tbday3.Text == "Saturday" || tbday3.Text == "Sunday" )
            {
                tbday3.BackColor = Color.LightPink;
                tbNo3.BackColor = Color.LightPink;
                //checkBox3.Visible = false;
            }
             if (tbday4.Text == "Saturday" || tbday4.Text == "Sunday" )
            {
                tbday4.BackColor = Color.LightPink;
                tbNo4.BackColor = Color.LightPink;
                //checkBox4.Visible = false;
            }
             if (tbday5.Text == "Saturday" || tbday5.Text == "Sunday" )
            {
                tbday5.BackColor = Color.LightPink;
                tbNo5.BackColor = Color.LightPink;
                //checkBox5.Visible = false;
            }
             if (tbday6.Text == "Saturday" || tbday6.Text == "Sunday" )
            {
                tbday6.BackColor = Color.LightPink;
                tbNo6.BackColor = Color.LightPink;
                //checkBox6.Visible = false;
            }
             if (tbday7.Text == "Saturday" || tbday7.Text == "Sunday" )
            {
                tbday7.BackColor = Color.LightPink;
                tbNo7.BackColor = Color.LightPink;
                //checkBox7.Visible = false;
            }
             if (tbday8.Text == "Saturday" || tbday8.Text == "Sunday" )
            {
                tbday8.BackColor = Color.LightPink;
                tbNo8.BackColor = Color.LightPink;
                //checkBox8.Visible = false;
            }
             if (tbday9.Text == "Saturday" || tbday9.Text == "Sunday" )
            {
                tbday9.BackColor = Color.LightPink;
                tbNo9.BackColor = Color.LightPink;
                //checkBox9.Visible = false;
            }
             if (tbday10.Text == "Saturday" || tbday10.Text == "Sunday" )
            {
                tbday10.BackColor = Color.LightPink;
                tbNo10.BackColor = Color.LightPink;
                //checkBox10.Visible = false;
            }
             if (tbday11.Text == "Saturday" || tbday11.Text == "Sunday" )
            {
                tbday11.BackColor = Color.LightPink;
                tbNo11.BackColor = Color.LightPink;
                //checkBox11.Visible = false;
            }
             if (tbday12.Text == "Saturday" || tbday12.Text == "Sunday" )
            {
                tbday12.BackColor = Color.LightPink;
                tbNo12.BackColor = Color.LightPink;
                //checkBox12.Visible = false;
            }
             if (tbday13.Text == "Saturday" || tbday13.Text == "Sunday" )
            {
                tbday13.BackColor = Color.LightPink;
                tbNo13.BackColor = Color.LightPink;
                //checkBox13.Visible = false;
            }
             if (tbday14.Text == "Saturday" || tbday14.Text == "Sunday" )
            {
                tbday14.BackColor = Color.LightPink;
                tbNo14.BackColor = Color.LightPink;
               // checkBox14.Visible = false;
            }
             if (tbday15.Text == "Saturday" || tbday15.Text == "Sunday" ) 
            {
                tbday15.BackColor = Color.LightPink;
                tbNo15.BackColor = Color.LightPink;
               // checkBox15.Visible = false;
            }
             if (tbday16.Text == "Saturday" || tbday16.Text == "Sunday" )
            {
                tbday16.BackColor = Color.LightPink;
                tbNo16.BackColor = Color.LightPink;
               // checkBox16.Visible = false;
            }
             if (tbday17.Text == "Saturday" || tbday17.Text == "Sunday" )
            {
                tbday17.BackColor = Color.LightPink;
                tbNo17.BackColor = Color.LightPink;
               // checkBox17.Visible = false;
            }
             if (tbday18.Text == "Saturday" || tbday18.Text == "Sunday" )
            {
                tbday18.BackColor = Color.LightPink;
                tbNo18.BackColor = Color.LightPink;
               // checkBox18.Visible = false;
            }
             if (tbday19.Text == "Saturday" || tbday19.Text == "Sunday" )
            {
                tbday19.BackColor = Color.LightPink;
                tbNo19.BackColor = Color.LightPink;
                //checkBox19.Visible = false;
            }
             if (tbday20.Text == "Saturday" || tbday20.Text == "Sunday" )
            {
                tbday20.BackColor = Color.LightPink;
                tbNo20.BackColor = Color.LightPink;
                //checkBox20.Visible = false;
            }
             if (tbday21.Text == "Saturday" || tbday21.Text == "Sunday" )
            {
                tbday21.BackColor = Color.LightPink;
                tbNo21.BackColor = Color.LightPink;
                //checkBox21.Visible = false;
            }
             if (tbday22.Text == "Saturday" || tbday22.Text == "Sunday" )
            {
                tbday22.BackColor = Color.LightPink;
                tbNo22.BackColor = Color.LightPink;
                //checkBox22.Visible = false;
            }
             if (tbday23.Text == "Saturday" || tbday23.Text == "Sunday" )
            {
                tbday23.BackColor = Color.LightPink;
                tbNo23.BackColor = Color.LightPink;
                //checkBox23.Visible = false;
            }
             if (tbday24.Text == "Saturday" || tbday24.Text == "Sunday" )
            {
                tbday24.BackColor = Color.LightPink;
                tbNo24.BackColor = Color.LightPink;
               // checkBox24.Visible = false;
            }
             if (tbday25.Text == "Saturday" || tbday25.Text == "Sunday" )
            {
                tbday25.BackColor = Color.LightPink;
                tbNo25.BackColor = Color.LightPink;
                //checkBox25.Visible = false;
            }
             if (tbday26.Text == "Saturday" || tbday26.Text == "Sunday" )
            {
                tbday26.BackColor = Color.LightPink;
                tbNo26.BackColor = Color.LightPink;
                //checkBox26.Visible = false;
            }
             if (tbday27.Text == "Saturday" || tbday27.Text == "Sunday" )
            {
                tbday27.BackColor = Color.LightPink;
                tbNo27.BackColor = Color.LightPink;
                //checkBox27.Visible = false;
            }
             if (tbday28.Text == "Saturday" || tbday28.Text == "Sunday" )
            {
                tbday28.BackColor = Color.LightPink;
                tbNo28.BackColor = Color.LightPink;
                //checkBox28.Visible = false;
            }
            if (tbday29.Text == "Saturday" || tbday29.Text == "Sunday" )
            {
                tbday29.BackColor = Color.LightPink;
                tbNo29.BackColor = Color.LightPink;
                //checkBox29.Visible = false;
            }
            if (tbday30.Text == "Saturday" || tbday30.Text == "Sunday")
            {
                tbday30.BackColor = Color.LightPink;
                tbNo30.BackColor = Color.LightPink;
                //checkBox30.Visible = false;
            }
            if (tbday31.Text == "Saturday" || tbday31.Text == "Sunday")
            {
                tbday31.BackColor = Color.LightPink;
                tbNo31.BackColor = Color.LightPink;
                // checkBox31.Visible = false;
            }



        }
        public void HolidayCheck()
        {
            if (CheckMY(1) == 1)
            {
                tbday1.BackColor = Color.LightPink;
                tbNo1.BackColor = Color.LightPink;
                checkBox1.Checked = true;    

            }
            if (CheckMY(2) == 1)
            {
                tbday2.BackColor = Color.LightPink;
                tbNo2.BackColor = Color.LightPink;
                checkBox2.Checked = true;
            }
            if ( CheckMY(3) == 1)
            {
                tbday3.BackColor = Color.LightPink;
                tbNo3.BackColor = Color.LightPink;
                checkBox3.Checked = true;
            }
            if ( CheckMY(4) == 1)
            {
                tbday4.BackColor = Color.LightPink;
                tbNo4.BackColor = Color.LightPink;
                checkBox4.Checked = true;
            }
            if ( CheckMY(5) == 1)
            {
                tbday5.BackColor = Color.LightPink;
                tbNo5.BackColor = Color.LightPink;
                checkBox5.Checked = true;
            }
            if ( CheckMY(6) == 1)
            {
                tbday6.BackColor = Color.LightPink;
                tbNo6.BackColor = Color.LightPink;
                checkBox6.Checked = true;
            }
            if ( CheckMY(7) == 1)
            {
                tbday7.BackColor = Color.LightPink;
                tbNo7.BackColor = Color.LightPink;
                checkBox7.Checked = true;
            }
            if ( CheckMY(8) == 1)
            {
                tbday8.BackColor = Color.LightPink;
                tbNo8.BackColor = Color.LightPink;
                checkBox8.Checked = true;
            }
            if ( CheckMY(9) == 1)
            {
                tbday9.BackColor = Color.LightPink;
                tbNo9.BackColor = Color.LightPink;
                checkBox9.Checked = true;
            }
            if ( CheckMY(10) == 1)
            {
                tbday10.BackColor = Color.LightPink;
                tbNo10.BackColor = Color.LightPink;
                checkBox10.Checked = true;
            }
            if ( CheckMY(11) == 1)
            {
                tbday11.BackColor = Color.LightPink;
                tbNo11.BackColor = Color.LightPink;
                checkBox11.Checked = true;
            }
            if ( CheckMY(12) == 1)
            {
                tbday12.BackColor = Color.LightPink;
                tbNo12.BackColor = Color.LightPink;
                checkBox12.Checked = true;
            }
            if ( CheckMY(13) == 1)
            {
                tbday13.BackColor = Color.LightPink;
                tbNo13.BackColor = Color.LightPink;
                checkBox13.Checked = true;
            }
            if ( CheckMY(14) == 1)
            {
                tbday14.BackColor = Color.LightPink;
                tbNo14.BackColor = Color.LightPink;
                checkBox14.Checked = true;
            }
            if ( CheckMY(15) == 1)
            {
                tbday15.BackColor = Color.LightPink;
                tbNo15.BackColor = Color.LightPink;
                checkBox15.Checked = true;
            }
            if ( CheckMY(16) == 1)
            {
                tbday16.BackColor = Color.LightPink;
                tbNo16.BackColor = Color.LightPink;
                checkBox16.Checked = true;
            }
            if ( CheckMY(17) == 1)
            {
                tbday17.BackColor = Color.LightPink;
                tbNo17.BackColor = Color.LightPink;
                checkBox17.Checked = true;
            }
            if ( CheckMY(18) == 1)
            {
                tbday18.BackColor = Color.LightPink;
                tbNo18.BackColor = Color.LightPink;
                checkBox18.Checked = true;
            }
            if ( CheckMY(19) == 1)
            {
                tbday19.BackColor = Color.LightPink;
                tbNo19.BackColor = Color.LightPink;
                checkBox19.Checked = true;
            }
            if ( CheckMY(20) == 1)
            {
                tbday20.BackColor = Color.LightPink;
                tbNo20.BackColor = Color.LightPink;
                checkBox20.Checked = true;
            }
            if ( CheckMY(21) == 1)
            {
                tbday21.BackColor = Color.LightPink;
                tbNo21.BackColor = Color.LightPink;
                checkBox21.Checked = true;
            }
            if ( CheckMY(22) == 1)
            {
                tbday22.BackColor = Color.LightPink;
                tbNo22.BackColor = Color.LightPink;
                checkBox1.Checked = true;
            }
            if ( CheckMY(23) == 1)
            {
                tbday23.BackColor = Color.LightPink;
                tbNo23.BackColor = Color.LightPink;
                checkBox23.Checked = true;
            }
            if ( CheckMY(24) == 1)
            {
                tbday24.BackColor = Color.LightPink;
                tbNo24.BackColor = Color.LightPink;
                checkBox24.Checked = true;
            }
            if ( CheckMY(25) == 1)
            {
                tbday25.BackColor = Color.LightPink;
                tbNo25.BackColor = Color.LightPink;
                checkBox25.Checked = true;
            }
            if ( CheckMY(26) == 1)
            {
                tbday26.BackColor = Color.LightPink;
                tbNo26.BackColor = Color.LightPink;
                checkBox26.Checked = true;
            }
            if ( CheckMY(27) == 1)
            {
                tbday27.BackColor = Color.LightPink;
                tbNo27.BackColor = Color.LightPink;
                checkBox27.Checked = true;
            }
            if ( CheckMY(28) == 1)
            {
                tbday28.BackColor = Color.LightPink;
                tbNo28.BackColor = Color.LightPink;
                checkBox28.Checked = true;
            }
            if ( CheckMY(29) == 1)
            {
                tbday29.BackColor = Color.LightPink;
                tbNo29.BackColor = Color.LightPink;
                checkBox29.Checked = true;
            }
            int m = int.Parse(ComboMonth(comboBoxM.Text));
            if (m != 2)
            {


                if (CheckMY(30) == 1)
                {
                    tbday30.BackColor = Color.LightPink;
                    tbNo30.BackColor = Color.LightPink;
                }
            }
            
            if  (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12)
            {


                if (CheckMY(31) == 1)
                {
                    tbday31.BackColor = Color.LightPink;
                    tbNo31.BackColor = Color.LightPink;
                }
            }

        }
        
        public int CheckMY(int date)
        {
            int yes = 0;
            int m = int.Parse(ComboMonth(comboBoxM.Text));
            int y = int.Parse(comboBoxY.Text);
            if (panelday29.Visible != false || panelday31.Visible != false)
            {


                using (IDbConnection cnn = new OleDbConnection(con))
                {

                    string sql_Text = "SELECT *";
                    sql_Text += "FROM Holiday ";
                    sql_Text += "WHERE MONTH(HoliDay_Date) = " + m + " AND YEAR(HoliDay_Date) = " + y + " AND Day(HoliDay_Date) = " + date;


                    DataTable tbl = new DataTable();
                    DateTime day = new DateTime(y, m, date);

                    using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql_Text, (OleDbConnection)cnn))
                    {
                        adapter.Fill(tbl);
                        foreach (DataRow dr in tbl.Rows)
                        {

                            if (dr[0].ToString() == day.ToString())
                            {
                                yes = 1;

                            }
                        }

                    }

                }
            }
            return yes;
        }

        private void ComboBoxMY()
        {

            comboBoxM.DataSource = CultureInfo.InvariantCulture.DateTimeFormat.MonthNames.Take(12).ToList();
            comboBoxM.SelectedItem = CultureInfo.InvariantCulture.DateTimeFormat.MonthNames[DateTime.Now.AddMonths(-1).Month - 1];
            comboBoxY.DataSource = Enumerable.Range(2011, DateTime.Now.Year - 2011 + 1).ToList();
            comboBoxY.SelectedItem = DateTime.Now.Year;


        }
        private string ComboMonth(string month)
        {
            string mnum = "";
            if (comboBoxM.Text == "January")
            {
                mnum = "01";
            }
            else if (comboBoxM.Text == "February")
            {
                mnum = "02";
            }
            else if (comboBoxM.Text == "March")
            {
                mnum = "03";
            }
            else if (comboBoxM.Text == "April")
            {
                mnum = "04";
            }
            else if (comboBoxM.Text == "May")
            {
                mnum = "05";
            }
            else if (comboBoxM.Text == "June")
            {
                mnum = "06";
            }
            else if (comboBoxM.Text == "July")
            {
                mnum = "07";
            }
            else if (comboBoxM.Text == "August")
            {
                mnum = "08";
            }
            else if (comboBoxM.Text == "September")
            {
                mnum = "09";
            }
            else if (comboBoxM.Text == "October")
            {
                mnum = "10";
            }
            else if (comboBoxM.Text == "November")
            {
                mnum = "11";
            }
            else if (comboBoxM.Text == "December")
            {
                mnum = "12";
            }
            return mnum;
        }
        private void CleanTb()
        {
            tbday1.BackColor = Color.Empty;
            tbNo1.BackColor = Color.Empty;
            tbday2.BackColor = Color.Empty;
            tbNo2.BackColor = Color.Empty;
            tbday3.BackColor = Color.Empty;
            tbNo3.BackColor = Color.Empty;
            tbday4.BackColor = Color.Empty;
            tbNo4.BackColor = Color.Empty;
            tbday5.BackColor = Color.Empty;
            tbNo5.BackColor = Color.Empty;
            tbday6.BackColor = Color.Empty;
            tbNo6.BackColor = Color.Empty;
            tbday7.BackColor = Color.Empty;
            tbNo7.BackColor = Color.Empty;
            tbday8.BackColor = Color.Empty;
            tbNo8.BackColor = Color.Empty;
            tbday9.BackColor = Color.Empty;
            tbNo9.BackColor = Color.Empty;
            tbday10.BackColor = Color.Empty;
            tbNo10.BackColor = Color.Empty;
            tbday11.BackColor = Color.Empty;
            tbNo11.BackColor = Color.Empty;
            tbday12.BackColor = Color.Empty;
            tbNo12.BackColor = Color.Empty;
            tbday13.BackColor = Color.Empty;
            tbNo13.BackColor = Color.Empty;
            tbday14.BackColor = Color.Empty;
            tbNo14.BackColor = Color.Empty;
            tbday15.BackColor = Color.Empty;
            tbNo15.BackColor = Color.Empty;
            tbday16.BackColor = Color.Empty;
            tbNo16.BackColor = Color.Empty;
            tbday17.BackColor = Color.Empty;
            tbNo17.BackColor = Color.Empty;
            tbday18.BackColor = Color.Empty;
            tbNo18.BackColor = Color.Empty;
            tbday19.BackColor = Color.Empty;
            tbNo19.BackColor = Color.Empty;
            tbday20.BackColor = Color.Empty;
            tbNo20.BackColor = Color.Empty;
            tbday21.BackColor = Color.Empty;
            tbNo21.BackColor = Color.Empty;
            tbday22.BackColor = Color.Empty;
            tbNo22.BackColor = Color.Empty;
            tbday23.BackColor = Color.Empty;
            tbNo23.BackColor = Color.Empty;
            tbday24.BackColor = Color.Empty;
            tbNo24.BackColor = Color.Empty;
            tbday25.BackColor = Color.Empty;
            tbNo25.BackColor = Color.Empty;
            tbday26.BackColor = Color.Empty;
            tbNo26.BackColor = Color.Empty;
            tbday27.BackColor = Color.Empty;
            tbNo27.BackColor = Color.Empty;
            tbday28.BackColor = Color.Empty;
            tbNo28.BackColor = Color.Empty;
            tbday29.BackColor = Color.Empty;
            tbNo29.BackColor = Color.Empty;
            tbday30.BackColor = Color.Empty;
            tbNo30.BackColor = Color.Empty;
            tbday3.BackColor = Color.Empty;
            tbNo31.BackColor = Color.Empty;
            checkBox1.Checked = false;
            checkBox1.Visible = true;
            checkBox2.Checked = false;
            checkBox2.Visible = true;
            checkBox3.Checked = false;
            checkBox3.Visible = true;
            checkBox4.Checked = false;
            checkBox4.Visible = true;
            checkBox5.Checked = false;
            checkBox5.Visible = true;
            checkBox6.Checked = false;
            checkBox6.Visible = true;
            checkBox7.Checked = false;
            checkBox7.Visible = true;
            checkBox8.Checked = false;
            checkBox8.Visible = true;
            checkBox9.Checked = false;
            checkBox9.Visible = true;
            checkBox10.Checked = false;
            checkBox10.Visible = true;
            checkBox11.Checked = false;
            checkBox11.Visible = true;
            checkBox12.Checked = false;
            checkBox12.Visible = true;
            checkBox13.Checked = false;
            checkBox13.Visible = true;
            checkBox14.Checked = false;
            checkBox14.Visible = true;
            checkBox15.Checked = false;
            checkBox15.Visible = true;
            checkBox16.Checked = false;
            checkBox16.Visible = true;
            checkBox17.Checked = false;
            checkBox17.Visible = true;
            checkBox18.Checked = false;
            checkBox18.Visible = true;
            checkBox19.Checked = false;
            checkBox19.Visible = true;
            checkBox20.Checked = false;
            checkBox20.Visible = true;
            checkBox21.Checked = false;
            checkBox21.Visible = true;
            checkBox22.Checked = false;
            checkBox22.Visible = true;
            checkBox23.Checked = false;
            checkBox23.Visible = true;
            checkBox24.Checked = false;
            checkBox24.Visible = true;
            checkBox25.Checked = false;
            checkBox25.Visible = true;
            checkBox26.Checked = false;
            checkBox26.Visible = true;
            checkBox27.Checked = false;
            checkBox27.Visible = true;
            checkBox28.Checked = false;
            checkBox28.Visible = true;
            checkBox29.Checked = false;
            checkBox29.Visible = true;
            checkBox30.Checked = false;
            checkBox30.Visible = true;
            checkBox31.Checked = false;
            checkBox31.Visible = true;

            panelday29.Visible = true;
            panelday30.Visible = true;
            panelday31.Visible = true;


        }

        private void butShowSalary_Click(object sender, EventArgs e)
        {
            CleanTb();
            ShowMonthly();
            //WeekendCheck();
            HolidayCheck();
            
            
        }
        private void CheckBox()
        {
            if (checkBox1.Checked == true)
            {
                InsertHoliday(1);
            }
            if (checkBox2.Checked == true)
            {
                InsertHoliday(2);
            }
            if (checkBox3.Checked == true)
            {
                InsertHoliday(3);
            }
            if (checkBox4.Checked == true)
            {
                InsertHoliday(4);
            }
            if (checkBox5.Checked == true)
            {
                InsertHoliday(5);
            }
            if (checkBox6.Checked == true)
            {
                InsertHoliday(6);
            }
            if (checkBox7.Checked == true)
            {
                InsertHoliday(7);
            }
            if (checkBox8.Checked == true)
            {
                InsertHoliday(8);
            }
            if (checkBox9.Checked == true)
            {
                InsertHoliday(9);
            }
            if (checkBox10.Checked == true)
            {
                InsertHoliday(10);
            }
            if (checkBox11.Checked == true)
            {
                InsertHoliday(11);
            }
            if (checkBox12.Checked == true)
            {
                InsertHoliday(12);
            }
            if (checkBox13.Checked == true)
            {
                InsertHoliday(13);
            }
            if (checkBox14.Checked == true)
            {
                InsertHoliday(14);
            }
            if (checkBox15.Checked == true)
            {
                InsertHoliday(15);
            }
            if (checkBox16.Checked == true)
            {
                InsertHoliday(16);
            }
            if (checkBox17.Checked == true)
            {
                InsertHoliday(17);
            }
            if (checkBox18.Checked == true)
            {
                InsertHoliday(18);
            }
            if (checkBox19.Checked == true)
            {
                InsertHoliday(19);
            }
            if (checkBox20.Checked == true)
            {
                InsertHoliday(20);
            }
            if (checkBox21.Checked == true)
            {
                InsertHoliday(21);
            }
            if (checkBox22.Checked == true)
            {
                InsertHoliday(22);
            }
            if (checkBox23.Checked == true)
            {
                InsertHoliday(23);
            }
            if (checkBox24.Checked == true)
            {
                InsertHoliday(24);
            }
            if (checkBox25.Checked == true)
            {
                InsertHoliday(25);
            }
            if (checkBox26.Checked == true)
            {
                InsertHoliday(26);
            }
            if (checkBox27.Checked == true)
            {
                InsertHoliday(27);
            }
            if (checkBox28.Checked == true)
            {
                InsertHoliday(28);
            }
            if (checkBox29.Checked == true)
            {
                InsertHoliday(29);
            }
            if (checkBox30.Checked == true)
            {
                InsertHoliday(31);
            }
            if (checkBox31.Checked == true)
            {
                InsertHoliday(31);
            }

        }
        private void CheckBoxDelete()
        {
            if (checkBox1.Checked == false)
            {
                DeleteHoliday(1);
            }
            if (checkBox2.Checked == false)
            {
                DeleteHoliday(2);
            }
            if (checkBox3.Checked == false)
            {
                DeleteHoliday(3);
            }
            if (checkBox4.Checked == false)
            {
                DeleteHoliday(4);
            }
            if (checkBox5.Checked == false)
            {
                DeleteHoliday(5);
            }
            if (checkBox6.Checked == false)
            {
                DeleteHoliday(6);
            }
            if (checkBox7.Checked == false)
            {
                DeleteHoliday(7);
            }
            if (checkBox8.Checked == false)
            {
                DeleteHoliday(8);
            }
            if (checkBox9.Checked == false)
            {
                DeleteHoliday(9);
            }
            if (checkBox10.Checked == false)
            {
                DeleteHoliday(10);
            }
            if (checkBox11.Checked == false)
            {
                DeleteHoliday(11);
            }
            if (checkBox12.Checked == false)
            {
                DeleteHoliday(12);
            }
            if (checkBox13.Checked == false)
            {
                DeleteHoliday(13);
            }
            if (checkBox14.Checked == false)
            {
                DeleteHoliday(14);
            }
            if (checkBox15.Checked == false)
            {
                DeleteHoliday(15);
            }
            if (checkBox16.Checked == false)
            {
                DeleteHoliday(16);
            }
            if (checkBox17.Checked == false)
            {
                DeleteHoliday(17);
            }
            if (checkBox18.Checked == false)
            {
                DeleteHoliday(18);
            }
            if (checkBox19.Checked == false)
            {
                DeleteHoliday(19);
            }
            if (checkBox20.Checked == false)
            {
                DeleteHoliday(20);
            }
            if (checkBox21.Checked == false)
            {
                DeleteHoliday(21);
            }
            if (checkBox22.Checked == false)
            {
                DeleteHoliday(22);
            }
            if (checkBox23.Checked == false)
            {
                DeleteHoliday(23);
            }
            if (checkBox24.Checked == false)
            {
                DeleteHoliday(24);
            }
            if (checkBox25.Checked == false)
            {
                DeleteHoliday(25);
            }
            if (checkBox26.Checked == false)
            {
                DeleteHoliday(26);
            }
            if (checkBox27.Checked == false)
            {
                DeleteHoliday(27);
            }
            if (checkBox28.Checked == false)
            {
                DeleteHoliday(28);
            }
            if (checkBox29.Checked == false)
            {
                DeleteHoliday(29);
            }
            if (checkBox30.Checked == false)
            {
                DeleteHoliday(31);
            }
            if (checkBox31.Checked == false)
            {
                DeleteHoliday(31);
            }

        }
        private void InsertHoliday(int no)
        {
           
            IDbTransaction trans = null;
           
            using (IDbConnection cnn = new OleDbConnection(con))
            {

                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                int m = int.Parse(ComboMonth(comboBoxM.Text));
                int y = int.Parse(comboBoxY.Text);
                DateTime day = new DateTime(y, m, no);
                string SQL = " SELECT * FROM Holiday WHERE MONTH(HoliDay_Date) = "+m+" AND YEAR(HoliDay_Date) = "+y+" AND Day(HoliDay_Date) = " + no;
               

                DataTable tbl = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(SQL, (OleDbConnection)cnn))
                {
                   if(adapter.Fill(tbl) == 0)
                    {

                        string sql_Text = "INSERT INTO Holiday (HoliDay_Date)";
                        sql_Text += "VALUES ('" + day + "')";

                        using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                        {
                            trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                            cmd_command.Connection = cnn;
                            cmd_command.Transaction = trans;

                            try
                            {
                                cmd_command.ExecuteNonQuery();
                                trans.Commit();
                                MessageBox.Show("Updated information", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }
                            catch (OleDbException ex)
                            {
                                trans.Rollback();
                                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }

                        }
                    }
                }

            }
        }
        private void DeleteHoliday(int no)
        {

            IDbTransaction trans = null;

            using (IDbConnection cnn = new OleDbConnection(con))
            {
                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


                int m = int.Parse(ComboMonth(comboBoxM.Text));
                int y = int.Parse(comboBoxY.Text);
                DateTime day = new DateTime(y, m, no);
                string sql_Text = "DELETE FROM Holiday ";
                sql_Text += "WHERE MONTH(HoliDay_Date) = " + m + " AND YEAR(HoliDay_Date) = " + y + " AND Day(HoliDay_Date) = " + no;

                using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                {
                    trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                    cmd_command.Connection = cnn;
                    cmd_command.Transaction = trans;

                    try
                    {
                        cmd_command.ExecuteNonQuery();
                        trans.Commit();
                        

                    }
                    catch (OleDbException ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }

            }
        }


        private void butupdate_Click(object sender, EventArgs e)
        {
            CheckBox();
            CheckBoxDelete();
        }
        

        private void MonthlyHolidaysForm_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                tbday1.BackColor = Color.LightPink;
                tbNo1.BackColor = Color.LightPink;
            }
            else
            {
                tbday1.BackColor = Color.Empty;
                tbNo1.BackColor = Color.Empty;
            }
            if (checkBox2.Checked)
            {
                tbday2.BackColor = Color.LightPink;
                tbNo2.BackColor = Color.LightPink;
            }
            else
            {
                tbday2.BackColor = Color.Empty;
                tbNo2.BackColor = Color.Empty;
            }
            if (checkBox3.Checked)
            {
                tbday3.BackColor = Color.LightPink;
                tbNo3.BackColor = Color.LightPink;
            }
            else
            {
                tbday3.BackColor = Color.Empty;
                tbNo3.BackColor = Color.Empty;
            }
            if (checkBox4.Checked)
            {
                tbday4.BackColor = Color.LightPink;
                tbNo4.BackColor = Color.LightPink;
            }
            else
            {
                tbday4.BackColor = Color.Empty;
                tbNo4.BackColor = Color.Empty;
            }
            if (checkBox5.Checked)
            {
                tbday5.BackColor = Color.LightPink;
                tbNo5.BackColor = Color.LightPink;
            }
            else
            {
                tbday5.BackColor = Color.Empty;
                tbNo5.BackColor = Color.Empty;
            }
            if (checkBox6.Checked)
            {
                tbday6.BackColor = Color.LightPink;
                tbNo6.BackColor = Color.LightPink;
            }
            else
            {
                tbday6.BackColor = Color.Empty;
                tbNo6.BackColor = Color.Empty;
            }
            if (checkBox7.Checked)
            {
                tbday7.BackColor = Color.LightPink;
                tbNo7.BackColor = Color.LightPink;
            }
            else
            {
                tbday7.BackColor = Color.Empty;
                tbNo7.BackColor = Color.Empty;
            }
            if (checkBox8.Checked)
            {
                tbday8.BackColor = Color.LightPink;
                tbNo8.BackColor = Color.LightPink;
            }
            else
            {
                tbday8.BackColor = Color.Empty;
                tbNo8.BackColor = Color.Empty;
            }
            if (checkBox9.Checked)
            {
                tbday9.BackColor = Color.LightPink;
                tbNo9.BackColor = Color.LightPink;
            }
            else
            {
                tbday9.BackColor = Color.Empty;
                tbNo9.BackColor = Color.Empty;
            }
            if (checkBox10.Checked)
            {
                tbday10.BackColor = Color.LightPink;
                tbNo10.BackColor = Color.LightPink;
            }
            else
            {
                tbday10.BackColor = Color.Empty;
                tbNo10.BackColor = Color.Empty;
            }
            if (checkBox11.Checked)
            {
                tbday11.BackColor = Color.LightPink;
                tbNo11.BackColor = Color.LightPink;
            }
            else
            {
                tbday11.BackColor = Color.Empty;
                tbNo11.BackColor = Color.Empty;
            }
            if (checkBox12.Checked)
            {
                tbday12.BackColor = Color.LightPink;
                tbNo12.BackColor = Color.LightPink;
            }
            else
            {
                tbday12.BackColor = Color.Empty;
                tbNo12.BackColor = Color.Empty;
            }
            if (checkBox13.Checked)
            {
                tbday13.BackColor = Color.LightPink;
                tbNo13.BackColor = Color.LightPink;
            }
            else
            {
                tbday13.BackColor = Color.Empty;
                tbNo13.BackColor = Color.Empty;
            }
            if (checkBox14.Checked)
            {
                tbday14.BackColor = Color.LightPink;
                tbNo14.BackColor = Color.LightPink;
            }
            else
            {
                tbday14.BackColor = Color.Empty;
                tbNo14.BackColor = Color.Empty;
            }
            if (checkBox15.Checked)
            {
                tbday15.BackColor = Color.LightPink;
                tbNo15.BackColor = Color.LightPink;
            }
            else
            {
                tbday15.BackColor = Color.Empty;
                tbNo15.BackColor = Color.Empty;
            }
            if (checkBox16.Checked)
            {
                tbday16.BackColor = Color.LightPink;
                tbNo16.BackColor = Color.LightPink;
            }
            else
            {
                tbday16.BackColor = Color.Empty;
                tbNo16.BackColor = Color.Empty;
            }
            if (checkBox17.Checked)
            {
                tbday17.BackColor = Color.LightPink;
                tbNo17.BackColor = Color.LightPink;
            }
            else
            {
                tbday17.BackColor = Color.Empty;
                tbNo17.BackColor = Color.Empty;
            }
            if (checkBox18.Checked)
            {
                tbday18.BackColor = Color.LightPink;
                tbNo18.BackColor = Color.LightPink;
            }
            else
            {
                tbday18.BackColor = Color.Empty;
                tbNo18.BackColor = Color.Empty;
            }
            if (checkBox19.Checked)
            {
                tbday19.BackColor = Color.LightPink;
                tbNo19.BackColor = Color.LightPink;
            }
            else
            {
                tbday19.BackColor = Color.Empty;
                tbNo19.BackColor = Color.Empty;
            }
            if (checkBox20.Checked)
            {
                tbday20.BackColor = Color.LightPink;
                tbNo20.BackColor = Color.LightPink;
            }
            else
            {
                tbday20.BackColor = Color.Empty;
                tbNo20.BackColor = Color.Empty;
            }
            if (checkBox21.Checked)
            {
                tbday21.BackColor = Color.LightPink;
                tbNo21.BackColor = Color.LightPink;
            }
            else
            {
                tbday21.BackColor = Color.Empty;
                tbNo21.BackColor = Color.Empty;
            }
            if (checkBox22.Checked)
            {
                tbday22.BackColor = Color.LightPink;
                tbNo22.BackColor = Color.LightPink;
            }
            else
            {
                tbday22.BackColor = Color.Empty;
                tbNo22.BackColor = Color.Empty;
            }
            if (checkBox23.Checked)
            {
                tbday23.BackColor = Color.LightPink;
                tbNo23.BackColor = Color.LightPink;
            }
            else
            {
                tbday23.BackColor = Color.Empty;
                tbNo23.BackColor = Color.Empty;
            }
            if (checkBox24.Checked)
            {
                tbday24.BackColor = Color.LightPink;
                tbNo24.BackColor = Color.LightPink;
            }
            else
            {
                tbday24.BackColor = Color.Empty;
                tbNo24.BackColor = Color.Empty;
            }
            if (checkBox25.Checked)
            {
                tbday25.BackColor = Color.LightPink;
                tbNo25.BackColor = Color.LightPink;
            }
            else
            {
                tbday25.BackColor = Color.Empty;
                tbNo25.BackColor = Color.Empty;
            }
            if (checkBox26.Checked)
            {
                tbday26.BackColor = Color.LightPink;
                tbNo26.BackColor = Color.LightPink;
            }
            else
            {
                tbday26.BackColor = Color.Empty;
                tbNo26.BackColor = Color.Empty;
            }
            if (checkBox27.Checked)
            {
                tbday27.BackColor = Color.LightPink;
                tbNo27.BackColor = Color.LightPink;
            }
            else
            {
                tbday27.BackColor = Color.Empty;
                tbNo27.BackColor = Color.Empty;
            }
            if (checkBox28.Checked)
            {
                tbday28.BackColor = Color.LightPink;
                tbNo28.BackColor = Color.LightPink;
            }
            else
            {
                tbday28.BackColor = Color.Empty;
                tbNo28.BackColor = Color.Empty;
            }
            if (checkBox29.Checked)
            {
                tbday29.BackColor = Color.LightPink;
                tbNo29.BackColor = Color.LightPink;
            }
            else
            {
                tbday29.BackColor = Color.Empty;
                tbNo29.BackColor = Color.Empty;
            }
            if (checkBox30.Checked)
            {
                tbday30.BackColor = Color.LightPink;
                tbNo30.BackColor = Color.LightPink;
            }
            else
            {
                tbday30.BackColor = Color.Empty;
                tbNo30.BackColor = Color.Empty;
            }
            if (checkBox31.Checked)
            {
                tbday31.BackColor = Color.LightPink;
                tbNo31.BackColor = Color.LightPink;
            }
            else
            {
                tbday31.BackColor = Color.Empty;
                tbNo31.BackColor = Color.Empty;
            }

        }
        private Color DayColor(string day)
        {
            Color color = Color.Transparent;
            if (day != "")
            {
                if (day == "Sunday")
                {
                    color = Color.Red;
                }
                else if (day == "Monday")
                {
                    color = Color.FromArgb(255, 215, 0);
                }
                else if (day == "Tuesday")
                {
                    color = Color.Pink;
                }
                else if (day == "Wednesday")
                {
                    color = Color.Green;
                }
                else if (day == "Thursday")
                {
                    color = Color.Orange;
                }
                else if (day == "Friday")
                {
                    color = Color.Blue;
                }
                else if (day == "Saturday")
                {
                    color = Color.Violet;
                }
            }
            else
            {
                color = Color.Transparent;
            }
            return color;

        }
    }
}
