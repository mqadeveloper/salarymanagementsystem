﻿using Guna.UI2.WinForms.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MQASalaryManagementSystem
{
    public partial class SocialForm : Form
    {
        public SocialForm()
        {
            InitializeComponent();
            ComboSelectYear();
            int thisyear = int.Parse(DateTime.Today.ToString("yyyy"));
            ShowSocia(thisyear);
            checkMonthYear();
            DataGridViewScrollHelper vScrollHelper = new DataGridViewScrollHelper(dataGridView1, guna2VScrollBar1, true);

        }

        private void butShowSocial_Click(object sender, EventArgs e)
        {
            ShowSocia(int.Parse(comboBoxSelectY.Text));
            checkMonthYear();

        }
        private void ComboSelectYear()
        {
            comboBoxSelectY.DataSource = Enumerable.Range(2011, DateTime.Now.Year - 2011 + 1).ToList();
            comboBoxSelectY.SelectedItem = DateTime.Now.Year;
        }
        private void ShowSocia(int selectyear)
        {
            
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (IDbConnection cnn = new OleDbConnection(con))
            {
                try
                {
                    cnn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                string cbposition  = "SELECT Employees.EMP_ID, Employees.EMP_FristName, Employees.EMP_LastName, Employees.EMP_NickName, Social_Security.* ";
                       cbposition += "FROM Social_Security INNER JOIN Employees ON Employees.EMP_ID = Social_Security.Social_Security_ID "; 
                       cbposition += "WHERE Year_Premiums = "+selectyear ;
                DataTable tbl = new DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(cbposition, con);
                adapter.Fill(tbl);
                dataGridView1.DataSource = tbl;
               
                     
            }
        }

        private void butUpdateSocial_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Do you want to edit?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {
                int x;
                if (!int.TryParse(tbJan.Text, out x))
                {
                    MessageBox.Show("Jan is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbFeb.Text, out x))
                {
                    MessageBox.Show("Feb is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbMar.Text, out x))
                {
                    MessageBox.Show("Mar is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbApr.Text, out x))
                {
                    MessageBox.Show("Apr is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbMay.Text, out x))
                {
                    MessageBox.Show("May is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbJun.Text, out x))
                {
                    MessageBox.Show("Jun is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbJul.Text, out x))
                {
                    MessageBox.Show("Jul is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbAug.Text, out x))
                {
                    MessageBox.Show("Aug is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbSep.Text, out x))
                {
                    MessageBox.Show("Sep is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbOct.Text, out x))
                {
                    MessageBox.Show("Oct is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbNov.Text, out x))
                {
                    MessageBox.Show("Nov is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!int.TryParse(tbDec.Text, out x))
                {
                    MessageBox.Show("Dec is not integer", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbJan.Text) < 0)
                {
                    MessageBox.Show("Jan is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbFeb.Text) < 0)
                {
                    MessageBox.Show("Feb  is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbMar.Text) < 0)
                {
                    MessageBox.Show("Mar  is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbApr.Text) < 0)
                {
                    MessageBox.Show("Apr is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbMay.Text) < 0)
                {
                    MessageBox.Show("May is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbJun.Text) < 0)
                {
                    MessageBox.Show("Jun  is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbJul.Text) < 0)
                {
                    MessageBox.Show("Jul is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbAug.Text) < 0)
                {
                    MessageBox.Show("Aug is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbSep.Text) < 0)
                {
                    MessageBox.Show("Sep is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbOct.Text) < 0)
                {
                    MessageBox.Show("Oct is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbNov.Text) < 0)
                {
                    MessageBox.Show("Nov is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (int.Parse(tbDec.Text) < 0)
                {
                    MessageBox.Show("Dec is not support Negative Values", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
                else
                {
                    UpdateSocial(labelEmpId.Text);
                    ShowSocia(int.Parse(comboBoxSelectY.Text));
                }





                
            }
        }
        private void UpdateSocial(string empid)
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            IDbTransaction trans = null;
            using (IDbConnection cnn = new OleDbConnection(con))
            {


               

                string sql_Text = "UPDATE Social_Security SET Jan ='"+tbJan.Text+"', Feb ='"+tbFeb.Text +
                    "', Mar ='" + tbMar.Text + "', Apr ='" + tbApr.Text + 
                    "', May ='" + tbMay.Text + "', Jun ='" + tbJun.Text + 
                    "', Jul ='" + tbJul.Text + "', Aug ='" + tbAug.Text + 
                    "', Sep ='" + tbSep.Text + "', Oct ='" + tbOct.Text + 
                    "', Nov ='" + tbNov.Text + "', Dece ='" + tbDec.Text + "' WHERE Social_Security_ID = " + empid;




                using (IDbCommand cmd_command = new OleDbCommand(sql_Text, (OleDbConnection)cnn))
                {
                    try
                    {
                        cnn.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                    cmd_command.Connection = cnn;
                    cmd_command.Transaction = trans;

                    try
                    {
                        
                        cmd_command.ExecuteNonQuery();
                        trans.Commit();
                        MessageBox.Show("Update Socil Security Premiums Complete", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (OleDbException ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }


            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                DataGridViewRow dgvRow = dataGridView1.Rows[e.RowIndex];
                labelEmpId.Text = dgvRow.Cells[0].Value.ToString();
                labelNname.Text = dgvRow.Cells[3].Value.ToString();
                tbJan.Text = dgvRow.Cells[5].Value.ToString();
                tbFeb.Text = dgvRow.Cells[6].Value.ToString();
                tbMar.Text = dgvRow.Cells[7].Value.ToString();
                tbApr.Text = dgvRow.Cells[8].Value.ToString();
                tbMay.Text = dgvRow.Cells[9].Value.ToString();
                tbJun.Text = dgvRow.Cells[10].Value.ToString();
                tbJul.Text = dgvRow.Cells[11].Value.ToString();
                tbAug.Text = dgvRow.Cells[12].Value.ToString();
                tbSep.Text = dgvRow.Cells[13].Value.ToString();
                tbOct.Text = dgvRow.Cells[14].Value.ToString();
                tbNov.Text = dgvRow.Cells[15].Value.ToString();
                tbDec.Text = dgvRow.Cells[16].Value.ToString();

            }
        }


        //----------Tesxt Box Read Only  true/false ------------------------------
        private void checkMonthYear()
        {
            string month = DateTime.Today.ToString("MM");
            int monthnum = int.Parse(month);
            string year = DateTime.Today.ToString("yyyy");
           

            if (year.Contains(comboBoxSelectY.Text))
            {
                tbJan.ReadOnly = false;
                tbFeb.ReadOnly = false;
                tbMar.ReadOnly = false;
                tbApr.ReadOnly = false;
                tbMay.ReadOnly = false;
                tbJun.ReadOnly = false;
                tbJul.ReadOnly = false;
                tbAug.ReadOnly = false;
                tbSep.ReadOnly = false;
                tbOct.ReadOnly = false;
                tbNov.ReadOnly = false;
                tbDec.ReadOnly = false;

                if (monthnum > 1)
                {
                    tbJan.ReadOnly = true;
                    if (monthnum > 2)
                    {
                        tbFeb.ReadOnly = true;
                        if (monthnum > 3)
                        {
                            tbMar.ReadOnly = true;
                            if (monthnum > 4)
                            {
                                tbApr.ReadOnly = true;
                                if (monthnum > 5)
                                {
                                    tbMay.ReadOnly = true;
                                    if (monthnum > 6)
                                    {
                                        tbJun.ReadOnly = true;
                                        if (monthnum > 7)
                                        {
                                            tbJul.ReadOnly = true;
                                            if (monthnum > 8)
                                            {
                                                tbAug.ReadOnly = true;
                                                if (monthnum > 9)
                                                {
                                                    tbSep.ReadOnly = true;
                                                    if (monthnum > 10)
                                                    {
                                                        tbOct.ReadOnly = true;
                                                        if (monthnum > 11)
                                                        {
                                                            tbNov.ReadOnly = true;
                                                            if (monthnum > 12)
                                                            {
                                                                tbDec.ReadOnly = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else 
            {
                tbJan.ReadOnly = true;
                tbFeb.ReadOnly = true;
                tbMar.ReadOnly = true;
                tbApr.ReadOnly = true;
                tbMay.ReadOnly = true;
                tbJun.ReadOnly = true;
                tbJul.ReadOnly = true;
                tbAug.ReadOnly = true;
                tbSep.ReadOnly = true;
                tbOct.ReadOnly = true;
                tbNov.ReadOnly = true;
                tbDec.ReadOnly = true;
            }



        }
    }
}
