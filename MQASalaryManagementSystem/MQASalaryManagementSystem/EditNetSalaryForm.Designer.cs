﻿
namespace MQASalaryManagementSystem
{
    partial class EditNetSalaryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSalaryID = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.tbOT = new System.Windows.Forms.TextBox();
            this.tbBonus = new System.Windows.Forms.TextBox();
            this.tbLanguage = new System.Windows.Forms.TextBox();
            this.tbHosing = new System.Windows.Forms.TextBox();
            this.tbTraval = new System.Windows.Forms.TextBox();
            this.tbExecutive = new System.Windows.Forms.TextBox();
            this.tbBaseSalary = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.butedit = new System.Windows.Forms.Button();
            this.butclose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelSalaryID
            // 
            this.labelSalaryID.AutoSize = true;
            this.labelSalaryID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSalaryID.Location = new System.Drawing.Point(202, 85);
            this.labelSalaryID.Name = "labelSalaryID";
            this.labelSalaryID.Size = new System.Drawing.Size(0, 16);
            this.labelSalaryID.TabIndex = 86;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(83, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 16);
            this.label12.TabIndex = 85;
            this.label12.Text = "Salary ID :";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(202, 117);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(0, 16);
            this.labelName.TabIndex = 84;
            // 
            // tbOT
            // 
            this.tbOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOT.Location = new System.Drawing.Point(141, 319);
            this.tbOT.Multiline = true;
            this.tbOT.Name = "tbOT";
            this.tbOT.Size = new System.Drawing.Size(156, 24);
            this.tbOT.TabIndex = 83;
            // 
            // tbBonus
            // 
            this.tbBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBonus.Location = new System.Drawing.Point(141, 289);
            this.tbBonus.Multiline = true;
            this.tbBonus.Name = "tbBonus";
            this.tbBonus.Size = new System.Drawing.Size(156, 24);
            this.tbBonus.TabIndex = 82;
            // 
            // tbLanguage
            // 
            this.tbLanguage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLanguage.Location = new System.Drawing.Point(141, 259);
            this.tbLanguage.Multiline = true;
            this.tbLanguage.Name = "tbLanguage";
            this.tbLanguage.Size = new System.Drawing.Size(156, 24);
            this.tbLanguage.TabIndex = 81;
            // 
            // tbHosing
            // 
            this.tbHosing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbHosing.Location = new System.Drawing.Point(141, 229);
            this.tbHosing.Multiline = true;
            this.tbHosing.Name = "tbHosing";
            this.tbHosing.Size = new System.Drawing.Size(156, 24);
            this.tbHosing.TabIndex = 80;
            // 
            // tbTraval
            // 
            this.tbTraval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTraval.Location = new System.Drawing.Point(141, 200);
            this.tbTraval.Multiline = true;
            this.tbTraval.Name = "tbTraval";
            this.tbTraval.Size = new System.Drawing.Size(156, 24);
            this.tbTraval.TabIndex = 79;
            // 
            // tbExecutive
            // 
            this.tbExecutive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbExecutive.Location = new System.Drawing.Point(141, 170);
            this.tbExecutive.Multiline = true;
            this.tbExecutive.Name = "tbExecutive";
            this.tbExecutive.Size = new System.Drawing.Size(156, 24);
            this.tbExecutive.TabIndex = 78;
            // 
            // tbBaseSalary
            // 
            this.tbBaseSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBaseSalary.Location = new System.Drawing.Point(141, 141);
            this.tbBaseSalary.Multiline = true;
            this.tbBaseSalary.Name = "tbBaseSalary";
            this.tbBaseSalary.Size = new System.Drawing.Size(156, 24);
            this.tbBaseSalary.TabIndex = 77;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(83, 297);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 16);
            this.label8.TabIndex = 76;
            this.label8.Text = "Bonus :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(4, 267);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 16);
            this.label7.TabIndex = 75;
            this.label7.Text = "Language Alloance :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(102, 327);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 16);
            this.label6.TabIndex = 74;
            this.label6.Text = "OT :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 237);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 16);
            this.label5.TabIndex = 73;
            this.label5.Text = "Housing Alloance :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 16);
            this.label4.TabIndex = 72;
            this.label4.Text = "Traval Alloance :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 16);
            this.label3.TabIndex = 71;
            this.label3.Text = "Executive Alloance :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 70;
            this.label2.Text = "Base Salary :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(83, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 69;
            this.label1.Text = "Name :";
            // 
            // butedit
            // 
            this.butedit.Location = new System.Drawing.Point(211, 402);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(107, 36);
            this.butedit.TabIndex = 87;
            this.butedit.Text = "Edit";
            this.butedit.UseVisualStyleBackColor = true;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // butclose
            // 
            this.butclose.Location = new System.Drawing.Point(12, 402);
            this.butclose.Name = "butclose";
            this.butclose.Size = new System.Drawing.Size(107, 36);
            this.butclose.TabIndex = 88;
            this.butclose.Text = "Close";
            this.butclose.UseVisualStyleBackColor = true;
            this.butclose.Click += new System.EventHandler(this.butclose_Click);
            // 
            // EditNetSalaryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 450);
            this.Controls.Add(this.butclose);
            this.Controls.Add(this.butedit);
            this.Controls.Add(this.labelSalaryID);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.tbOT);
            this.Controls.Add(this.tbBonus);
            this.Controls.Add(this.tbLanguage);
            this.Controls.Add(this.tbHosing);
            this.Controls.Add(this.tbTraval);
            this.Controls.Add(this.tbExecutive);
            this.Controls.Add(this.tbBaseSalary);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "EditNetSalaryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Net Salry Form";
            this.Load += new System.EventHandler(this.OutputForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSalaryID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox tbOT;
        private System.Windows.Forms.TextBox tbBonus;
        private System.Windows.Forms.TextBox tbLanguage;
        private System.Windows.Forms.TextBox tbHosing;
        private System.Windows.Forms.TextBox tbTraval;
        private System.Windows.Forms.TextBox tbExecutive;
        private System.Windows.Forms.TextBox tbBaseSalary;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button butclose;
    }
}