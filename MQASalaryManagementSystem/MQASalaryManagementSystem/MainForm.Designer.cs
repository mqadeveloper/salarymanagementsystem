﻿
namespace MQASalaryManagementSystem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelLeftSise = new System.Windows.Forms.Panel();
            this.butMonthly = new System.Windows.Forms.Button();
            this.butworkday = new System.Windows.Forms.Button();
            this.butSalary = new System.Windows.Forms.Button();
            this.butSocial = new System.Windows.Forms.Button();
            this.butEmp = new System.Windows.Forms.Button();
            this.panelcontrol = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelLeftSise.SuspendLayout();
            this.panelcontrol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelLeftSise
            // 
            this.panelLeftSise.BackColor = System.Drawing.Color.LightCoral;
            this.panelLeftSise.Controls.Add(this.butMonthly);
            this.panelLeftSise.Controls.Add(this.butworkday);
            this.panelLeftSise.Controls.Add(this.butSalary);
            this.panelLeftSise.Controls.Add(this.butSocial);
            this.panelLeftSise.Controls.Add(this.butEmp);
            this.panelLeftSise.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeftSise.Location = new System.Drawing.Point(0, 0);
            this.panelLeftSise.Name = "panelLeftSise";
            this.panelLeftSise.Size = new System.Drawing.Size(153, 550);
            this.panelLeftSise.TabIndex = 0;
            // 
            // butMonthly
            // 
            this.butMonthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butMonthly.Location = new System.Drawing.Point(0, 370);
            this.butMonthly.Name = "butMonthly";
            this.butMonthly.Size = new System.Drawing.Size(153, 59);
            this.butMonthly.TabIndex = 4;
            this.butMonthly.Text = "Monthly Holidays";
            this.butMonthly.UseVisualStyleBackColor = true;
            this.butMonthly.Click += new System.EventHandler(this.butMonthly_Click);
            // 
            // butworkday
            // 
            this.butworkday.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butworkday.Location = new System.Drawing.Point(0, 305);
            this.butworkday.Name = "butworkday";
            this.butworkday.Size = new System.Drawing.Size(153, 59);
            this.butworkday.TabIndex = 3;
            this.butworkday.Text = "Work Day";
            this.butworkday.UseVisualStyleBackColor = true;
            this.butworkday.Click += new System.EventHandler(this.butworkday_Click);
            // 
            // butSalary
            // 
            this.butSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSalary.Location = new System.Drawing.Point(0, 240);
            this.butSalary.Name = "butSalary";
            this.butSalary.Size = new System.Drawing.Size(153, 59);
            this.butSalary.TabIndex = 2;
            this.butSalary.Text = "Salary";
            this.butSalary.UseVisualStyleBackColor = true;
            this.butSalary.Click += new System.EventHandler(this.butSalary_Click);
            // 
            // butSocial
            // 
            this.butSocial.BackColor = System.Drawing.Color.Transparent;
            this.butSocial.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.butSocial.FlatAppearance.BorderSize = 5;
            this.butSocial.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightCoral;
            this.butSocial.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCoral;
            this.butSocial.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSocial.Location = new System.Drawing.Point(0, 175);
            this.butSocial.Name = "butSocial";
            this.butSocial.Size = new System.Drawing.Size(153, 59);
            this.butSocial.TabIndex = 1;
            this.butSocial.Text = "Social Insurance";
            this.butSocial.UseVisualStyleBackColor = false;
            this.butSocial.Click += new System.EventHandler(this.butSocial_Click);
            // 
            // butEmp
            // 
            this.butEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butEmp.Location = new System.Drawing.Point(0, 110);
            this.butEmp.Name = "butEmp";
            this.butEmp.Size = new System.Drawing.Size(153, 59);
            this.butEmp.TabIndex = 0;
            this.butEmp.Text = "Employee";
            this.butEmp.UseVisualStyleBackColor = true;
            this.butEmp.Click += new System.EventHandler(this.butEmp_Click);
            // 
            // panelcontrol
            // 
            this.panelcontrol.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelcontrol.Controls.Add(this.pictureBox1);
            this.panelcontrol.Location = new System.Drawing.Point(153, 0);
            this.panelcontrol.Name = "panelcontrol";
            this.panelcontrol.Size = new System.Drawing.Size(820, 550);
            this.panelcontrol.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(161, 191);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 147);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 550);
            this.Controls.Add(this.panelcontrol);
            this.Controls.Add(this.panelLeftSise);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MQA Salary Management System";
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.panelLeftSise.ResumeLayout(false);
            this.panelcontrol.ResumeLayout(false);
            this.panelcontrol.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLeftSise;
        private System.Windows.Forms.Button butEmp;
        private System.Windows.Forms.Button butSalary;
        private System.Windows.Forms.Button butSocial;
        private System.Windows.Forms.Panel panelcontrol;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button butworkday;
        private System.Windows.Forms.Button butMonthly;
    }
}

