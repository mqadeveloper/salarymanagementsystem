﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQASalaryManagementSystem
{
    class MQATaxWithholdingWithTemplate
    {
        public static void MQATaxWithholdingWithTemplateEx()
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            DateTime time = DateTime.Now;
            string filenametime = time.ToString("yyyyMM");
            Application excApp;
            Workbook excBook;
            Worksheet excSheet, excSheet2, excSheet3 = null, excSheetnew;
            Range rg;
            excApp = new Application();
            excApp.Visible = false;
            excApp.UserControl = false;
            string templateFile = "MQATaxWithholding202105.xlsx";
            string template = Path.Combine(Environment.CurrentDirectory, @"template\", templateFile);
            excBook = (Workbook)excApp.Workbooks.Add(template);

            using (IDbConnection cnn = new OleDbConnection(con))

            {
                    cnn.Open();
                string tr = DateTime.Today.ToString("MM");
                string SQL_Text = "SELECT  Employees.EMP_ID,Employees.EMP_FristName,Employees.EMP_LastName,Employees.EMP_NickName,Net_Salary.*,Tax_Deduction.*";
                SQL_Text += "FROM ((Employees INNER JOIN Net_Salary ON Employees.EMP_ID = Net_Salary.Net_Salary_ID) ";
                SQL_Text += "INNER JOIN Tax_Deduction ON Employees.EMP_ID = Tax_Deduction.Tax_ID) ";
                SQL_Text += "WHERE MONTH(Pay_Date) = " + SalaryForm.comboMonth + " AND YEAR(Pay_Date) = " + SalaryForm.comboYear;
                SQL_Text += " ORDER BY Employees.EMP_ID ASC";


                System.Data.DataTable tbl = new System.Data.DataTable();
                OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
                adapter.Fill(tbl);
                

                int i = 0;
                foreach (DataRow dr in tbl.Rows)
                {
                    int empid = int.Parse(dr[0].ToString());
                    string Fname = dr[1].ToString();
                    string Lname = dr[2].ToString();
                    string Nname = dr[3].ToString();
                    int Base = int.Parse(dr[7].ToString());
                    int Executive = int.Parse(dr[8].ToString());
                    int Travel = int.Parse(dr[9].ToString());
                    int housing = int.Parse(dr[10].ToString());
                    int Language = int.Parse(dr[11].ToString());
                    int Bonus = int.Parse(dr[12].ToString());
                    int OT = int.Parse(dr[13].ToString());
                    string sdate = dr[18].ToString();
                    int Personal = int.Parse(dr[20].ToString());
                    int Spouse = int.Parse(dr[21].ToString());
                    int Child = int.Parse(dr[22].ToString());
                    int Parental = int.Parse(dr[23].ToString());
                    int Expense = int.Parse(dr[24].ToString());
                    int LMF_RTF = int.Parse(dr[25].ToString());
                    int Home_Loan = int.Parse(dr[26].ToString());

                    excSheet = excBook.Sheets[1];
                    excSheet.Copy(Type.Missing, excBook.Sheets[excBook.Sheets.Count]);
                    excSheetnew = excBook.Sheets[excBook.Sheets.Count];
                    excSheetnew.Name = Nname;
                    excSheetnew.Cells[1, 2] = Fname + " " + Lname;
                    excSheetnew.Cells[4, 2] = Base;
                    excSheetnew.Cells[4, 3] = CountEmpidSalary(empid, 1);
                    excSheetnew.Cells[5, 2] = Base;
                    excSheetnew.Cells[5, 3] = CountEmpidSalary(empid, 2);
                    excSheetnew.Cells[6, 2] = Bonus;
                    excSheetnew.Cells[7, 2] = Executive;
                    excSheetnew.Cells[9, 2] = Travel;
                    excSheetnew.Cells[9, 3] = TravalAllow(empid, 1);
                    excSheetnew.Cells[9, 4] = TravalAllow(empid, 0);
                    excSheetnew.Cells[12, 2] = housing;
                    excSheetnew.Cells[12, 4].Formula = "=E79";
                    excSheetnew.Cells[14, 2] = Language;
                    excSheetnew.Cells[16, 2] = OT;
                    excSheetnew.Cells[16, 4].Formula = "=G79";
                    excSheetnew.Cells[24, 4] = -Personal;
                    excSheetnew.Cells[25, 4] = -Spouse;
                    excSheetnew.Cells[26, 4] = -Child;
                    excSheetnew.Cells[28, 4] = -Parental;
                    excSheetnew.Cells[29, 4] = -Expense;
                    excSheetnew.Cells[30, 4] = -Home_Loan;
                    excSheetnew.Cells[31, 4] = -LMF_RTF;
                    excSheetnew.Cells[32, 3] = Secur_premium(empid, 0, "none");
                    excSheetnew.Cells[32, 4] = -Secur_premium(empid, 1, "none");
                    excSheetnew.Cells[66, 1] = "1/1/"+ SalaryForm.comboYear;
                    excSheetnew.Cells[66, 2] = MonthlySalary(empid, 1, 1);
                    excSheetnew.Cells[66, 3] = MonthlySalary(empid, 1, 2);
                    excSheetnew.Cells[66, 4] = MonthlySalary(empid, 1, 3);
                    excSheetnew.Cells[66, 5] = MonthlySalary(empid, 1, 4);
                    excSheetnew.Cells[66, 6] = MonthlySalary(empid, 1, 5); ;
                    excSheetnew.Cells[66, 7] = MonthlySalary(empid, 1, 6); ;
                    excSheetnew.Cells[66, 8] = -Secur_premium(empid, 2, "Jan");
                    excSheetnew.Cells[67, 1] = "2/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[67, 2] = MonthlySalary(empid, 2, 1);
                    excSheetnew.Cells[67, 3] = MonthlySalary(empid, 2, 2);
                    excSheetnew.Cells[67, 4] = MonthlySalary(empid, 2, 3);
                    excSheetnew.Cells[67, 5] = MonthlySalary(empid, 2, 4);
                    excSheetnew.Cells[67, 6] = MonthlySalary(empid, 2, 5);
                    excSheetnew.Cells[67, 7] = MonthlySalary(empid, 2, 6);
                    excSheetnew.Cells[67, 8] = -Secur_premium(empid, 2, "Feb");
                    excSheetnew.Cells[68, 1] = "3/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[68, 2] = MonthlySalary(empid, 3, 1);
                    excSheetnew.Cells[68, 3] = MonthlySalary(empid, 3, 2);
                    excSheetnew.Cells[68, 4] = MonthlySalary(empid, 3, 3);
                    excSheetnew.Cells[68, 5] = MonthlySalary(empid, 3, 4);
                    excSheetnew.Cells[68, 6] = MonthlySalary(empid, 3, 5);
                    excSheetnew.Cells[68, 7] = MonthlySalary(empid, 3, 6);
                    excSheetnew.Cells[68, 8] = -Secur_premium(empid, 2, "Mar");
                    excSheetnew.Cells[69, 1] = "4/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[69, 2] = MonthlySalary(empid, 4, 1);
                    excSheetnew.Cells[69, 3] = MonthlySalary(empid, 4, 2);
                    excSheetnew.Cells[69, 4] = MonthlySalary(empid, 4, 3);
                    excSheetnew.Cells[69, 5] = MonthlySalary(empid, 4, 4);
                    excSheetnew.Cells[69, 6] = MonthlySalary(empid, 4, 5);
                    excSheetnew.Cells[69, 7] = MonthlySalary(empid, 4, 6);
                    excSheetnew.Cells[69, 8] = -Secur_premium(empid, 2, "Apr");
                    excSheetnew.Cells[69, 10].Formula = "=SUM(B69:I69)";
                    excSheetnew.Cells[70, 1] = "5/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[70, 2] = MonthlySalary(empid, 5, 1);
                    excSheetnew.Cells[70, 3] = MonthlySalary(empid, 5, 2);
                    excSheetnew.Cells[70, 4] = MonthlySalary(empid, 5, 3);
                    excSheetnew.Cells[70, 5] = MonthlySalary(empid, 5, 4);
                    excSheetnew.Cells[70, 6] = MonthlySalary(empid, 5, 5);
                    excSheetnew.Cells[70, 7] = MonthlySalary(empid, 5, 6);
                    excSheetnew.Cells[70, 8] = -Secur_premium(empid, 2, "May");
                    excSheetnew.Cells[71, 1] = "6/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[71, 2] = MonthlySalary(empid, 6, 1);
                    excSheetnew.Cells[71, 3] = MonthlySalary(empid, 6, 2);
                    excSheetnew.Cells[71, 4] = MonthlySalary(empid, 6, 3);
                    excSheetnew.Cells[71, 5] = MonthlySalary(empid, 6, 4);
                    excSheetnew.Cells[71, 6] = MonthlySalary(empid, 6, 5);
                    excSheetnew.Cells[71, 7] = MonthlySalary(empid, 6, 6);
                    excSheetnew.Cells[71, 8] = -Secur_premium(empid, 2, "Jun");
                    excSheetnew.Cells[72, 1] = "7/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[72, 2] = MonthlySalary(empid, 7, 1);
                    excSheetnew.Cells[72, 3] = MonthlySalary(empid, 7, 2);
                    excSheetnew.Cells[72, 4] = MonthlySalary(empid, 7, 3);
                    excSheetnew.Cells[72, 5] = MonthlySalary(empid, 7, 4);
                    excSheetnew.Cells[72, 6] = MonthlySalary(empid, 7, 5);
                    excSheetnew.Cells[72, 7] = MonthlySalary(empid, 7, 6);
                    excSheetnew.Cells[72, 8] = -Secur_premium(empid, 2, "Jul");
                    excSheetnew.Cells[73, 1] = "8/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[73, 2] = MonthlySalary(empid, 8, 1);
                    excSheetnew.Cells[73, 3] = MonthlySalary(empid, 8, 2);
                    excSheetnew.Cells[73, 4] = MonthlySalary(empid, 8, 3);
                    excSheetnew.Cells[73, 5] = MonthlySalary(empid, 8, 4);
                    excSheetnew.Cells[73, 6] = MonthlySalary(empid, 8, 5);
                    excSheetnew.Cells[73, 7] = MonthlySalary(empid, 8, 6);
                    excSheetnew.Cells[73, 8] = -Secur_premium(empid, 2, "Aug");
                    excSheetnew.Cells[74, 1] = "9/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[74, 2] = MonthlySalary(empid, 9, 1);
                    excSheetnew.Cells[74, 3] = MonthlySalary(empid, 9, 2);
                    excSheetnew.Cells[74, 4] = MonthlySalary(empid, 9, 3);
                    excSheetnew.Cells[74, 5] = MonthlySalary(empid, 9, 4);
                    excSheetnew.Cells[74, 6] = MonthlySalary(empid, 9, 5);
                    excSheetnew.Cells[74, 7] = MonthlySalary(empid, 9, 6);
                    excSheetnew.Cells[74, 8] = -Secur_premium(empid, 2, "Sep");
                    excSheetnew.Cells[75, 1] = "10/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[75, 2] = MonthlySalary(empid, 10, 1);
                    excSheetnew.Cells[75, 3] = MonthlySalary(empid, 10, 2);
                    excSheetnew.Cells[75, 4] = MonthlySalary(empid, 10, 3);
                    excSheetnew.Cells[75, 5] = MonthlySalary(empid, 10, 4);
                    excSheetnew.Cells[75, 6] = MonthlySalary(empid, 10, 5);
                    excSheetnew.Cells[75, 7] = MonthlySalary(empid, 10, 6);
                    excSheetnew.Cells[75, 8] = -Secur_premium(empid, 2, "Oct");
                    excSheetnew.Cells[76, 1] = "11/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[76, 2] = MonthlySalary(empid, 11, 1);
                    excSheetnew.Cells[76, 3] = MonthlySalary(empid, 11, 2);
                    excSheetnew.Cells[76, 4] = MonthlySalary(empid, 11, 3);
                    excSheetnew.Cells[76, 5] = MonthlySalary(empid, 11, 4);
                    excSheetnew.Cells[76, 6] = MonthlySalary(empid, 11, 5);
                    excSheetnew.Cells[76, 7] = MonthlySalary(empid, 11, 6);
                    excSheetnew.Cells[76, 8] = -Secur_premium(empid, 2, "Nov");
                    excSheetnew.Cells[77, 1] = "12/1/" + SalaryForm.comboYear;
                    excSheetnew.Cells[77, 2] = MonthlySalary(empid, 12, 1);
                    excSheetnew.Cells[77, 3] = MonthlySalary(empid, 12, 2);
                    excSheetnew.Cells[77, 4] = MonthlySalary(empid, 12, 3);
                    excSheetnew.Cells[77, 5] = MonthlySalary(empid, 12, 4);
                    excSheetnew.Cells[77, 6] = MonthlySalary(empid, 12, 5);
                    excSheetnew.Cells[77, 7] = MonthlySalary(empid, 12, 6);
                    excSheetnew.Cells[77, 8] = -Secur_premium(empid, 2, "Dec");
                }
                excBook.SaveAs(@"C:\MQATaxWithholding" + SalaryForm.comboYear+""+SalaryForm.comboMonth + ".xlsx");
                excBook.Close();
            }

        }
        static int CountEmpidSalary(int empid, int num)
        {
            int i = 0;
            int n = 0;
            string month;
            int cmonth = 0;
            if (num == 1)
            {
                month = "AND ( MONTH(Pay_Date) BETWEEN 1 AND 5 )";
                cmonth = 5;

            }
            else if (num == 2)
            {
                month = "AND ( MONTH(Pay_Date) BETWEEN 6 AND 12 )";
                cmonth = 7;
            }
            else
            {
                month = " ";
            }

            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string SQL_Text = "SELECT  * FROM Net_Salary WHERE EMP_ID = " + empid + month;

            System.Data.DataTable tbl = new System.Data.DataTable();
            OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
            adapter.Fill(tbl);
            string tr = DateTime.Today.ToString("MM");
            int rt = int.Parse(tr);
            foreach (DataRow dr in tbl.Rows)
            {
                i++;
            }

            if (i < cmonth && rt > i)
            {
                Console.WriteLine("the " + i);
                i = cmonth;
            }
            return i;
        }
        static int Secur_premium(int empid, int num, string month)
        {
            int i = 0;
            int n = 0;
            int ms = 0;
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string SQL_Text = "SELECT  * FROM Social_Security  WHERE Social_Security_ID = " + empid;

            System.Data.DataTable tbl = new System.Data.DataTable();
            OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
            adapter.Fill(tbl);
            foreach (DataRow dr in tbl.Rows)
            {


                if (int.Parse(dr[1].ToString()) != 0)
                {
                    n += int.Parse(dr[1].ToString());
                    i++;
                    if (month.Contains("Jan"))
                    {
                        ms = int.Parse(dr[1].ToString());
                    }
                }
                if (int.Parse(dr[2].ToString()) != 0)
                {
                    n += int.Parse(dr[2].ToString());
                    i++;
                    if (month.Contains("Feb"))
                    {
                        ms = int.Parse(dr[2].ToString());
                    }
                }
                if (int.Parse(dr[3].ToString()) != 0)
                {
                    n += int.Parse(dr[3].ToString());
                    i++;
                    if (month.Contains("Mar"))
                    {
                        ms = int.Parse(dr[3].ToString());
                    }
                }
                if (int.Parse(dr[4].ToString()) != 0)
                {
                    n += int.Parse(dr[4].ToString());
                    i++;
                    if (month.Contains("Apr"))
                    {
                        ms = int.Parse(dr[4].ToString());
                    }
                }
                if (int.Parse(dr[5].ToString()) != 0)
                {
                    n += int.Parse(dr[5].ToString());
                    i++;
                    if (month.Contains("May"))
                    {
                        ms = int.Parse(dr[5].ToString());
                    }

                }
                if (int.Parse(dr[6].ToString()) != 0)
                {
                    n += int.Parse(dr[6].ToString());
                    i++;
                    if (month.Contains("Jun"))
                    {
                        ms = int.Parse(dr[6].ToString());
                    }
                }
                if (int.Parse(dr[7].ToString()) != 0)
                {
                    n += int.Parse(dr[7].ToString());
                    i++;
                    if (month.Contains("Jul"))
                    {
                        ms = int.Parse(dr[7].ToString());
                    }
                }
                if (int.Parse(dr[8].ToString()) != 0)
                {
                    n += int.Parse(dr[8].ToString());
                    i++;
                    if (month.Contains("Aug"))
                    {
                        ms = int.Parse(dr[8].ToString());
                    }
                }
                if (int.Parse(dr[9].ToString()) != 0)
                {
                    n += int.Parse(dr[9].ToString());
                    i++;
                    if (month.Contains("Sep"))
                    {
                        ms = int.Parse(dr[9].ToString());
                    }
                }
                if (int.Parse(dr[10].ToString()) != 0)
                {
                    n += int.Parse(dr[10].ToString());
                    i++;
                    if (month.Contains("Oct"))
                    {
                        ms = int.Parse(dr[10].ToString());
                    }
                }
                if (int.Parse(dr[11].ToString()) != 0)
                {
                    n += int.Parse(dr[11].ToString());
                    i++;
                    if (month.Contains("Nov"))
                    {
                        ms = int.Parse(dr[11].ToString());
                    }
                }
                if (int.Parse(dr[12].ToString()) != 0)
                {
                    n += int.Parse(dr[12].ToString());
                    i++;
                    if (month.Contains("Dec"))
                    {
                        ms = int.Parse(dr[12].ToString());
                    }
                }

            }
            if (num == 1)
            {
                return n;
            }
            else if (num == 2)
            {
                return ms;
            }
            else
            {
                return i;
            }

        }
        static int TravalAllow(int empid, int count)
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string SQL_Text = "SELECT * FROM Net_Salary WHERE EMP_ID = " + empid;
            int traval = 0;
            int maintraval = 0;
            int i = 0;
            int l = 0;
            System.Data.DataTable tbl = new System.Data.DataTable();
            OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
            adapter.Fill(tbl);
            foreach (DataRow dr in tbl.Rows)
            {

                if (maintraval < int.Parse(dr[5].ToString()))
                {
                    maintraval = int.Parse(dr[5].ToString());
                }
                if (maintraval == int.Parse(dr[5].ToString()))
                {
                    traval += int.Parse(dr[5].ToString());
                    i++;
                }
                else if (maintraval != int.Parse(dr[5].ToString()))
                {
                    traval += int.Parse(dr[5].ToString());
                    l++;
                }


                Console.WriteLine(i);

            }
            int u = 0;
            if (i != 12)
            {
                for (int t = (l + i + 1); t <= 12; t++)
                {
                    traval += maintraval;
                    u++;
                   
                }
            }
            if (count == 1)
            {
                return i + u;
            }
            else
            {
                return traval;
            }
        }
        static int MonthlySalary(int empid ,int month ,int num)
        {
            int Base = 0;
            int Executive = 0;
            int Travel = 0;
            int housing = 0;
            int Language = 0;
            int Bonus = 0;
            int OT = 0;
            if (CheckHaveNetSalary(empid, month) == 0)
            {
                month = 1;
                
            }
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string SQL_Text = "SELECT * FROM Net_Salary WHERE EMP_ID = " + empid +" AND MONTH(Pay_date) = " + month;
            System.Data.DataTable tbl = new System.Data.DataTable();
            OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
              adapter.Fill(tbl);
            foreach (DataRow dr in tbl.Rows)
            {
                 Base = int.Parse(dr[3].ToString());
                 Executive = int.Parse(dr[4].ToString());
                 Travel = int.Parse(dr[5].ToString());
                 housing = int.Parse(dr[6].ToString());
                 Language = int.Parse(dr[7].ToString());
                 Bonus = int.Parse(dr[8].ToString());
                 OT = int.Parse(dr[9].ToString());
            }

            if (num == 1)
            {
                return Base;
            }
            else if (num == 2)
            {
                return Executive;
            }
            else if (num == 3)
            {
                return Travel;
            }
            else if (num == 4)
            {
                return housing;
            }
            else if (num == 5)
            {
                return Language;
            }
            else if (num == 6)
            {
                return OT;
            }
            else if (num == 7)
            {
                return Bonus;
            }
            else
            {
                return 0;
            }
        }

        static int CheckHaveNetSalary(int empid, int month)
        {
            string con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string SQL_Text = "SELECT * FROM Net_Salary WHERE EMP_ID = " + empid + " AND MONTH(Pay_date) = " + month;
            System.Data.DataTable tbl = new System.Data.DataTable();
            OleDbDataAdapter adapter = new OleDbDataAdapter(SQL_Text, con);
           int sum =  adapter.Fill(tbl);

            return sum;

            
        }
        
        }
}
